import 'package:flutter/material.dart';
import 'package:tos_payment/pages/attach_success.page.dart';
import 'package:tos_payment/pages/charge.page.dart';
import 'package:tos_payment/pages/charge_detail.page.dart';
import 'package:tos_payment/pages/charge_success.page.dart';
import 'package:tos_payment/pages/login.page.dart';
import 'package:tos_payment/pages/payment_chart.page.dart';
import 'package:tos_payment/pages/payment_detail.page.dart';
import 'package:tos_payment/pages/payment_list.page.dart';
import 'package:tos_payment/pages/payment_summary.page.dart';
import 'package:tos_payment/pages/root.page.dart';
import 'package:tos_payment/pages/scan_delivery.page.dart';
import 'package:tos_payment/pages/scan_history.page.dart';
import 'package:tos_payment/pages/scan_pickup.page.dart';
import 'package:tos_payment/pages/scan_verify.page.dart';
import 'package:tos_payment/pages/scan_veryfy_attach.dart';
import 'package:tos_payment/pages/select_receipt.page.dart';
import 'package:tos_payment/pages/splash.page.dart';
import 'package:tos_payment/utils/routes.util.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case RouteData.RouteSplash:
      return CustomRoute(
        builder: (_) => new SplashPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteLogin:
      return CustomRoute(
        builder: (_) => new LoginPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteRoot:
      return CustomRoute(
        builder: (_) => new RootPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteScanPickup:
      return CustomRoute(
        builder: (_) => new ScanPickupPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteScanDelivery:
      return CustomRoute(
        builder: (_) => new ScanDeliveryPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RoutePaymentDetail:
      return CustomRoute(
        builder: (_) => new PaymentDetailPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RoutePaymentSummary:
      return CustomRoute(
        builder: (_) => new PaymentSummaryPage(arguments: settings.arguments),
        settings: settings,
      );

    case RouteData.RoutePaymentList:
      return CustomRoute(
        builder: (_) => new PaymentListPage(arguments: settings.arguments),
        settings: settings,
      );

    case RouteData.RoutePaymentChart:
      return CustomRoute(
        builder: (_) => new PaymentChartPage(arguments: settings.arguments),
        settings: settings,
      );

    case RouteData.RouteCharge:
      return CustomRoute(
        builder: (_) => new ChargePage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteChargeDetail:
      return CustomRoute(
        builder: (_) => new ChargeDetailPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteChargeSuccess:
      return CustomRoute(
        builder: (_) => new ChargeSuccessPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteSelectReceipt:
      return CustomRoute(
        builder: (_) => new SelectReceiptPage(arguments: settings.arguments),
        settings: settings,
      );

    case RouteData.RouteScanVerify:
      return CustomRoute(
        builder: (_) => new ScanVerifyPage(arguments: settings.arguments),
        settings: settings,
      );

    case RouteData.RouteScanVerifyAttach:
      return CustomRoute(
        builder: (_) => new ScanVerifyAttachPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteScanHistory:
      return CustomRoute(
        builder: (_) => new ScanHistoryPage(arguments: settings.arguments),
        settings: settings,
      );
    case RouteData.RouteAttachSuccess:
      return CustomRoute(
        builder: (_) => new AttachSuccessPage(arguments: settings.arguments),
        settings: settings,
      );
    default:
      return CustomRoute(
        builder: (_) => new LoginPage(),
        settings: settings,
      );
  }
}

List<Route<dynamic>> generateInitialRoutes(String initialRoute) {
  return <Route>[CustomRoute()];
}

class CustomRoute<T> extends MaterialPageRoute<T> {
  CustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // if (settings.name == RouteData.RouteRoot) return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}
