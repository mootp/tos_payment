class RouteData {
  static const String RouteSplash = '/splash';
  static const String RouteRoot = '/root';
  static const String RouteLogin = '/login';
  static const String RouteHome = '/home';
  static const String RouteScanPickup = '/scanpickup';
  static const String RouteScanDelivery = '/scandelivery';
  static const String RouteScanVerify = '/scanverify';
  
  static const String RouteScanVerifyAttach = '/scanverifyattach';
  static const String RouteAttachSuccess = '/attachsuccess';
  static const String RouteScanHistory = '/scanhistory';

  static const String RoutePaymentDetail = '/paymentdetail';
  static const String RoutePaymentSummary = '/paymentsummary';

  static const String RoutePaymentList = '/paymentlist';
  static const String RoutePaymentChart = '/paymentchart';

  static const String RouteCharge = '/charge';
  static const String RouteChargeDetail = '/chargedetail';
  static const String RouteChargeSuccess = '/chargesuccess';

  static const String RouteSelectReceipt = '/selectreceipt';

  
}
