class Config {
  // version
  static const String version = "1.00.01";

  // Api
  static const String apiUrl = "http://150.107.220.70/sf_api";
  static const String bankUiUrl = "http://150.107.220.70:23450/kbank";
    // Numbers
  static const int timeOutDuration = 90;
}