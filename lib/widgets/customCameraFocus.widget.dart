import 'package:flutter/material.dart';

class CustomCameraFocus {
  CustomCameraFocus._();

  static Widget idCard({Color color, double width, double height}) =>
      _IdCard(color: color, width: width, height: height);

  static Widget human({Color color, double width, double height}) =>
      _Human(color: color, width: width, height: height);
}

class _IdCard extends StatelessWidget {
  final Color color;
  final double width;
  final double height;
  const _IdCard({Key key, this.color, this.width, this.height})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: ClipPath(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: color,
        ),
        clipper: _IdCardPhoto(
          width: width,
          height: height,
        ),
      ),
    );
  }
}

class _IdCardPhoto extends CustomClipper<Path> {
  final double width;
  final double height;
  const _IdCardPhoto({this.width, this.height});
  @override
  Path getClip(Size size) {
    var path = Path();
    var reactPath = Path();

    double hLeft = ((size.width - (size.width * width).roundToDouble()) / 2);
    double hRight =
        size.width - ((size.width - (size.width * width).roundToDouble()) / 2);

    double vTop = ((size.height - (size.height * height).roundToDouble()) / 2);
    double vBottom = size.height -
        ((size.height - (size.height * height).roundToDouble()) / 2);

    reactPath.moveTo(hLeft, vTop);

    reactPath.lineTo(hRight, vTop);
    reactPath.lineTo(hRight, vBottom);
    reactPath.lineTo(hLeft, vBottom);

    path.addPath(reactPath, Offset(0, 0));
    path.addRect(Rect.fromLTWH(0.0, 0.0, size.width, size.height));
    path.fillType = PathFillType.evenOdd;

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class _Human extends StatelessWidget {
  final Color color;
  final double width;
  final double height;
  const _Human({Key key, this.color, this.width, this.height})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Stack(
        children: <Widget>[
          Center(
            child: Container(
              padding: EdgeInsets.only(top: 5),
              width:
                  (MediaQuery.of(context).size.width * width).roundToDouble(),
              height:
                  (MediaQuery.of(context).size.height * height).roundToDouble(),
              child: Opacity(
                opacity: 0.4,
                child: Image.asset('assets/images/human.png'),
              ),
            ),
          ),
          ClipPath(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: color,
            ),
            clipper: _HumanPhoto(
              width: width,
              height: height,
            ),
          ),
        ],
      ),
    );
  }
}

class _HumanPhoto extends CustomClipper<Path> {
  final double width;
  final double height;
  const _HumanPhoto({this.width, this.height});
  @override
  Path getClip(Size size) {
    var path = Path();
    var reactPath = Path();

    double hLeft = ((size.width - (size.width * width).roundToDouble()) / 2);
    double hRight =
        size.width - ((size.width - (size.width * width).roundToDouble()) / 2);

    double vTop = ((size.height - (size.height * height).roundToDouble()) / 2);
    double vBottom = size.height -
        ((size.height - (size.height * height).roundToDouble()) / 2);

    reactPath.moveTo(hLeft, vTop);

    reactPath.lineTo(hRight, vTop);
    reactPath.lineTo(hRight, vBottom);
    reactPath.lineTo(hLeft, vBottom);

    path.addPath(reactPath, Offset(0, 0));
    path.addRect(Rect.fromLTWH(0.0, 0.0, size.width, size.height));
    path.fillType = PathFillType.evenOdd;

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
