import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef PickerSelectedCallback = void Function(CustomPicker picker);

typedef PickerConfirmCallback = void Function(CustomPicker picker);

class CustomPicker {
  static const double DefaultTextSize = 20.0;
  final Widget child;
  final VoidCallback onCancel;
  final PickerSelectedCallback onSelect;
  final PickerConfirmCallback onConfirm;
  final double heightFactor;
  final Widget title;
  final Widget cancel;
  final Widget confirm;
  final String cancelText;
  final String confirmText;
  final bool hideHeader;
  final Widget footer;
  Widget _widget;
  CustomPicker({
    this.child,
    this.title,
    this.cancel,
    this.confirm,
    this.cancelText = "Cancel",
    this.confirmText = "Confirm",
    this.hideHeader = false,
    this.footer,
    this.onCancel,
    this.onSelect,
    this.onConfirm,
    this.heightFactor = 0.0,
  }) : assert(child != null);

  Widget makePicker([ThemeData themeData, bool isModal = false]) {
    _widget = CustomPickerWidget(picker: this);
    return _widget;
  }

  Future<T> showModal<T>(BuildContext context, [ThemeData themeData]) async {
    return await showModalBottomSheet<T>(
        isScrollControlled: true,
        context: context, //state.context,
        builder: (BuildContext context) {
          // if (heightFactor

          return (heightFactor == 0.0)
              ? makePicker(themeData, true)
              : FractionallySizedBox(
                  heightFactor: heightFactor,
                  child: makePicker(themeData, true));
        });
  }

  void doCancel(BuildContext context) {
    if (onCancel != null) onCancel();
    Navigator.of(context).pop();
    _widget = null;
  }

  void doConfirm(BuildContext context) {
    if (onConfirm != null) onConfirm(this);
    Navigator.of(context).pop();
    _widget = null;
  }
}

class CustomPickerWidget extends StatefulWidget {
  final CustomPicker picker;
  final bool isModal;
  CustomPickerWidget({Key key, this.picker, this.isModal}) : super(key: key);

  @override
  _CustomPickerWidgetState createState() => _CustomPickerWidgetState();
}

class _CustomPickerWidgetState extends State<CustomPickerWidget> {
  @override
  Widget build(BuildContext context) {
    var v = Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        (widget.picker.hideHeader)
            ? SizedBox()
            : Container(
                child: Row(
                  children: _buildHeaderViews(),
                ),
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 0.5),
                    bottom: BorderSide(width: 0.5),
                  ),
                ),
              ),
        widget.picker.child,
        widget.picker.footer ?? SizedBox(width: 0.0, height: 0.0),
      ],
    );
    if (widget.isModal == null || widget.isModal == false) return v;
    return GestureDetector(
      onTap: () {},
      child: v,
    );
  }

  List<Widget> _buildHeaderViews() {
    // Theme.of(context);
    List<Widget> items = [];

    items.add(Container(
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: <Widget>[
          Container(
            height: 45,
            width: double.infinity,
            alignment: Alignment.center,
            child: widget.picker.title == null
                ? widget.picker.title
                : DefaultTextStyle(
                    style: TextStyle(
                      fontSize: CustomPicker.DefaultTextSize,
                    ),
                    child: widget.picker.title),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Center(
                child: FlatButton(
                  onPressed: () {
                    widget.picker.doCancel(context);
                  },
                  child: Icon(
                    Icons.close,
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    ));

    return items;
  }
}
