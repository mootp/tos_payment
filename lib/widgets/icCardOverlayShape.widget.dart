import 'package:flutter/material.dart';

class IdCardOverlayShape extends ShapeBorder {
  IdCardOverlayShape({
    this.borderColor = Colors.red,
    this.borderWidth = 3.0,
    this.overlayColor = const Color.fromRGBO(0, 0, 0, 80),
    this.borderRadius = 0,
    this.borderLength = 40,
    this.widthSize = 300,
    this.heightSize = 200,
  });

  final Color borderColor;
  final double borderWidth;
  final Color overlayColor;
  final double borderRadius;
  final double borderLength;
  final double widthSize;
  final double heightSize;

  @override
  EdgeInsetsGeometry get dimensions => const EdgeInsets.all(10);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) {
    return Path()
      ..fillType = PathFillType.evenOdd
      ..addPath(getOuterPath(rect), Offset.zero);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    Path _getLeftTopPath(Rect rect) {
      return Path()
        ..moveTo(rect.left, rect.bottom)
        ..lineTo(rect.left, rect.top)
        ..lineTo(rect.right, rect.top);
    }

    return _getLeftTopPath(rect)
      ..lineTo(
        rect.right,
        rect.bottom,
      )
      ..lineTo(
        rect.left,
        rect.bottom,
      )
      ..lineTo(
        rect.left,
        rect.top,
      );
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {
    final width = rect.width;
    final height = rect.height;

    // final _borderLength = borderLength > cutOutSize / 2 + borderWidth * 2
    //     ? borderWidthSize / 2
    //     : borderLength;
    // final _cutOutSize = cutOutSize != null && cutOutSize < width
    //     ? cutOutSize
    //     : width - borderOffset;

    // final backgroundPaint = Paint()
    //   ..color = overlayColor
    //   ..style = PaintingStyle.fill;

    final borderPaint = Paint()
      ..color = borderColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    // final borderLine = Paint()
    //   ..color = Color.fromRGBO(borderColor.red, borderColor.green, borderColor.blue, 95)
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = (borderWidth / 2);

    final boxPaint = Paint()
      ..color = borderColor
      ..style = PaintingStyle.fill
      ..blendMode = BlendMode.dstOut;

    final cutOutRect = Rect.fromLTWH(
      rect.left + borderWidth + (width / 2) - (widthSize / 2),
      rect.top + borderWidth + (height / 2) - (heightSize / 2),
      widthSize - borderWidth * 2,
      heightSize - borderWidth * 2,
    );

    canvas
      // ..drawLine(
      //   Offset(
      //     borderWidth + (width / 2) - (widthSize / 2),
      //     rect.top + (height / 2),
      //   ),
      //   Offset(
      //     (widthSize - borderWidth) + (width / 2) - (widthSize / 2),
      //     rect.top + (height / 2),
      //   ),
      //   borderLine,
      // )
      // ..saveLayer(
      //   rect,
      //   backgroundPaint,
      // )
      // ..drawRect(
      //   rect,
      //   backgroundPaint,
      // )

      // Draw top right corner
      ..drawRRect(
        RRect.fromLTRBAndCorners(
          cutOutRect.right - borderWidth,
          cutOutRect.top,
          cutOutRect.right,
          cutOutRect.top + borderWidth,
          topRight: Radius.circular(borderRadius),
        ),
        borderPaint,
      )
      // Draw top left corner
      ..drawRRect(
        RRect.fromLTRBAndCorners(
          cutOutRect.left,
          cutOutRect.top,
          cutOutRect.left + borderWidth,
          cutOutRect.top + borderWidth,
          topLeft: Radius.circular(borderRadius),
        ),
        borderPaint,
      )
      // Draw bottom right corner
      ..drawRRect(
        RRect.fromLTRBAndCorners(
          cutOutRect.right - borderWidth,
          cutOutRect.bottom - borderWidth,
          cutOutRect.right,
          cutOutRect.bottom,
          bottomRight: Radius.circular(borderRadius),
        ),
        borderPaint,
      )
      // Draw bottom left corner
      ..drawRRect(
        RRect.fromLTRBAndCorners(
          cutOutRect.left,
          cutOutRect.bottom - borderWidth,
          cutOutRect.left + borderWidth,
          cutOutRect.bottom,
          bottomLeft: Radius.circular(borderRadius),
        ),
        borderPaint,
      )
      ..drawRRect(
        RRect.fromRectAndRadius(
          cutOutRect,
          Radius.circular(borderRadius),
        ),
        boxPaint,
      )
      ..restore();
  }

  @override
  ShapeBorder scale(double t) {
    return IdCardOverlayShape(
      borderColor: borderColor,
      borderWidth: borderWidth,
      overlayColor: overlayColor,
    );
  }
}
