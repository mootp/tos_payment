import 'package:flutter/material.dart';
import 'package:tos_payment/pages/splash.page.dart';
import 'package:tos_payment/routes.dart' as router;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SF-TOS',
      theme: ThemeData(
        primarySwatch: Colors.red,
        // accentColor: Colors.white,
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.red,
          textTheme: ButtonTextTheme.primary,
        ),
      ),
      home: SplashPage(),
      onGenerateRoute: router.generateRoute,
      // onGenerateInitialRoutes: router.generateInitialRoutes,
    );
  }
}
