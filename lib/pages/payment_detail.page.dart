import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tos_payment/blocs/payment.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/charge.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/models/station.model.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:tos_payment/widgets/custom_picker.widget.dart';

import 'package:tos_payment/models/enum.model.dart';

class PaymentDetailPage extends StatefulWidget {
  PaymentDetailPage({Key key, this.arguments}) : super(key: key);
  final PaymentArguments arguments;
  @override
  _PaymentDetailPageState createState() => _PaymentDetailPageState();
}

class _PaymentDetailPageState extends State<PaymentDetailPage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');

  PaymentBloc _paymentBloc;
  @override
  void initState() {
    _paymentBloc = PaymentBloc();
    _paymentBloc.nameEvent.add(widget.arguments.name);
    _paymentBloc.taxidEvent.add(widget.arguments.taxId);
    _paymentBloc.branchEvent.add(widget.arguments.branch);
    _paymentBloc.addressEvent.add(widget.arguments.address);
    _paymentBloc.stationEvent.add(widget.arguments.station);
    _paymentBloc.methodEvent.add(widget.arguments.method);

    _paymentBloc.listawbEvent.add(widget.arguments.listAwb);
    _paymentBloc.loadstation();
    super.initState();
  }

  @override
  void dispose() {
    _paymentBloc?.dispose();
    super.dispose();
  }

  Widget inputname() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('Name'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _paymentBloc.nameTextEditingControllerStream,
              builder: (context, textEditingController) {
                return StreamBuilder(
                  stream: _paymentBloc.nameStream,
                  builder: (context, snapshot) {
                    return TextFormField(
                      controller: textEditingController.data,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      decoration: InputDecoration(
                        errorText: snapshot.error,
                        hintText: 'ชื่อลูกค้า หรือ ชื่อบริษัท',
                      ),
                      onChanged: _paymentBloc.nameEvent.add,
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget inputbranch() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('Branch'),
          ),
          Expanded(
            flex: 3,
            child: MultiProvider(
              providers: [
                StreamProvider<TextEditingController>.value(
                    value: _paymentBloc.branchTextEditingControllerStream),
                StreamProvider<String>.value(value: _paymentBloc.branchStream),
              ],
              child: Builder(
                builder: (context) {
                  TextEditingController controller =
                      Provider.of<TextEditingController>(context);
                  String branch = Provider.of<String>(context);
                  if (controller != null && branch != null) {
                    return TextFormField(
                      controller: controller,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: 'สาขา',
                      ),
                      onChanged: _paymentBloc.branchEvent.add,
                    );
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget inputtaxid() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('Tax ID'),
          ),
          Expanded(
            flex: 3,
            child: MultiProvider(
              providers: [
                StreamProvider<TextEditingController>.value(
                    value: _paymentBloc.taxidTextEditingControllerStream),
                StreamProvider<String>.value(value: _paymentBloc.taxidStream),
              ],
              child: Builder(
                builder: (context) {
                  TextEditingController controller =
                      Provider.of<TextEditingController>(context);
                  String taxid = Provider.of<String>(context);
                  if (controller != null && taxid != null) {
                    return TextFormField(
                      controller: controller,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: 'เลขประจำตัวผู้เสียภาษีอากร',
                      ),
                      onChanged: _paymentBloc.taxidEvent.add,
                    );
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget inputaddress() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('Address'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _paymentBloc.addressTextEditingControllerStream,
              builder: (context, textEditingController) {
                return StreamBuilder(
                  stream: _paymentBloc.addressStream,
                  builder: (context, snapshot) {
                    return TextFormField(
                      controller: textEditingController.data,
                      keyboardType: TextInputType.text,
                      maxLines: 2,
                      decoration: InputDecoration(
                        errorText: snapshot.error,
                        hintText: 'ที่อยู่ลูกค้า หรือ ที่อยู่บริษัท',
                      ),
                      onChanged: _paymentBloc.addressEvent.add,
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget inputstation() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('Station'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _paymentBloc.stationTextEditingControllerStream,
              builder: (context, textEditingController) {
                return StreamBuilder(
                  stream: _paymentBloc.stationStream,
                  builder: (context, snapshot) {
                    return TextFormField(
                      controller: textEditingController.data,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      decoration: InputDecoration(
                        errorText: snapshot.error,
                        hintText: 'สาขาที่ออกใบเสร็จ',
                      ),
                      readOnly: true,
                      onChanged: _paymentBloc.stationEvent.add,
                      onTap: () {
                        
                        return CustomPicker(
                          hideHeader: false,
                          title: Text(
                            'Station',
                            style: TextStyle(color: Colors.black),
                          ),
                          heightFactor: 0.8,
                          child: Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: StreamBuilder(
                                stream: _paymentBloc.liststationStream,
                                builder: (context, snapshot) {

                                  List<StationData> list = snapshot.data;
                                  if (snapshot.hasData) {
                                    return ListView.separated(
                                      itemCount: list.length,
                                      separatorBuilder: (context, index) =>
                                          Divider(),
                                      itemBuilder: (context, index) {
                                        String code = list[index].stationCode;
                                        String text = list[index].stationCode + ' - ' + list[index].stationName;

                                        bool chk =
                                            _paymentBloc.checkStationCode(code);

                                        return ListTile(
                                          onTap: () {
                                            _paymentBloc.selectstation(code);
                                            Navigator.pop(context);
                                          },
                                          title: Text(text),
                                          subtitle: Text(code),
                                          trailing: chk
                                              ? Icon(
                                                  Icons.check_box,
                                                  color: Colors.red,
                                                )
                                              : Icon(
                                                  Icons.check_box_outline_blank,
                                                  color: Colors.grey,
                                                ),
                                        );
                                      },
                                    );
                                  } else {
                                    return Center(
                                      child: SizedBox(
                                        width: 40.0,
                                        height: 40.0,
                                        child:
                                            const CircularProgressIndicator(),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ),
                        ).showModal(context);
                      },
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget inputmethod() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('Method'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _paymentBloc.methodTextEditingControllerStream,
              builder: (context, textEditingController) {
                return StreamBuilder(
                  stream: _paymentBloc.methodStream,
                  builder: (context, snapshot) {
                    return TextFormField(
                      controller: textEditingController.data,
                      keyboardType: TextInputType.text,
                      maxLines: 1,
                      decoration: InputDecoration(
                        errorText: snapshot.error,
                        hintText: 'ประเภทการเก็บเงิน',
                      ),
                      readOnly: true,
                      onChanged: _paymentBloc.methodEvent.add,
                      onTap: () {
                        _paymentBloc.loadmethod();
                        return CustomPicker(
                          hideHeader: false,
                          title: Text(
                            'Method',
                            style: TextStyle(color: Colors.black),
                          ),
                          heightFactor: 0.8,
                          child: Flexible(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: StreamBuilder(
                                stream: _paymentBloc.listmethodStream,
                                builder: (context, snapshot) {
                                  List<CodeText> list = snapshot.data;
                                  if (snapshot.hasData) {
                                    return ListView.separated(
                                      itemCount: list.length,
                                      separatorBuilder: (context, index) =>
                                          Divider(),
                                      itemBuilder: (context, index) {
                                        String code = list[index].code;
                                        String text = list[index].text;

                                        bool chk =
                                            _paymentBloc.checkMethod(code);

                                        return ListTile(
                                          onTap: () {
                                            _paymentBloc.selectmethod(code);
                                            Navigator.pop(context);
                                          },
                                          title: Text(text),
                                          subtitle: Text(code),
                                          trailing: chk
                                              ? Icon(
                                                  Icons.check_box,
                                                  color: Colors.red,
                                                )
                                              : Icon(
                                                  Icons.check_box_outline_blank,
                                                  color: Colors.grey,
                                                ),
                                        );
                                      },
                                    );
                                  } else {
                                    return Center(
                                      child: SizedBox(
                                        width: 40.0,
                                        height: 40.0,
                                        child:
                                            const CircularProgressIndicator(),
                                      ),
                                    );
                                  }
                                },
                              ),
                            ),
                          ),
                        ).showModal(context);
                      },
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  listPrice(
    String txtTitle,
    String subTitle,
    String awb,
    double price,
    PriceType type,
  ) {
    return ListTile(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            txtTitle,
          ),
          Text(
            subTitle,
            style: TextStyle(
                fontSize: Theme.of(context).textTheme.subtitle1.fontSize * 0.9),
          ),
        ],
      ),
      trailing: Padding(
        padding: const EdgeInsets.only(top: 6.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(numberFormat.format(price).toString()),
                InkWell(
                  child: Icon(Icons.edit),
                  onTap: () {
                    _paymentBloc.priceEvent.add(price);
                    TextEditingController _controller =
                        new TextEditingController(
                      text: price.toString(),
                    );
                    _controller.selection = TextSelection(
                      baseOffset: 0,
                      extentOffset: price.toString().length,
                    );
                    return showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('AWB : $awb'),
                              Text(txtTitle),
                              TextField(
                                controller: _controller,
                                autofocus: true,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  hintText: 'Price',
                                ),
                                onChanged: (txt) {
                                  _paymentBloc.priceEvent.add(
                                    double.parse(txt),
                                  );
                                },
                              ),
                            ],
                          ),
                          contentPadding: EdgeInsets.all(10.0),
                          actions: <Widget>[
                            FlatButton(
                              child: Text('ยกเลิก'),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            RaisedButton(
                              child: Text('บันทึก'),
                              onPressed: () async {
                                await _paymentBloc.editawbprice(awb, type);
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  listAwb() {
    return StreamBuilder(
      stream: _paymentBloc.listawbStream,
      builder: (context, snapshot) {
        List<Widget> list = [];

        if (snapshot.hasData) {
          List<AwbData> listAwb = snapshot.data;

          double total = 0.0;
          listAwb.forEach((item) {
            double totalRow = item.itemAdvance +
                item.itemFreight +
                item.itemService +
                item.itemServiceVat;
            total += totalRow;
          });

          list.add(
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  CircleAvatar(
                    child: Text(listAwb.length.toString()),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        'List AWB',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Text(
                    'ราคา',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 35,
                  ),
                ],
              ),
            ),
          );

          list.add(
            Divider(),
          );

          list.add(
            Flexible(
              child: ListView.separated(
                itemCount: listAwb.length,
                separatorBuilder: (context, index) => Divider(),
                itemBuilder: (context, index) {
                  String hawbNo = listAwb[index].hawbNo;
                  double itemAdvance = listAwb[index].itemAdvance;
                  double itemFreight = listAwb[index].itemFreight;
                  double itemService = listAwb[index].itemService;
                  double itemServiceVat = listAwb[index].itemServiceVat;
                  // double total =
                  //     itemAdvance + itemFreight + itemService + itemServiceVat;
                  return ListTile(
                    leading: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text((index + 1).toString()),
                        VerticalDivider(),
                      ],
                    ),
                    title: Text(
                      hawbNo,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    subtitle: Column(
                      children: <Widget>[
                        (itemFreight > 0 ||
                                widget.arguments.type == ProcessType.pickup)
                            ? listPrice(
                                'FREIGHT CHARGE',
                                '',
                                hawbNo,
                                itemFreight,
                                PriceType.freight,
                              )
                            : Container(),
                        itemAdvance > 0
                            ? listPrice(
                                'ADVANCE PAYMENT',
                                '(CUSTOM TAX / FUEL / OTHER)',
                                hawbNo,
                                itemAdvance,
                                PriceType.advance,
                              )
                            : Container(),
                        itemService > 0
                            ? listPrice(
                                'SERVICES',
                                '(EDI / STORAGE / OTHER)',
                                hawbNo,
                                itemService,
                                PriceType.service,
                              )
                            : Container(),
                        itemServiceVat > 0
                            ? listPrice(
                                'SERVICES',
                                '(VAT 7%)',
                                hawbNo,
                                itemServiceVat,
                                PriceType.vat,
                              )
                            : Container(),
                      ],
                    ),
                  );
                },
              ),
            ),
          );

          list.add(
            Divider(),
          );

          list.add(
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              child: Row(
                // mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    numberFormat.format(total).toString(),
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 10.0),
                  Text(
                    'THB',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          );

          list.add(
            Divider(),
          );
        } else {
          list.add(
            Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        return Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Column(
              children: list,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'ข้อมูลการชำระเงิน',
        ),
      ),
      body: Column(
        children: <Widget>[
          inputtaxid(),
          inputbranch(),
          inputname(),
          inputaddress(),
          inputstation(),
          // inputmethod(),
          listAwb(),
        ],
      ),
      bottomNavigationBar: StreamBuilder(
          stream: _paymentBloc.detailValid,
          builder: (context, snapshot) {
            return RaisedButton(
              // color: Theme.of(context).primaryColor,
              onPressed: !snapshot.hasData
                  ? null
                  : () async {
                      PaymentSummaryArguments arguments =
                          await _paymentBloc.paymentSummaryArguments();
                      if (arguments != null) {
                        Navigator.of(context).pushNamed(
                          RouteData.RoutePaymentSummary,
                          arguments: arguments,
                        );
                      }
                    },
              child: Text('ถัดไป'),
            );
          }),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _paymentBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
