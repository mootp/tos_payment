import 'package:flutter/material.dart';
import 'package:tos_payment/blocs/payment.bloc.dart';
import 'package:tos_payment/models/api_tos.model.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/utils/routes.util.dart';

class SelectReceiptPage extends StatefulWidget {
  SelectReceiptPage({Key key, this.arguments}) : super(key: key);
  final PaymentArguments arguments;
  @override
  _SelectReceiptPageState createState() => _SelectReceiptPageState();
}

class _SelectReceiptPageState extends State<SelectReceiptPage> {
  PaymentBloc _paymentBloc;
  @override
  void initState() {
    _paymentBloc = PaymentBloc();
    _paymentBloc.listawbEvent.add(widget.arguments.listAwb);
    _paymentBloc.loadReceiptList();
    super.initState();
  }

  @override
  void dispose() {
    _paymentBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      appBar: AppBar(
        title: Text('เลือกเลขที่ใบเสร็จ'),
      ),
      body: StreamBuilder(
          stream: _paymentBloc.listreceiptStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<ReceiptListData> list = snapshot.data;
              return ListView.separated(
                itemCount: list.length,
                separatorBuilder: (context, index) => Divider(),
                itemBuilder: (context, index) {
                  ReceiptListData data = list[index];
                  List<AwbData> listAwb = [];
                  String awb = "";
                  data.d.forEach((element) {
                    awb += (awb == '' ? '' : ', ') + element.hawbNo;
                    listAwb.add(
                      AwbData(
                        hawbNo: element.hawbNo,
                        itemAdvance: element.itemAdvance ?? 0,
                        itemFreight: element.itemFreight ?? 0,
                        itemService: element.itemService ?? 0,
                        itemServiceVat: element.itemServiceVat ?? 0,
                      ),
                    );
                  });

                  return ListTile(
                    onTap: () async {
                      PaymentSummaryArguments arguments =
                          PaymentSummaryArguments(
                        paymentCode: data.h.paymentCode,
                        receiptCode: data.h.receiptCode,
                        taxId: data.h.receiptTaxId,
                        branch: data.h.receiptBranch,
                        name: data.h.receiptName,
                        address: data.h.receiptAddress,
                        station: data.h.receiptStation,
                        process: 'DELIVERY',
                        listAwb: listAwb,
                      );
                      if (arguments != null) {
                        Navigator.of(context).pushNamed(
                          RouteData.RoutePaymentSummary,
                          arguments: arguments,
                        );
                      }
                    },
                    leading: Text((index + 1).toString()),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('เลขที่ใบเสร็จ : '),
                        Text(
                          data.h.receiptCode,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('เลขที่ AWB : '),
                        Text(
                          awb,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _paymentBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
