import 'package:flutter/material.dart';
import 'package:tos_payment/blocs/charge.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:url_launcher/url_launcher.dart';

class ChargeSuccessPage extends StatefulWidget {
  ChargeSuccessPage({Key key, this.arguments}) : super(key: key);
  final ChargeSuccessArguments arguments;
  @override
  _ChargeSuccessPageState createState() => _ChargeSuccessPageState();
}

class _ChargeSuccessPageState extends State<ChargeSuccessPage> {
  ChargeBloc _chargeBloc;
  @override
  void initState() {
    _chargeBloc = ChargeBloc();
    super.initState();
  }

  @override
  void dispose() {
    _chargeBloc?.dispose();
    super.dispose();
  }

  openPdf(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      appBar: AppBar(
        title: Text(
          'ชำระเงินสำเร็จ',
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              widget.arguments.payment.paymentStatus != 'PAID'
                  ? RaisedButton(
                      padding: const EdgeInsets.all(20.0),
                      child: Text('ชำระเพิ่ม'),
                      onPressed: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                          RouteData.RouteRoot,
                          ModalRoute.withName('/'),
                        );

                        Navigator.of(context)
                            .pushNamed(RouteData.RoutePaymentList);
                        Navigator.of(context).pushNamed(
                          RouteData.RouteChargeDetail,
                          arguments: ChargeDetailArguments(
                            paymentCode: widget.arguments.payment.paymentCode,
                          ),
                        );
                      },
                    )
                  : Container(),
              (widget.arguments.payment.receiptCode != null &&
                      widget.arguments.payment.receiptCode != '')
                  ? RaisedButton(
                      padding: const EdgeInsets.all(20.0),
                      child: Text('ใบเสร็จ'),
                      onPressed: () async {
                        String url = await _chargeBloc
                            .receiptUrl(widget.arguments.payment.receiptCode);
                        openPdf(url);
                      },
                    )
                  : Container(),
              SizedBox(
                height: 10.0,
              ),
              OutlineButton(
                padding: const EdgeInsets.all(20.0),
                child: Text('กลับหน้าหลัก'),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    RouteData.RouteRoot,
                    ModalRoute.withName('/'),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _chargeBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}

// paymnetSuccess(PaymentUrl payment) {
//     Navigator.of(context).pushNamedAndRemoveUntil(
//       RouteData.RouteRoot,
//       ModalRoute.withName('/'),
//     );

//     Navigator.of(context).pushNamed(
//       RouteData.RouteChargeDetail,
//       arguments: ChargeSuccessArguments(
//         payment: payment,
//       ),
//     );
//   }
