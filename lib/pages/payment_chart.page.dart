import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tos_payment/blocs/payment.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';

class PaymentChartPage extends StatefulWidget {
  PaymentChartPage({Key key, this.arguments}) : super(key: key);
  final PaymentChartArguments arguments;
  @override
  _PaymentChartPageState createState() => _PaymentChartPageState();
}

class _PaymentChartPageState extends State<PaymentChartPage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');
  PaymentBloc _paymentBloc;
  @override
  void initState() {
    _paymentBloc = PaymentBloc();

    super.initState();
  }

  @override
  void dispose() {
    _paymentBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'รายงานสรุปรายวัน',
        ),
      ),
      body: Container(),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _paymentBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
