import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:intl/intl.dart';
import 'package:tos_payment/blocs/attach.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/pages/photoView.page.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:camera_camera/camera_camera.dart';
import 'package:tos_payment/widgets/customCameraFocus.widget.dart';

class ScanVerifyAttachPage extends StatefulWidget {
  ScanVerifyAttachPage({Key key, this.arguments}) : super(key: key);
  final AttachArguments arguments;
  @override
  _ScanVerifyAttachPageState createState() => _ScanVerifyAttachPageState();
}

class _ScanVerifyAttachPageState extends State<ScanVerifyAttachPage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');

  AttachBloc _attachBloc;
  @override
  void initState() {
    _attachBloc = AttachBloc();
    _attachBloc.addAwbList(widget.arguments.listAwb);
    super.initState();
  }

  @override
  void dispose() {
    _attachBloc?.dispose();
    super.dispose();
  }

  Future<Size> _calculateImageDimension(File file) {
    Completer<Size> completer = Completer();
    Image image = Image.file(file);
    image.image.resolve(ImageConfiguration()).addListener(
      ImageStreamListener(
        (ImageInfo image, bool synchronousCall) {
          var myImage = image.image;
          Size size = Size(myImage.width.toDouble(), myImage.height.toDouble());
          completer.complete(size);
        },
      ),
    );
    return completer.future;
  }

  Future<File> cropImage(File pic, double w, double h) async {
    ImageProperties imgProperties =
        await FlutterNativeImage.getImageProperties(pic.path);

    w = w * 1.05;
    h = h * 1.05;

    double width = imgProperties.width.toDouble();
    double height = imgProperties.height.toDouble();
    int widthSize = (width * (width > height ? h : w)).round();
    int heightSize = (height * (width > height ? w : h)).round();

    // int originX = ((width - widthSize) / 2).round();
    // int originY = ((height - heightSize) / 2).round();

    File croppedFile = await FlutterNativeImage.cropImage(
      pic.path,
      ((width - widthSize) / 2).round(),
      ((height - heightSize) / 2).round(),
      widthSize,
      heightSize,
    );

    // File croppedFile = await FlutterNativeImage.cropImage(
    //   pic.path,
    //   originX,
    //   originY,
    //   widthSize,
    //   heightSize,
    // );

    return croppedFile;
  }

  imagesViewCard(List<File> images) {
    double size = 80;
    List<Widget> list = [];

    List<ImageProvider> listp = [];
    int i = 0;

    images.forEach(
      (img) {
        list.add(
          Card(
            clipBehavior: Clip.antiAlias,
            child: Stack(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PhotoViewWrapper(
                          galleryItems: listp,
                          backgroundDecoration: const BoxDecoration(
                            color: Colors.black,
                          ),
                          initialIndex: i,
                          // scrollDirection: verticalGallery ? Axis.vertical : Axis.horizontal,
                        ),
                      ),
                    );
                  },
                  child: Image.file(
                    img,
                    height: size,
                    width: size,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  right: 5,
                  top: 5,
                  child: InkWell(
                    child: Icon(
                      Icons.remove_circle,
                      size: 20,
                      color: Colors.red,
                    ),
                    onTap: () {
                      _attachBloc.removeImageCard(img.path);
                    },
                  ),
                ),
              ],
            ),
          ),
        );

        listp.add(FileImage(img));
        i++;
      },
    );

    if (images.length < 2) {
      list.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () async {
                  var camera = Camera(
                    mode: CameraMode.normal,
                    initialCamera: CameraSide.back,
                    orientationEnablePhoto: CameraOrientation.portrait,
                    imageMask: CustomCameraFocus.idCard(
                      color: Colors.black.withOpacity(0.5),
                      width: 0.80,
                      height: 0.27,
                    ),
                  );
                  File pic = await showDialog(
                    context: context,
                    builder: (context) => camera,
                  );
                  if (pic != null) {
                    File crop = await cropImage(pic, 0.80, 0.27);
                    _attachBloc.addImageCard(crop);
                  }
                },
                child: Container(
                  color: Colors.grey[300],
                  width: double.parse(size.toString()),
                  height: double.parse(size.toString()),
                  child: Icon(
                    Icons.add_a_photo,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
    return list;
  }

  imagesViewPhoto(List<File> images) {
    double size = 80;
    List<Widget> list = [];

    List<ImageProvider> listp = [];
    int i = 0;

    images.forEach(
      (img) {
        list.add(
          Card(
            clipBehavior: Clip.antiAlias,
            child: Stack(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PhotoViewWrapper(
                          galleryItems: listp,
                          backgroundDecoration: const BoxDecoration(
                            color: Colors.black,
                          ),
                          initialIndex: i,
                          // scrollDirection: verticalGallery ? Axis.vertical : Axis.horizontal,
                        ),
                      ),
                    );
                  },
                  child: Image.file(
                    img,
                    height: size,
                    width: size,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  right: 5,
                  top: 5,
                  child: InkWell(
                    child: Icon(
                      Icons.remove_circle,
                      size: 20,
                      color: Colors.red,
                    ),
                    onTap: () {
                      _attachBloc.removeImagePhoto(img.path);
                    },
                  ),
                ),
              ],
            ),
          ),
        );

        listp.add(FileImage(img));
        i++;
      },
    );

    if (images.length < 2) {
      list.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () async {
                  File pic = await showDialog(
                    context: context,
                    builder: (context) => Camera(
                      mode: CameraMode.normal,
                      initialCamera: CameraSide.back,
                      orientationEnablePhoto: CameraOrientation.portrait,
                      imageMask: CustomCameraFocus.human(
                        color: Colors.black.withOpacity(0.5),
                        width: 0.90,
                        height: 0.60,
                      ),
                    ),
                  );
                  if (pic != null) {
                    File crop = await cropImage(pic, 0.90, 0.60);
                    _attachBloc.addImagePhoto(crop);
                  }
                },
                child: Container(
                  color: Colors.grey[300],
                  width: double.parse(size.toString()),
                  height: double.parse(size.toString()),
                  child: Icon(
                    Icons.add_a_photo,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return list;
  }

  imagesViewOther(List<File> images) {
    double size = 80;
    List<Widget> list = [];

    List<ImageProvider> listp = [];
    int i = 0;

    images.forEach(
      (img) {
        list.add(
          Card(
            clipBehavior: Clip.antiAlias,
            child: Stack(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PhotoViewWrapper(
                          galleryItems: listp,
                          backgroundDecoration: const BoxDecoration(
                            color: Colors.black,
                          ),
                          initialIndex: i,
                          // scrollDirection: verticalGallery ? Axis.vertical : Axis.horizontal,
                        ),
                      ),
                    );
                  },
                  child: Image.file(
                    img,
                    height: size,
                    width: size,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  right: 5,
                  top: 5,
                  child: InkWell(
                    child: Icon(
                      Icons.remove_circle,
                      size: 20,
                      color: Colors.red,
                    ),
                    onTap: () {
                      _attachBloc.removeImageOther(img.path);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
        listp.add(FileImage(img));
        i++;
      },
    );

    if (images.length < 5) {
      list.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () async {
                  File pic = await showDialog(
                    context: context,
                    builder: (context) => Camera(
                      mode: CameraMode.normal,
                      initialCamera: CameraSide.back,
                    ),
                  );
                  if (pic != null) {
                    _attachBloc.addImageOther(pic);
                  }
                },
                child: Container(
                  color: Colors.grey[300],
                  width: double.parse(size.toString()),
                  height: double.parse(size.toString()),
                  child: Icon(
                    Icons.add_a_photo,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return list;
  }

  imagesViewAwb(List<File> images, String awb) {
    double size = 50;
    List<Widget> list = [];

    List<ImageProvider> listp = [];
    int i = 0;

    images.forEach(
      (img) {
        list.add(
          Card(
            clipBehavior: Clip.antiAlias,
            child: Stack(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PhotoViewWrapper(
                          galleryItems: listp,
                          backgroundDecoration: const BoxDecoration(
                            color: Colors.black,
                          ),
                          initialIndex: i,
                          // scrollDirection: verticalGallery ? Axis.vertical : Axis.horizontal,
                        ),
                      ),
                    );
                  },
                  child: Image.file(
                    img,
                    height: size,
                    width: size,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  right: 5,
                  top: 5,
                  child: InkWell(
                    child: Icon(
                      Icons.remove_circle,
                      size: 20,
                      color: Colors.red,
                    ),
                    onTap: () {
                      _attachBloc.removeImageAwb(img.path, awb);
                    },
                  ),
                ),
              ],
            ),
          ),
        );

        listp.add(FileImage(img));
        i++;
      },
    );

    if (images.length < 1) {
      list.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () async {
                  File pic = await showDialog(
                    context: context,
                    builder: (context) => Camera(
                      mode: CameraMode.normal,
                      initialCamera: CameraSide.back,
                    ),
                  );
                  if (pic != null) {
                    _attachBloc.addImageAwb(pic, awb);
                  }
                },
                child: Container(
                  color: Colors.grey[300],
                  width: double.parse(size.toString()),
                  height: double.parse(size.toString()),
                  child: Icon(
                    Icons.add_a_photo,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return list;
  }

  Widget attachCard() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text("บัตรประชาชน/Passport"),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _attachBloc.listImagesCardStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<File> images = snapshot.data;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Wrap(
                        children: imagesViewCard(images),
                      ),
                      (images.length <= 0
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Divider(
                                  color: Theme.of(context).errorColor,
                                  thickness: 1.0,
                                ),
                                Text(
                                  'กรุณาแนบรูปภาพ บัตรประชาชน หรือ Passport',
                                  style: TextStyle(
                                    color: Theme.of(context).errorColor,
                                    fontWeight: FontWeight.normal,
                                  ),
                                )
                              ],
                            )
                          : Container()),
                    ],
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget attachPhoto() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text("ผู้ส่งสินค้า"),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _attachBloc.listImagesPhotoStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<File> images = snapshot.data;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Wrap(
                        children: imagesViewPhoto(images),
                      ),
                      (images.length <= 0
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Divider(
                                  color: Theme.of(context).errorColor,
                                  thickness: 1.0,
                                ),
                                Text(
                                  'กรุณาแนบรูปภาพ ผู้ที่มาส่งของ',
                                  style: TextStyle(
                                    color: Theme.of(context).errorColor,
                                    fontWeight: FontWeight.normal,
                                  ),
                                )
                              ],
                            )
                          : Container()),
                    ],
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget attachOther() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text("รูปภาพเพิ่มเติม"),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _attachBloc.listImagesOtherStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<File> images = snapshot.data;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Wrap(
                        children: imagesViewOther(images),
                      ),
                    ],
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  listAwb() {
    return StreamBuilder(
        stream: _attachBloc.listawbStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Widget> list = [];

            List<AwbVerifyData> listAwb = snapshot.data;

            if (listAwb.length <= 0) {
              list.add(
                Text(
                  'กรุณาระบุเลข AWB',
                  style: TextStyle(
                    color: Theme.of(context).errorColor,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              );
            }

            list.add(
              Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    CircleAvatar(
                      child: Text(listAwb.length.toString()),
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          'List AWB',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );

            list.add(
              Divider(),
            );

            list.add(
              Flexible(
                child: ListView.separated(
                  itemCount: listAwb.length,
                  separatorBuilder: (context, index) => Divider(),
                  itemBuilder: (context, index) {
                    String hawbNo = listAwb[index].hawbNo;
                    List<File> listfile = listAwb[index].awbAttach;
                    return ListTile(
                      leading: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text((index + 1).toString()),
                          VerticalDivider(),
                        ],
                      ),
                      title: Text(
                        hawbNo,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      trailing: Wrap(
                        children: imagesViewAwb(listfile, hawbNo),
                      ),
                    );
                  },
                ),
              ),
            );

            list.add(
              Divider(),
            );

            return Flexible(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                child: Column(
                  children: list,
                ),
              ),
            );
          } else {
            return Container();
          }
        });
  }

  buttonSave() {
    return StreamBuilder(
      stream: _attachBloc.attachValid,
      builder: (context, snapshot) {
        return RaisedButton(
          onPressed: !snapshot.hasData
              ? null
              : () {
                  return showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('ยืนยัน'),
                        content: Text(
                            'ยืนยันความถูกต้องของข้อมูล และบันทึกข้อมูลในระบบ.'),
                        contentPadding: EdgeInsets.all(10.0),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('ปิด'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          RaisedButton(
                            child: Text('ยืนยัน'),
                            onPressed: () async {
                              Navigator.of(context).pop();
                              bool res = await _attachBloc.attachAwb();
                              if (res == true) {
                                success();
                              }
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.save),
              SizedBox(width: 10),
              Text('บันทึกข้อมูล'),
            ],
          ),
        );
      },
    );
  }

  success() {
    Navigator.of(context).pushNamedAndRemoveUntil(
      RouteData.RouteRoot,
      ModalRoute.withName('/'),
    );

    Navigator.of(context).pushNamed(
      RouteData.RouteAttachSuccess,
    );
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      appBar: AppBar(
        title: Text(
          'บันทึกชำระเงิน',
        ),
      ),
      body: Column(
        children: <Widget>[
          Divider(),
          attachCard(),
          Divider(),
          attachPhoto(),
          // Divider(),
          // attachOther(),
          Divider(),
          listAwb(),
        ],
      ),
      bottomNavigationBar: Container(child: buttonSave()),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _attachBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
