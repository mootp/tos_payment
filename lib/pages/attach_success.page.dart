import 'package:flutter/material.dart';
import 'package:tos_payment/blocs/attach.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/utils/routes.util.dart';

class AttachSuccessPage extends StatefulWidget {
  AttachSuccessPage({Key key, this.arguments}) : super(key: key);
  final AttachSuccessArguments arguments;
  @override
  _AttachSuccessPageState createState() => _AttachSuccessPageState();
}

class _AttachSuccessPageState extends State<AttachSuccessPage> {
  AttachBloc _attachBloc;
  @override
  void initState() {
    _attachBloc = AttachBloc();
    super.initState();
  }

  @override
  void dispose() {
    _attachBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      appBar: AppBar(
        title: Text(
          'บันทึกสำเร็จ',
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text('บันทึกข้อมูลในระบบเรียบร้อย'),
              SizedBox(
                height: 10.0,
              ),
              OutlineButton(
                padding: const EdgeInsets.all(20.0),
                child: Text('กลับหน้าหลัก'),
                onPressed: () {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                    RouteData.RouteRoot,
                    ModalRoute.withName('/'),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _attachBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
