import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:tos_payment/blocs/login.bloc.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.arguments}) : super(key: key);
  final LoginArguments arguments;
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final userController = TextEditingController();
  final passController = TextEditingController();

  bool loading = false;
  LoginBloc _loginBloc;
  void showInSnackBar(String value) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(value),
        backgroundColor: Theme.of(context).accentColor,
      ),
    );
  }

  @override
  void dispose() {
    _loginBloc?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _loginBloc = LoginBloc();

    super.initState();
  }

  DateTime currentBackPressTime;
  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: 'Please click BACK again to exit.');
      return Future.value(false);
    }
    return Future.value(true);
  }

  usernameField() {
    return TextField(
      controller: userController,
      obscureText: false,
      // style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        hintText: "Username",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );
  }

  passwordField() {
    return TextField(
      controller: passController,
      obscureText: true,
      // style: TextStyle(fontSize: 20.0),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        hintText: "Password",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(32.0),
        ),
      ),
    );
  }

  loginButon() {
    return SizedBox(
      width: double.infinity,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(32.0),
          side: BorderSide(color: Colors.red),
        ),
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          if (userController.text != null && userController.text != '') {
            if (passController.text != null && passController.text != '') {
              bool status = await _loginBloc.login(
                  userController.text, passController.text);
              if (status) {
                Navigator.pushNamedAndRemoveUntil(
                    context, RouteData.RouteSplash, (route) => false);
              }
            }else{
              Fluttertoast.showToast(msg: 'Please fill password.');
            }
          }else{
             Fluttertoast.showToast(msg: 'Please fill username.');
          }
        },
        child: Text(
          "Login",
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // final Size screenSize = MediaQuery.of(context).size;
    var form = WillPopScope(
      onWillPop: onWillPop,
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_sf.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.transparent,
          body: Center(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Card(
                  margin: EdgeInsets.all(30.0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Center(
                            child: Image(
                              image: AssetImage('assets/images/logo.png'),
                              height: 100,
                            ),
                          ),
                        ),
                        Text(
                          'TOS Payment',
                          style: TextStyle(
                            // fontFamily: 'christmas-cookies',
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            // color: Colors.redAccent,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Container(
                          margin: EdgeInsets.fromLTRB(30.0, 2.0, 30.0, 2.0),
                          child: usernameField(),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(30.0, 2.0, 30.0, 2.0),
                          child: passwordField(),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(30.0, 2.0, 30.0, 2.0),
                          child: loginButon(),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _loginBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
