import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:tos_payment/blocs/payment.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/utils/routes.util.dart';

import 'package:tos_payment/models/enum.model.dart';

class PaymentSummaryPage extends StatefulWidget {
  PaymentSummaryPage({Key key, this.arguments}) : super(key: key);
  final PaymentSummaryArguments arguments;
  @override
  _PaymentSummaryPageState createState() => _PaymentSummaryPageState();
}

class _PaymentSummaryPageState extends State<PaymentSummaryPage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');
  PaymentBloc _paymentBloc;
  @override
  void initState() {
    _paymentBloc = PaymentBloc();

    _paymentBloc.paymentCodeEvent.add(widget.arguments.paymentCode);
    _paymentBloc.receiptCodeEvent.add(widget.arguments.receiptCode);

    _paymentBloc.nameEvent.add(widget.arguments.name);
    _paymentBloc.taxidEvent.add(widget.arguments.taxId);
    _paymentBloc.branchEvent.add(widget.arguments.branch);
    _paymentBloc.addressEvent.add(widget.arguments.address);
    _paymentBloc.stationEvent.add(widget.arguments.station);
    _paymentBloc.listawbEvent.add(widget.arguments.listAwb);
    super.initState();
  }

  @override
  void dispose() {
    _paymentBloc?.dispose();
    super.dispose();
  }

  Widget viewdata(String title, String data, int line) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              data,
              overflow: TextOverflow.ellipsis,
              maxLines: line,
            ),
          ),
        ],
      ),
    );
  }

  Widget viewDetail() {
    return ListTile(
      title: Padding(
        padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
        child: Column(
          children: <Widget>[
            StreamBuilder(
              stream: _paymentBloc.taxidStream,
              builder: (context, snapshot) {
                return viewdata('Tax ID : ', snapshot.data ?? '', 1);
              },
            ),
            Divider(),
            StreamBuilder(
              stream: _paymentBloc.branchStream,
              builder: (context, snapshot) {
                return viewdata('Branch : ', snapshot.data ?? '', 1);
              },
            ),
            Divider(),
            StreamBuilder(
              stream: _paymentBloc.nameStream,
              builder: (context, snapshot) {
                return viewdata('Name : ', snapshot.data ?? '', 1);
              },
            ),
            Divider(),
            StreamBuilder(
              stream: _paymentBloc.addressStream,
              builder: (context, snapshot) {
                return viewdata('Address : ', snapshot.data ?? '', 3);
              },
            ),
            Divider(),
            StreamBuilder(
              stream: _paymentBloc.stationStream,
              builder: (context, snapshot) {
                return viewdata('Station : ', snapshot.data ?? '', 1);
              },
            ),
            Divider(),
          ],
        ),
      ),
    );
  }

  listPrice(
    String txtTitle,
    String subTitle,
    String awb,
    double price,
    PriceType type,
  ) {
    return ListTile(
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            txtTitle,
          ),
          Text(
            subTitle,
            style: TextStyle(
                fontSize: Theme.of(context).textTheme.subtitle1.fontSize * 0.9),
          ),
        ],
      ),
      trailing: Padding(
        padding: const EdgeInsets.only(top: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(numberFormat.format(price).toString()),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  listAwb() {
    return StreamBuilder(
      stream: _paymentBloc.listawbStream,
      builder: (context, snapshot) {
        List<Widget> list = [];

        if (snapshot.hasData) {
          List<AwbData> listAwb = snapshot.data;

          double total = 0.0;
          listAwb.forEach((item) {
            double totalRow = item.itemAdvance +
                item.itemFreight +
                item.itemService +
                item.itemServiceVat;
            total += totalRow;
          });

          list.add(
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  CircleAvatar(
                    child: Text(listAwb.length.toString()),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        'List AWB',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Text(
                    'ราคา',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 35,
                  ),
                ],
              ),
            ),
          );

          list.add(
            Divider(),
          );

          list.add(
            Flexible(
              child: ListView.separated(
                itemCount: listAwb.length,
                separatorBuilder: (context, index) => Divider(),
                itemBuilder: (context, index) {
                  String hawbNo = listAwb[index].hawbNo;
                  double itemAdvance = listAwb[index].itemAdvance;
                  double itemFreight = listAwb[index].itemFreight;
                  double itemService = listAwb[index].itemService;
                  double itemServiceVat = listAwb[index].itemServiceVat;
                  // double total =
                  //     itemAdvance + itemFreight + itemService + itemServiceVat;
                  return ListTile(
                    leading: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text((index + 1).toString()),
                        VerticalDivider(),
                      ],
                    ),
                    title: Text(
                      hawbNo,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    subtitle: Column(
                      children: <Widget>[
                        itemFreight > 0
                            ? listPrice(
                                'FREIGHT CHARGE',
                          '',
                                hawbNo,
                                itemFreight,
                                PriceType.freight,
                              )
                            : Container(),
                        itemAdvance > 0
                            ? listPrice(
                                'ADVANCE PAYMENT',
                                '(CUSTOM TAX / FUEL / OTHER)',
                                hawbNo,
                                itemAdvance,
                                PriceType.advance,
                              )
                            : Container(),
                        itemService > 0
                            ? listPrice(
                                'SERVICES',
                                '(EDI / STORAGE / OTHER)',
                                hawbNo,
                                itemService,
                                PriceType.service,
                              )
                            : Container(),
                        itemServiceVat > 0
                            ? listPrice(
                                'SERVICES',
                                '(VAT 7%)',
                                hawbNo,
                                itemServiceVat,
                                PriceType.vat,
                              )
                            : Container(),
                      ],
                    ),
                  );
                },
              ),
            ),
          );

          list.add(
            Divider(),
          );

          list.add(
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              child: Row(
                // mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    numberFormat.format(total).toString(),
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 10.0),
                  Text(
                    'THB',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          );

          list.add(
            Divider(),
          );
        } else {
          list.add(
            Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        return Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Column(
              children: list,
            ),
          ),
        );
      },
    );
  }

  paymnetSuccess(payment) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(payment.paymentCode),
          content: Text('บันทึกข้อมูลเรียบร้อย '),
          contentPadding: EdgeInsets.all(10.0),
          actions: <Widget>[
            RaisedButton(
              child: Text('ไปที่หน้าชำระเงิน'),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                  RouteData.RouteRoot,
                  ModalRoute.withName('/'),
                );

                Navigator.of(context).pushNamed(RouteData.RoutePaymentList);
                Navigator.of(context).pushNamed(
                  RouteData.RouteChargeDetail,
                  arguments: ChargeDetailArguments(
                    paymentCode: payment.paymentCode,
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }

  checkpaymnetSuccess(payment) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(payment.paymentCode),
          // content: Text('ตรวจสอบข้อมูลในระบบเรียบร้อย '),
          contentPadding: EdgeInsets.all(10.0),
          actions: <Widget>[
            RaisedButton(
              child: Text('ไปที่หน้าชำระเงิน'),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                  RouteData.RouteRoot,
                  ModalRoute.withName('/'),
                );

                Navigator.of(context).pushNamed(RouteData.RoutePaymentList);
                Navigator.of(context).pushNamed(
                  RouteData.RouteChargeDetail,
                  arguments: ChargeDetailArguments(
                    paymentCode: payment.paymentCode,
                  ),
                );
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'ตรวจสอบข้อมูล',
        ),
      ),
      body: Column(
        children: <Widget>[
          viewDetail(),
          listAwb(),
        ],
      ),
      bottomNavigationBar: RaisedButton(
        // color: Theme.of(context).primaryColor,
        onPressed: () async {
          if (widget.arguments.paymentCode != null &&
              widget.arguments.paymentCode != '') {
            PaymentUrl payment = await _paymentBloc.createPayment(
                context, widget.arguments.process);
            if (payment != null) {
              checkpaymnetSuccess(payment);
            }
          } else {
            return showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text('ยืนยันบันทึกข้อมูล'),
                  content: Text(
                      'ตรวจสอบความถูกต้องของข้อมูล และยืนยันบันทึกข้อมูลลงระบบ.'),
                  contentPadding: EdgeInsets.all(10.0),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('ยกเลิก'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    RaisedButton(
                      child: Text('บันทึก'),
                      onPressed: () async {
                        Navigator.pop(context);
                        PaymentUrl payment = await _paymentBloc.createPayment(
                            context, widget.arguments.process);
                        if (payment != null) {
                          paymnetSuccess(payment);
                        }
                      },
                    ),
                  ],
                );
              },
            );
          }
        },
        child: (widget.arguments.paymentCode != null &&
                widget.arguments.paymentCode != '')
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.check_box),
                  SizedBox(
                    width: 20.0,
                  ),
                  Text('ยืนยันข้อมูล'),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.save),
                  SizedBox(
                    width: 20.0,
                  ),
                  Text('บันทึกลงระบบ'),
                ],
              ),
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _paymentBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
