import 'package:flutter/material.dart';
import 'package:tos_payment/blocs/account.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/utils/routes.util.dart';

class AccountPage extends StatefulWidget {
  AccountPage({Key key, this.arguments}) : super(key: key);
  final AccountArguments arguments;
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  AccountBloc _accountBloc;
  @override
  void dispose() {
    _accountBloc?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _accountBloc = AccountBloc();

    super.initState();
  }

  logoutButton(Icon icon, Text text) {
    return Container(
      margin: EdgeInsets.all(15),
      child: RaisedButton(
        padding: EdgeInsets.all(15),
        onPressed: () async {
          return showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Logout'),
                content: Text('Are you sure you want to logout ?'),
                contentPadding: EdgeInsets.all(10.0),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Cancel'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  RaisedButton(
                    child: Text('Logout'),
                    onPressed: () async {
                      bool status = await _accountBloc.logout();
                      if (status) {
                        Navigator.pushNamedAndRemoveUntil(
                            context, RouteData.RouteSplash, (route) => false);
                      }
                      // Navigator.pop(context);
                    },
                  ),
                ],
              );
            },
          );
        },
        child: Column(
          children: <Widget>[
            icon,
            text,
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('Build account page');
    var form = Scaffold(
      appBar: AppBar(
        title: Text('Account'),
      ),
      body: ListView(
        children: <Widget>[
          logoutButton(
            Icon(Icons.lock),
            Text('Logout'),
          ),
        ],
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _accountBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
