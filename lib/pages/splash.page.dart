import 'package:flutter/material.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/blocs/splash.bloc.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key, this.arguments}) : super(key: key);
  final SplashArguments arguments;
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  SplashBloc _splashBloc;

  // DateTime currentStart;

  @override
  void initState() {
    _splashBloc = new SplashBloc();
    // currentStart = DateTime.now();
    // loadApp();
    _splashBloc.checkUser();
    super.initState();
    // Future.delayed(Duration(seconds: 2)).then((_) {

    // });
  }

  // loadApp() async {
  //   var duration = const Duration(seconds: 3);
  //   return new Timer(duration, () {
  //     _splashBloc.checkUser();
  //   });
  // }

  // delayPage(RouteData page) {
  //   DateTime now = DateTime.now();
  //   if (currentStart == null ||
  //       now.difference(currentStart) > Duration(seconds: 3)) {
  //     return Future.value(false);
  //   }
  //   return Future.value(true);
  // }

  void myCallback(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  buildsplash() {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/bg_r8.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              StreamBuilder(
                  stream: _splashBloc.pageStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data != '') {
                      myCallback(() {
                        // Navigator.of(context).pushNamedAndRemoveUntil(
                        //   snapshot.data,
                        //   ModalRoute.withName('/'),
                        // );

                        Navigator.of(context)
                            .pushReplacementNamed(snapshot.data);
                      });
                    }
                    return Text(
                      'Loading...',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildsplash();
  }
}
