import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:tos_payment/blocs/root.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';

class RootPage extends StatefulWidget {
  RootPage({Key key, this.arguments}) : super(key: key);
  final RootArguments arguments;
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  DateTime currentBackPressTime;
  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: 'Please click BACK again to exit.');
      return Future.value(false);
    }
    return Future.value(true);
  }

  RootBloc _rootBloc;
  @override
  void initState() {
    _rootBloc = RootBloc();

    super.initState();
  }

  @override
  void dispose() {
    _rootBloc?.dispose();
    super.dispose();
  }

  _buildPage(List<Widget> listPage, int pageindex) {
    return Scaffold(
      body: listPage[pageindex],
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: (index) {
            _rootBloc.pageindexEvent.add(index);
          },
          currentIndex: pageindex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text(
                'Home',
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              title: Text(
                'Account',
              ),
            ),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('Build root page');
    return WillPopScope(
      onWillPop: onWillPop,
      child: MultiProvider(
        providers: [
          StreamProvider<int>.value(value: _rootBloc.pageindexStream),
          StreamProvider<List<Widget>>.value(value: _rootBloc.listPageStream),
        ],
        child: Builder(
          builder: (context) {
            int pageindex = Provider.of<int>(context);
            List<Widget> listPage = Provider.of<List<Widget>>(context);
            if (pageindex != null && listPage != null) {
              return _buildPage(listPage, pageindex);
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}
