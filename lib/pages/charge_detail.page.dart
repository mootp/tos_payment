import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:intl/intl.dart';
import 'package:tos_payment/blocs/charge.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/payment.model.dart';

import 'package:tos_payment/models/enum.model.dart';
import 'package:tos_payment/utils/routes.util.dart';

class ChargeDetailPage extends StatefulWidget {
  ChargeDetailPage({Key key, this.arguments}) : super(key: key);
  final ChargeDetailArguments arguments;
  @override
  _ChargeDetailPageState createState() => _ChargeDetailPageState();
}

class _ChargeDetailPageState extends State<ChargeDetailPage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');

  ChargeBloc _chargeBloc;
  @override
  void initState() {
    _chargeBloc = ChargeBloc();
    _chargeBloc.paymentCodeEvent.add(widget.arguments.paymentCode);
    _chargeBloc.loadPaymentDetail();
    super.initState();
  }

  @override
  void dispose() {
    _chargeBloc?.dispose();
    super.dispose();
  }

  Widget viewdata(String title, String data, int line) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              data,
              overflow: TextOverflow.ellipsis,
              maxLines: line,
            ),
          ),
        ],
      ),
    );
  }

  Widget viewDetail() {
    return StreamBuilder(
        stream: _chargeBloc.paymentStream,
        builder: (context, snapshot) {
          String taxid = '';
          String branch = '';
          String name = '';
          String address = '';
          String station = '';
          String process = '';
          if (snapshot.hasData) {
            PaymentData data = snapshot.data;
            taxid = data.paymentH.paymentTaxid;
            branch = data.paymentH.paymentBranch;
            name = data.paymentH.paymentName;
            address = data.paymentH.paymentAddress;
            station = data.paymentH.paymentStation;
            process = data.paymentH.paymentProcess ?? '';
          }

          return Column(
            children: <Widget>[
              ListTile(
                title: Padding(
                  padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                  child: Column(
                    children: <Widget>[
                      viewdata('Tax ID : ', taxid, 1),
                      Divider(),
                      viewdata('Branch : ', branch, 1),
                      Divider(),
                      viewdata('Name : ', name, 1),
                      Divider(),
                      viewdata('Address : ', address, 2),
                      Divider(),
                      viewdata('Station : ', station, 1),
                      Divider(),
                    ],
                  ),
                ),
              ),
              listAwb(process),
            ],
          );
        });
  }

  Widget buttonPay() {
    return StreamBuilder(
      stream: _chargeBloc.paymentStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          PaymentData data = snapshot.data;

          String paymentCode = data.paymentH.paymentCode;
          String receiptCode = data.paymentH.receiptCode;
          String taxid = data.paymentH.paymentTaxid;
          String branch = data.paymentH.paymentBranch;
          String name = data.paymentH.paymentName;
          String address = data.paymentH.paymentAddress;
          String station = data.paymentH.paymentStation;

          List<PaymentD> listA = data.paymentA;
          List<PaymentP> listP = data.paymentP;

          return StreamBuilder(
            stream: _chargeBloc.listawbStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<AwbData> listAwb = snapshot.data;

                double totalAmount = 0.0;
                double totalPay = 0.0;

                listA.forEach((x) => totalAmount += (x.itemAdvance ?? 0) +
                    (x.itemFreight ?? 0) +
                    (x.itemService ?? 0) +
                    (x.itemServiceVat ?? 0));
                listP.forEach((x) {
                  x.paymentPd.forEach((p) => totalPay += (p.itemAdvance ?? 0) +
                      (p.itemFreight ?? 0) +
                      (p.itemService ?? 0) +
                      (p.itemServiceVat ?? 0));
                });
                listAwb.forEach((x) => totalPay += (x.itemAdvance ?? 0) +
                    (x.itemFreight ?? 0) +
                    (x.itemService ?? 0) +
                    (x.itemServiceVat ?? 0));

                return Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          padding: const EdgeInsets.all(8.0),
                          onPressed: totalPay > totalAmount
                              ? null
                              : () async {
                                  Navigator.of(context).pushNamed(
                                    RouteData.RouteCharge,
                                    arguments: ChargeArguments(
                                      paymentCode: paymentCode,
                                      receiptCode: receiptCode,
                                      taxid: taxid,
                                      branch: branch,
                                      name: name,
                                      address: address,
                                      station: station,
                                      method: "CASH",
                                      listAwb: listAwb,
                                    ),
                                  );
                                },
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text('รับชำระด้วย'),
                              Icon(MaterialCommunityIcons.cash),
                              Text('เงินสด'),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          padding: const EdgeInsets.all(8.0),
                          onPressed: totalPay > totalAmount
                              ? null
                              : () async {
                                  Navigator.of(context).pushNamed(
                                    RouteData.RouteCharge,
                                    arguments: ChargeArguments(
                                      paymentCode: paymentCode,
                                      receiptCode: receiptCode,
                                      taxid: taxid,
                                      branch: branch,
                                      name: name,
                                      address: address,
                                      station: station,
                                      method: "TRANSFER",
                                      listAwb: listAwb,
                                    ),
                                  );
                                },
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text('รับชำระด้วย'),
                              Icon(MaterialCommunityIcons.bank_transfer),
                              Text('โอนเงิน'),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          padding: const EdgeInsets.all(8.0),
                          onPressed: totalPay > totalAmount
                              ? null
                              : () async {
                                  Navigator.of(context).pushNamed(
                                    RouteData.RouteCharge,
                                    arguments: ChargeArguments(
                                      paymentCode: paymentCode,
                                      receiptCode: receiptCode,
                                      taxid: taxid,
                                      branch: branch,
                                      name: name,
                                      address: address,
                                      station: station,
                                      method: "QR",
                                      listAwb: listAwb,
                                    ),
                                  );
                                },
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text('รับชำระด้วย'),
                              Icon(MaterialCommunityIcons.qrcode),
                              Text('QR Code'),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  listPrice(
    String txtTitle,
    String subTitle,
    String awb,
    double price,
    PriceType type,
  ) {
    return ListTile(
      // contentPadding: EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            txtTitle,
          ),
          Text(
            subTitle,
            style: TextStyle(
                fontSize: Theme.of(context).textTheme.subtitle1.fontSize * 0.9),
          ),
        ],
      ),
      trailing: Padding(
        padding: const EdgeInsets.only(top: 6.5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(numberFormat.format(price).toString()),
                InkWell(
                  child: Icon(Icons.edit),
                  onTap: () {
                    _chargeBloc.priceEvent.add(price);
                    TextEditingController _controller =
                        new TextEditingController(
                      text: price.toString(),
                    );
                    _controller.selection = TextSelection(
                      baseOffset: 0,
                      extentOffset: price.toString().length,
                    );
                    return showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('AWB : $awb'),
                              Text(txtTitle),
                              TextField(
                                controller: _controller,
                                autofocus: true,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  hintText: 'Price',
                                ),
                                onChanged: (txt) {
                                  _chargeBloc.priceEvent.add(
                                    double.parse(txt),
                                  );
                                },
                              ),
                            ],
                          ),
                          contentPadding: EdgeInsets.all(10.0),
                          actions: <Widget>[
                            FlatButton(
                              child: Text('ยกเลิก'),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            RaisedButton(
                              child: Text('บันทึก'),
                              onPressed: () async {
                                await _chargeBloc.editawbprice(awb, type);
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  listAwb(String process) {
    return StreamBuilder(
      stream: _chargeBloc.listawbStream,
      builder: (context, snapshot) {
        List<Widget> list = [];

        if (snapshot.hasData) {
          List<AwbData> listAwb = snapshot.data;

          double total = 0.0;
          listAwb.forEach((item) {
            double totalRow = item.itemAdvance +
                item.itemFreight +
                item.itemService +
                item.itemServiceVat;
            total += totalRow;
          });

          list.add(
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  CircleAvatar(
                    child: Text(listAwb.length.toString()),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        'List AWB',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Text(
                    'ราคา',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    width: 35,
                  ),
                ],
              ),
            ),
          );

          list.add(
            Divider(),
          );

          list.add(
            Flexible(
              child: ListView.separated(
                itemCount: listAwb.length,
                separatorBuilder: (context, index) => Divider(),
                itemBuilder: (context, index) {
                  String hawbNo = listAwb[index].hawbNo;
                  double itemAdvance = listAwb[index].itemAdvance;
                  double itemFreight = listAwb[index].itemFreight;
                  double itemService = listAwb[index].itemService;
                  double itemServiceVat = listAwb[index].itemServiceVat;
                  // double total =
                  //     itemAdvance + itemFreight + itemService + itemServiceVat;

                  return ListTile(
                    leading: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text((index + 1).toString()),
                        VerticalDivider(),
                      ],
                    ),
                    title: Text(
                      hawbNo,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        listPrice(
                          'FREIGHT CHARGE',
                          '',
                          hawbNo,
                          itemFreight,
                          PriceType.freight,
                        ),
                        process == 'PICKUP'
                            ? Container()
                            : listPrice(
                                'ADVANCE PAYMENT',
                                '(CUSTOM TAX / FUEL / OTHER)',
                                hawbNo,
                                itemAdvance,
                                PriceType.advance,
                              ),
                        process == 'PICKUP'
                            ? Container()
                            : listPrice(
                                'SERVICES',
                                '(EDI / STORAGE / OTHER)',
                                hawbNo,
                                itemService,
                                PriceType.service,
                              ),
                        process == 'PICKUP'
                            ? Container()
                            : listPrice(
                                'SERVICES',
                                '(VAT 7%)',
                                hawbNo,
                                itemServiceVat,
                                PriceType.vat,
                              ),
                      ],
                    ),
                  );
                },
              ),
            ),
          );

          list.add(
            Divider(),
          );

          list.add(
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              child: Row(
                // mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    numberFormat.format(total).toString(),
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 10.0),
                  Text(
                    'THB',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          );

          list.add(
            Divider(),
          );
        } else {
          list.add(
            Center(
              child: CircularProgressIndicator(),
            ),
          );
        }

        return Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Column(
              children: list,
            ),
          ),
        );
      },
    );
  }

  showConfirm(Widget title, List<Widget> action) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: title,
          contentPadding: EdgeInsets.all(10.0),
          actions: action,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'ข้อมูลการชำระเงิน',
        ),
      ),
      body: viewDetail(),
      bottomNavigationBar: buttonPay(),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _chargeBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
