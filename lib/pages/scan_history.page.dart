import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tos_payment/blocs/attach.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/history.model.dart';
import 'package:tos_payment/pages/photoView.page.dart';
import 'package:tos_payment/utils/config.util.dart';

class ScanHistoryPage extends StatefulWidget {
  ScanHistoryPage({Key key, this.arguments}) : super(key: key);
  final ScanHistoryArguments arguments;
  @override
  _ScanHistoryPageState createState() => _ScanHistoryPageState();
}

class _ScanHistoryPageState extends State<ScanHistoryPage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');
  AttachBloc _attachBloc;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    _attachBloc = AttachBloc();
    String apptime = dateFormat.format(DateTime.now());
    _attachBloc.fdateEvent.add(apptime);
    _attachBloc.tdateEvent.add(apptime);
    _attachBloc.loadScanHistoryList();
    super.initState();
  }

  @override
  void dispose() {
    _attachBloc?.dispose();
    super.dispose();
  }

  Widget fdate() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('From Date'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _attachBloc.fdateStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  DateTime tmpDate;
                  tmpDate = dateFormat.parse(snapshot.data);

                  return DateTimeField(
                    format: dateFormat,
                    initialValue: tmpDate,
                    resetIcon: null,
                    onChanged: (date) {
                      String strDate = dateFormat.format(date);
                      _attachBloc.fdateEvent.add(strDate);
                      _attachBloc.loadScanHistoryList();
                    },
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100),
                      );

                      if (date != null) {
                        return date;
                      } else {
                        return currentValue;
                      }
                    },
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget tdate() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('To Date'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _attachBloc.fdateStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  DateTime tmpDate;
                  tmpDate = dateFormat.parse(snapshot.data);

                  return DateTimeField(
                    format: dateFormat,
                    initialValue: tmpDate,
                    resetIcon: null,
                    onChanged: (date) {
                      String strDate = dateFormat.format(date);
                      _attachBloc.fdateEvent.add(strDate);
                      _attachBloc.loadScanHistoryList();
                    },
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100),
                      );

                      if (date != null) {
                        return date;
                      } else {
                        return currentValue;
                      }
                    },
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  headlist() {
    return StreamBuilder(
      stream: _attachBloc.listhistoryStream,
      builder: (context, snapshot) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              CircleAvatar(
                child: Text(
                    snapshot.hasData ? snapshot.data.length.toString() : '0'),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    'List',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  imagesViewAwb(List<String> images) {
    double size = 50;
    List<Widget> list = [];

    List<ImageProvider> listp = [];
    int i = 0;

    images.forEach(
      (img) {
        String pathimg = '${Config.apiUrl}/AwbImage/' + img;
        list.add(
          Card(
            clipBehavior: Clip.antiAlias,
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PhotoViewWrapper(
                      galleryItems: listp,
                      backgroundDecoration: const BoxDecoration(
                        color: Colors.black,
                      ),
                      initialIndex: i,
                      // scrollDirection: verticalGallery ? Axis.vertical : Axis.horizontal,
                    ),
                  ),
                );
              },
              child: Image.network(
                pathimg,
                height: size,
                width: size,
                fit: BoxFit.cover,
              ),
            ),
          ),
        );

        listp.add(NetworkImage(pathimg));
        i++;
      },
    );

    return list;
  }

  editAwb(int verifyId, String verifyCode, int awbId, String hawbNo) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('แก้ไขเลขที่ AWB \n' + hawbNo),
            content: StreamBuilder(
              stream: _attachBloc.awbTextEditingControllerStream,
              builder: (context, textEditingController) {
                return StreamBuilder(
                  stream: _attachBloc.awbStream,
                  builder: (context, snapshot) {
                    return TextField(
                      controller: textEditingController.data,
                      decoration: InputDecoration(
                        hintText: 'ระบุเลขที่ AWB ใหม่',
                        errorText: snapshot.error,
                      ),
                      onChanged: (value) {
                        _attachBloc.awbEvent.add(value);
                      },
                    );
                  },
                );
              },
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              FlatButton(
                child: Text('ยกเลิก'),
                onPressed: () {
                  _attachBloc.cancelEdit();
                  Navigator.pop(context);
                },
              ),
              StreamBuilder(
                  stream: _attachBloc.awbValid,
                  builder: (context, snapshot) {
                    return RaisedButton(
                      child: Text('ยืนยันการแก้ไข'),
                      onPressed: !snapshot.hasData
                          ? null
                          : () async {
                              bool res = await _attachBloc.editAwb(
                                verifyId,
                                verifyCode,
                                awbId,
                                hawbNo,
                              );
                              if (res) {
                                _attachBloc.cancelEdit();
                                Navigator.pop(context);

                                _attachBloc.loadScanHistoryList();
                              }
                            },
                    );
                  }),
            ],
          );
        });
  }

  listHistory() {
    return StreamBuilder(
      stream: _attachBloc.listhistoryStream,
      builder: (context, snapshot) {
        List<Widget> list = [];

        if (snapshot.hasData) {
          List<HistoryListData> listHistory = snapshot.data;

          list.add(
            Divider(),
          );

          list.add(
            Flexible(
              child: SmartRefresher(
                enablePullDown: true,
                enablePullUp: false,
                controller: _refreshController,
                onRefresh: () {
                  _attachBloc.loadScanHistoryList();
                  _refreshController.refreshCompleted();
                },
                child: ListView.separated(
                  itemCount: listHistory.length,
                  separatorBuilder: (context, index) => Divider(),
                  itemBuilder: (context, index) {
                    int verifyId = listHistory[index].verifyId;
                    String verifyCode = listHistory[index].verifyCode;
                    String createDate =
                        datetimeFormat.format(listHistory[index].createDate);

                    String nowDate = dateFormat.format(DateTime.now());
                    String checkDate =
                        dateFormat.format(listHistory[index].createDate);

                    List<Widget> lp = [];

                    listHistory[index].hawbNos.forEach(
                      (item) {
                        int awbId = item.awbId;
                        String hawbNo = item.hawbNo;
                        List<String> listimg = item.awbAttachs;
                        lp.add(ListTile(
                          leading: Wrap(
                            children: imagesViewAwb(listimg),
                          ),
                          title: Text(hawbNo),
                          trailing: Container(
                            width: 30,
                            child: nowDate == checkDate
                                ? RaisedButton(
                                    // color: Colors.blue,
                                    padding: EdgeInsets.all(0.0),
                                    child: Icon(Icons.edit),
                                    onPressed: () {
                                      editAwb(
                                          verifyId, verifyCode, awbId, hawbNo);
                                    },
                                  )
                                : Container(),
                          ),
                        ));
                      },
                    );
                    return Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  10.0, 0.0, 10.0, 0.0),
                              child: Text((index + 1).toString() + ')'),
                            ),
                            Divider(),
                            Expanded(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    child: InkWell(
                                      onTap: () {},
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Date : ' + createDate,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                          child: Column(
                            children: lp,
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
            ),
          );

          list.add(
            Divider(),
          );
        } else {
          list.add(
            Flexible(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }

        return Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Column(children: list),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'รายการเอกสารผู้ส่งสินค้า',
        ),
      ),
      body: Column(
        children: <Widget>[
          fdate(),
          tdate(),
          headlist(),
          listHistory(),
        ],
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _attachBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
