import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:tos_payment/blocs/home.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.arguments}) : super(key: key);
  final HomeArguments arguments;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  menuButton(String texttop, IconData icon, String text, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 5,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                texttop,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Icon(
                icon,
                size: 70.0,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                text,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
    // return Container(
    //   margin: EdgeInsets.all(15),
    //   child: RaisedButton(
    //     padding: EdgeInsets.all(15),
    //     onPressed: () {
    //       Navigator.of(context).pushNamed(RouteData.RouteScan);
    //     },
    //     child: Column(
    //       children: <Widget>[
    //         icon,
    //         text,
    //       ],
    //     ),
    //   ),
    // );
  }

  openPdf(String url) async {
    if (await canLaunch(url)) {
      // Map<String, String> headers = new HashMap();
      // headers['Content-type'] = 'application/pdf';
      // headers['Accept-Ranges'] = 'bytes';
      // headers['Access-Control-Allow-Origin'] = "*";
      // headers['Content-Disposition'] = 'attachment';
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  HomeBloc _homeBloc;
  @override
  void initState() {
    _homeBloc = HomeBloc();

    super.initState();
  }

  @override
  void dispose() {
    _homeBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('Build home page');
    var form = Scaffold(
      appBar: AppBar(
        title: Text('หน้าหลัก'),
      ),
      // backgroundColor: Colors.grey[300],
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            menuButton(
              '',
              MaterialCommunityIcons.cube_scan,
              'รับสินค้า',
              () {
                Navigator.of(context).pushNamed(RouteData.RouteScanPickup);
              },
            ),
            menuButton(
              '',
              MaterialCommunityIcons.cube_send,
              'ส่งสินค้า',
              () {
                Navigator.of(context).pushNamed(RouteData.RouteScanDelivery);
              },
            ),
            menuButton(
              'รายการ',
              MaterialCommunityIcons.format_list_bulleted_square,
              'รับชำระเงิน',
              () {
                Navigator.of(context).pushNamed(RouteData.RoutePaymentList);
              },
            ),
            menuButton(
              'เอกสาร',
              MaterialCommunityIcons.credit_card_scan,
              'ผู้ส่งสินค้า',
              () {
                Navigator.of(context).pushNamed(RouteData.RouteScanVerify);
              },
            ),
            menuButton(
              'รายการ',
              MaterialCommunityIcons.history,
              'ผู้ส่งสินค้า',
              () {
                Navigator.of(context).pushNamed(RouteData.RouteScanHistory);
              },
            ),
          ],
        ),
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _homeBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
