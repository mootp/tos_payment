import 'dart:io';

import 'package:camera_camera/camera_camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:intl/intl.dart';
import 'package:tos_payment/blocs/charge.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/enum.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/pages/charge_qr.page.dart';
import 'package:tos_payment/pages/photoView.page.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:url_launcher/url_launcher.dart';

class ChargePage extends StatefulWidget {
  ChargePage({Key key, this.arguments}) : super(key: key);
  final ChargeArguments arguments;
  @override
  _ChargePageState createState() => _ChargePageState();
}

class _ChargePageState extends State<ChargePage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');

  ChargeBloc _chargeBloc;

  @override
  void initState() {
    _chargeBloc = ChargeBloc();
    super.initState();
  }

  @override
  void dispose() {
    _chargeBloc?.dispose();
    super.dispose();
  }

  imagesView(List<File> images) {
    double size = 80;
    List<Widget> list = [];

    List<ImageProvider> listp = [];
    int i = 0;

    images.forEach(
      (img) {
        list.add(
          Card(
            clipBehavior: Clip.antiAlias,
            child: Stack(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PhotoViewWrapper(
                          galleryItems: listp,
                          backgroundDecoration: const BoxDecoration(
                            color: Colors.black,
                          ),
                          initialIndex: i,
                          // scrollDirection: verticalGallery ? Axis.vertical : Axis.horizontal,
                        ),
                      ),
                    );
                  },
                  child: Image.file(
                    img,
                    height: size,
                    width: size,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  right: 5,
                  top: 5,
                  child: InkWell(
                    child: Icon(
                      Icons.remove_circle,
                      size: 20,
                      color: Colors.red,
                    ),
                    onTap: () {
                      _chargeBloc.removeImage(img.path);
                    },
                  ),
                ),
              ],
            ),
          ),
        );
        listp.add(FileImage(img));
        i++;
      },
    );

    if (images.length < 2) {
      list.add(
        Card(
          clipBehavior: Clip.antiAlias,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: () async {
                  File pic = await showDialog(
                    context: context,
                    builder: (context) => Camera(
                      mode: CameraMode.normal,
                      initialCamera: CameraSide.back,
                    ),
                  );
                  if (pic != null) {
                    _chargeBloc.addImage(pic);
                  }
                },
                child: Container(
                  color: Colors.grey[300],
                  width: double.parse(size.toString()),
                  height: double.parse(size.toString()),
                  child: Icon(
                    Icons.add_a_photo,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
    return list;
  }

  Widget transferDate() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text("Transfer Date"),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _chargeBloc.transferDateTextEditingStream,
              builder: (context, textEditingController) {
                return StreamBuilder(
                  stream: _chargeBloc.transferDateStream,
                  builder: (context, snapshot) {
                    return TextFormField(
                      onTap: () {
                        return Picker(
                          cancel: FlatButton(
                            onPressed: () {},
                            child: Text(''),
                          ),
                          hideHeader: false,
                          adapter: new DateTimePickerAdapter(
                            type: PickerDateTimeType.kYMDHM,
                          ),
                          title: new Text("Transfer Date"),
                          confirmText: 'OK',
                          confirmTextStyle: TextStyle(
                            color: Theme.of(context).primaryColor,
                          ),
                          onConfirm: (Picker picker, List value) {
                            DateFormat dateFormat =
                                DateFormat("yyyy-MM-dd HH:mm");
                            String apptime = dateFormat.format(
                                dateFormat.parse(picker.adapter.toString()));
                            _chargeBloc.showTransferDate(apptime);
                          },
                        ).showModal(context);
                      },
                      readOnly: true,
                      controller: textEditingController.data,
                      onChanged: _chargeBloc.transferDateEvent.add,
                      maxLines: 1,
                      autofocus: false,
                      decoration: InputDecoration(
                        hintText: "Transfer Date",
                        errorText: snapshot.error,
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget transferAmount() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text("Transfer Amount"),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
                stream: _chargeBloc.transferAmountEditingStream,
                builder: (context, textEditingController) {
                  return StreamBuilder(
                      stream: _chargeBloc.transferAmountStream,
                      builder: (context, snapshot) {
                        return TextFormField(
                          controller: textEditingController.data,
                          keyboardType: TextInputType.number,
                          maxLines: 1,
                          autofocus: false,
                          decoration: InputDecoration(
                            errorText: snapshot.error,
                            hintText: "Transfer Amount",
                          ),
                          onChanged: (String value) {
                            if (value != null && value != '') {
                              _chargeBloc.transferAmountEvent
                                  .add(double.parse(value));
                            }
                          },
                        );
                      });
                }),
          ),
        ],
      ),
    );
  }

  Widget transferSlip() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text("Transfer Slip"),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _chargeBloc.listImagesStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<File> images = snapshot.data;

                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Wrap(
                        children: imagesView(images),
                      ),
                      (images.length <= 0
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Divider(
                                  color: Theme.of(context).errorColor,
                                  thickness: 1.0,
                                ),
                                Text(
                                  'กรุณาแนบสลิปการโอนเงิน',
                                  style: TextStyle(
                                    color: Theme.of(context).errorColor,
                                    fontWeight: FontWeight.normal,
                                  ),
                                )
                              ],
                            )
                          : Container()),
                    ],
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget viewdata(String title, String data, int line) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            flex: 3,
            child: Text(
              data,
              overflow: TextOverflow.ellipsis,
              maxLines: line,
            ),
          ),
        ],
      ),
    );
  }

  Widget viewDetail() {
    return ListTile(
      title: Padding(
        padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
        child: Column(
          children: <Widget>[
            viewdata('Tax ID : ', widget.arguments.taxid ?? '', 1),
            Divider(),
            viewdata('Branch : ', widget.arguments.branch ?? '', 1),
            Divider(),
            viewdata('Name : ', widget.arguments.name ?? '', 1),
            Divider(),
            viewdata('Address : ', widget.arguments.address ?? '', 2),
            Divider(),
            viewdata('Station : ', widget.arguments.station ?? '', 1),
            Divider(),
          ],
        ),
      ),
    );
  }

  listPrice(String txtTitle, String awb, double price, PriceType type) {
    return ListTile(
      title: Text(
        txtTitle,
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(numberFormat.format(price).toString()),
          SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }

  listAwb() {
    List<Widget> list = [];

    List<AwbData> listAwb = widget.arguments.listAwb;

    double total = 0.0;
    listAwb.forEach((item) {
      double totalRow = item.itemAdvance +
          item.itemFreight +
          item.itemService +
          item.itemServiceVat;
      total += totalRow;
    });

    list.add(
      Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            CircleAvatar(
              child: Text(listAwb.length.toString()),
            ),
            Expanded(
              child: Center(
                child: Text(
                  'List AWB',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Text(
              'ราคา',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              width: 35,
            ),
          ],
        ),
      ),
    );

    list.add(
      Divider(),
    );

    list.add(
      Flexible(
        child: ListView.separated(
          itemCount: widget.arguments.listAwb.length,
          separatorBuilder: (context, index) => Divider(),
          itemBuilder: (context, index) {
            String hawbNo = widget.arguments.listAwb[index].hawbNo;
            double itemAdvance = widget.arguments.listAwb[index].itemAdvance;
            double itemFreight = widget.arguments.listAwb[index].itemFreight;
            double itemService = widget.arguments.listAwb[index].itemService;
            double itemServiceVat =
                widget.arguments.listAwb[index].itemServiceVat;
            // double total =
            //     itemAdvance + itemFreight + itemService + itemServiceVat;
            return ListTile(
              leading: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text((index + 1).toString()),
                  VerticalDivider(),
                ],
              ),
              title: Text(
                hawbNo,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              subtitle: Column(
                children: <Widget>[
                  itemFreight > 0
                      ? listPrice(
                          'ค่าขนส่ง (Freight)',
                          hawbNo,
                          itemFreight,
                          PriceType.freight,
                        )
                      : Container(),
                  itemAdvance > 0
                      ? listPrice(
                          'ค่าภาษี (Duty & Tax)',
                          hawbNo,
                          itemAdvance,
                          PriceType.advance,
                        )
                      : Container(),
                  itemService > 0
                      ? listPrice(
                          'ค่าดำเนินการ (Surcharge)',
                          hawbNo,
                          itemService,
                          PriceType.service,
                        )
                      : Container(),
                  itemServiceVat > 0
                      ? listPrice(
                          'ภาษีค่าดำเนินการ (Vat)',
                          hawbNo,
                          itemServiceVat,
                          PriceType.vat,
                        )
                      : Container(),
                ],
              ),
            );
          },
        ),
      ),
    );

    list.add(
      Divider(),
    );

    list.add(
      Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        child: Row(
          // mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              numberFormat.format(total).toString(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(width: 10.0),
            Text(
              'THB',
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );

    list.add(
      Divider(),
    );

    return Flexible(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Column(
          children: list,
        ),
      ),
    );
  }

  openPdf(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  paymnetSuccess(PaymentUrl payment) {
    Navigator.of(context).pushNamedAndRemoveUntil(
      RouteData.RouteRoot,
      ModalRoute.withName('/'),
    );

    Navigator.of(context).pushNamed(
      RouteData.RouteChargeSuccess,
      arguments: ChargeSuccessArguments(
        payment: payment,
      ),
    );
  }

  buttonCash() {
    return RaisedButton(
      onPressed: () {
        return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('ยืนยัน'),
              content: Text(
                  'ยืนยันความถูกต้องของข้อมูล และยืนยันการรับชำระ ด้วยเงินสด.'),
              contentPadding: EdgeInsets.all(10.0),
              actions: <Widget>[
                FlatButton(
                  child: Text('ปิด'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                RaisedButton(
                  child: Text('ยืนยัน'),
                  onPressed: () async {
                    Navigator.of(context).pop();
                    PaymentUrl res =
                        await _chargeBloc.payPayment(widget.arguments, 'CASH');
                    if (res != null) {
                      paymnetSuccess(res);
                    }
                  },
                ),
              ],
            );
          },
        );
      },
      child: Text('ยืนยันรับเงินสด'),
    );
  }

  buttonTransfer() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        // transferAmount(),
        // transferDate(),
        transferSlip(),
        StreamBuilder(
            stream: _chargeBloc.transferValid,
            builder: (context, snapshot) {
              return RaisedButton(
                onPressed: !snapshot.hasData
                    ? null
                    : () {
                        return showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text('ยืนยัน'),
                              content: Text(
                                  'ยืนยันความถูกต้องของข้อมูล และยืนยันการรับชำระด้วยการ โอนเงิน.'),
                              contentPadding: EdgeInsets.all(10.0),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text('ปิด'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                RaisedButton(
                                  child: Text('ยืนยัน'),
                                  onPressed: () async {
                                    Navigator.of(context).pop();
                                    PaymentUrl res =
                                        await _chargeBloc.payPayment(
                                            widget.arguments, 'TRANSFER');
                                    if (res != null) {
                                      paymnetSuccess(res);
                                    }
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      },
                child: Text('ยืนยันรับเงินโอน'),
              );
            }),
      ],
    );
  }

  buttonQr() {
    return RaisedButton(
      onPressed: () {
        return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('ยืนยัน'),
              content: Text('ยืนยันความถูกต้องของข้อมูล และสร้าง QR Code.'),
              contentPadding: EdgeInsets.all(10.0),
              actions: <Widget>[
                FlatButton(
                  child: Text('ปิด'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                RaisedButton(
                  child: Text('ยืนยัน'),
                  onPressed: () async {
                    Navigator.of(context).pop();
                    String chargeId = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChargeQrPage(
                          arguments: widget.arguments,
                        ),
                      ),
                    );
                    if (chargeId != null && chargeId != '') {
                      _chargeBloc.chargeIdEvent.add(chargeId);
                      PaymentUrl res =
                          await _chargeBloc.payPayment(widget.arguments, 'QR');
                      if (res != null) {
                        paymnetSuccess(res);
                      }
                    }
                  },
                ),
              ],
            );
          },
        );
      },
      child: Text('สร้าง QR Code'),
    );
  }

  buildTransfer() {
    return Container(
      // padding: EdgeInsets.all(10),
      // margin: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Text(
            'ข้อมูลการโอนเงิน',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          transferAmount(),
          transferDate(),
          transferSlip(),
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.all(10),
            child: RaisedButton(
              onPressed: () async {
                PaymentUrl res = await _chargeBloc.registerPayment(
                    widget.arguments, 'TRANSFER');
                if (res != null) {
                  return showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Success'),
                        content: Text('บันทึกโอนเงินเรียบร้อย รอยืนยัน'),
                        contentPadding: EdgeInsets.all(10.0),
                        actions: <Widget>[
                          RaisedButton(
                            child: Text('ปิด'),
                            onPressed: () {
                              Navigator.of(context)
                                  .pushReplacementNamed(RouteData.RouteRoot);
                            },
                          ),
                          // RaisedButton(
                          //   child: Text('ปิด'),
                          //   onPressed: () async {},
                          // ),
                        ],
                      );
                    },
                  );
                }
              },
              child: Text('บันทึกโอนเงิน'),
            ),
          )
        ],
      ),
    );
  }

  buildQR() {
    return StreamBuilder(
      stream: _chargeBloc.chargeIdStream,
      builder: (context, snapshot) {
        List<Widget> l = [];

        if (snapshot.hasData && snapshot.data != '') {
          l.add(
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              child: RaisedButton(
                onPressed: () async {
                  PaymentUrl res =
                      await _chargeBloc.registerPayment(widget.arguments, 'QR');
                  if (res != null) {
                    return showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text('Success'),
                          content: Text('สร้างใบเสร็จสำเร็จ'),
                          contentPadding: EdgeInsets.all(10.0),
                          actions: <Widget>[
                            FlatButton(
                              child: Text('ปิด'),
                              onPressed: () {
                                Navigator.of(context)
                                    .pushReplacementNamed(RouteData.RouteRoot);
                              },
                            ),
                            RaisedButton(
                              child: Text('ใบเสร็จ'),
                              onPressed: () async {
                                String url = await _chargeBloc
                                    .receiptUrl(res.receiptCode);
                                openPdf(url);
                              },
                            ),
                          ],
                        );
                      },
                    );
                  }
                },
                child: Text('ยืนยันรับเงิน QR'),
              ),
            ),
          );
          l.add(viewdata('Charge ID', snapshot.data, 1));
        } else {
          // l.add(viewdata('Charge ID', '', 1));
          l.add(
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              child: RaisedButton(
                onPressed: () async {
                  String chargeId = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChargeQrPage(
                        arguments: widget.arguments,
                      ),
                    ),
                  );
                  if (chargeId != null && chargeId != '') {
                    _chargeBloc.chargeIdEvent.add(chargeId);
                  }
                },
                child: Text('สร้าง QR'),
              ),
            ),
          );
        }
        return Column(
          children: l,
        );
      },
    );
  }

  double calTotal() {
    double total = 0.0;
    widget.arguments.listAwb.forEach((item) {
      double totalRow = item.itemAdvance +
          item.itemFreight +
          item.itemService +
          item.itemServiceVat;
      total += totalRow;
    });
    return total;
  }

  btnConfirm() {
    switch (widget.arguments.method) {
      case "CASH":
        return buttonCash();
        break;
      case "TRANSFER":
        return buttonTransfer();
        break;
      case "QR":
        return buttonQr();
        break;
      default:
        return Center(
          child: Text('Wrong Method'),
        );
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      appBar: AppBar(
        title: Text(
          'บันทึกชำระเงิน',
        ),
      ),
      body: Column(
        children: <Widget>[
          viewDetail(),
          listAwb(),
        ],
      ),
      bottomNavigationBar: Container(child: btnConfirm()),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _chargeBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
