import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:tos_payment/blocs/charge.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/utils/config.util.dart';

class ChargeQrPage extends StatefulWidget {
  ChargeQrPage({Key key, this.arguments}) : super(key: key);
  final ChargeArguments arguments;
  @override
  _ChargeQrPageState createState() => _ChargeQrPageState();
}

class _ChargeQrPageState extends State<ChargeQrPage> {
  FlutterWebviewPlugin webviewPlugin = new FlutterWebviewPlugin();
  ChargeBloc _chargeBloc;
  @override
  void initState() {
    _chargeBloc = ChargeBloc();
    webviewPlugin.onUrlChanged.listen((String url) {
      if (url.startsWith(Config.bankUiUrl + '/failed')) {}
      if (url.startsWith(Config.bankUiUrl + '/qrdone')) {
        String chargeId = url.split('/')[url.split('/').length - 1];
        Navigator.pop(context, chargeId);
      }
    });
    _chargeBloc.qrurl(
      widget.arguments,
    );
    super.initState();
  }

  @override
  void dispose() {
    _chargeBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Code'),
      ),
      body: StreamBuilder(
          stream: _chargeBloc.urlStream,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data != '') {
              return WebviewScaffold(
                url: snapshot.data,
                // appCacheEnabled: true,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}
