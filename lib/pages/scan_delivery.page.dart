import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:tos_payment/blocs/scan.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/enum.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:tos_payment/widgets/barcodeScannerOverlayShape.widget.dart';

class ScanDeliveryPage extends StatefulWidget {
  ScanDeliveryPage({Key key, this.arguments}) : super(key: key);
  final ScanArguments arguments;
  @override
  _ScanDeliveryPageState createState() => _ScanDeliveryPageState();
}

class _ScanDeliveryPageState extends State<ScanDeliveryPage> {
  ScanBloc _scanBloc;
  @override
  void initState() {
    _scanBloc = ScanBloc();
    super.initState();
  }

  @override
  void dispose() {
    _scanBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('build page scan');
    var form = Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Scan AWB (Deivery)'),
        actions: <Widget>[
          StreamBuilder(
            stream: _scanBloc.flashStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                FlashStatus status = snapshot.data;
                return IconButton(
                  icon: Icon(
                    status == FlashStatus.flashOn
                        ? Icons.flash_off
                        : Icons.flash_on,
                    // color: Colors.white,
                  ),
                  onPressed: _scanBloc.toggleFlash,
                );
              } else {
                return Container();
              }
            },
          ),
          StreamBuilder(
            stream: _scanBloc.cameraStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                CameraType status = snapshot.data;
                return IconButton(
                  icon: Icon(
                    status == CameraType.backCamera
                        ? Icons.camera_front
                        : Icons.camera_rear,
                    // color: Colors.white,
                  ),
                  onPressed: _scanBloc.flipCamera,
                );
              } else {
                return Container();
              }
            },
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: StreamBuilder(
              stream: _scanBloc.keyStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return QRView(
                    key: snapshot.data,
                    onQRViewCreated: _scanBloc.onCameraCreated,
                    overlay: BarcodeScannerOverlayShape(
                      borderColor: Colors.red,
                      borderRadius: 10,
                      borderLength: 30,
                      borderWidth: 10,
                      widthSize: 380,
                      heightSize: 200,
                    ),
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
          Expanded(
            flex: 2,
            child: StreamBuilder(
              stream: _scanBloc.listawbStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Widget> listwidget = [];
                  List<AwbData> list = snapshot.data;
                  list.forEach((item) {
                    listwidget.add(ListTile(title: Text(item.hawbNo)));
                  });
                  return Column(
                    children: <Widget>[
                      ListTile(
                        title: Center(
                          child: Text(
                            'List AWB',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        leading: CircleAvatar(
                          child: Text(list.length.toString()),
                        ),
                        trailing: CircleAvatar(
                          child: IconButton(
                            icon: Icon(Icons.playlist_add),
                            onPressed: () {
                              _scanBloc.manualawbEvent.add('');
                              _scanBloc.pauseCamera();
                              return showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('AWB'),
                                        TextField(
                                          decoration: InputDecoration(
                                            hintText: 'AWB Number',
                                          ),
                                          onChanged:
                                              _scanBloc.manualawbEvent.add,
                                        ),
                                      ],
                                    ),
                                    contentPadding: EdgeInsets.all(10.0),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text('ยกเลิก'),
                                        onPressed: () {
                                          Navigator.pop(context);
                                          _scanBloc.resumeCamera();
                                        },
                                      ),
                                      RaisedButton(
                                        child: Text('เพิ่ม'),
                                        onPressed: () async {
                                          await _scanBloc.awbadd();
                                          Navigator.pop(context);
                                          _scanBloc.resumeCamera();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                        ),
                      ),
                      Divider(),
                      Expanded(
                        flex: 1,
                        child: Scrollbar(
                          child: ListView.separated(
                            itemCount: list.length,
                            separatorBuilder: (context, index) => Divider(),
                            itemBuilder: (context, index) {
                              String hawbNo = list[index].hawbNo;
                              return ListTile(
                                leading: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text((index + 1).toString()),
                                    VerticalDivider(),
                                  ],
                                ),
                                title: Text(hawbNo),
                                trailing: IconButton(
                                  icon: Icon(Icons.delete),
                                  color: Colors.red,
                                  onPressed: () {
                                    return showDialog(
                                      context: context,
                                      barrierDismissible:
                                          false, // user must tap button for close dialog!
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text('ลบ AWB : $hawbNo'),
                                          actions: <Widget>[
                                            FlatButton(
                                              child: Text(
                                                'ยกเลิก',
                                                style: TextStyle(
                                                    color: Colors.black),
                                              ),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                            ),
                                            RaisedButton(
                                              child: Text(
                                                'ยืนยัน',
                                              ),
                                              onPressed: () async {
                                                await _scanBloc.awbdel(hawbNo);
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Divider(),
                      SizedBox(
                        width: double.infinity,
                        child: StreamBuilder(
                            stream: _scanBloc.awbValid,
                            builder: (context, snapshot) {
                              return RaisedButton(
                                // color: Theme.of(context).primaryColor,
                                onPressed: !snapshot.hasData
                                    ? null
                                    : () async {
                                        _scanBloc.pauseCamera();
                                        await Navigator.of(context).pushNamed(
                                          RouteData.RouteSelectReceipt,
                                          arguments: PaymentArguments(
                                            type: ProcessType.delivery,
                                            listAwb: list,
                                          ),
                                        );
                                        _scanBloc.resumeCamera();
                                      },
                                child: Text('ถัดไป'),
                              );
                            }),
                      ),
                    ],
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _scanBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
