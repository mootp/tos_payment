import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tos_payment/blocs/payment.bloc.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/utils/routes.util.dart';
import 'package:url_launcher/url_launcher.dart';

class PaymentListPage extends StatefulWidget {
  PaymentListPage({Key key, this.arguments}) : super(key: key);
  final PaymentListArguments arguments;
  @override
  _PaymentListPageState createState() => _PaymentListPageState();
}

class _PaymentListPageState extends State<PaymentListPage> {
  final NumberFormat numberFormat = new NumberFormat("#,##0.00", "en_US");
  final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');
  PaymentBloc _paymentBloc;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    _paymentBloc = PaymentBloc();
    String apptime = dateFormat.format(DateTime.now());

    _paymentBloc.fdateEvent.add(apptime);
    _paymentBloc.tdateEvent.add(apptime);

    _paymentBloc.loadPaymentList();
    super.initState();
  }

  @override
  void dispose() {
    _paymentBloc?.dispose();
    super.dispose();
  }

  Widget fdate() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('From Date'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _paymentBloc.fdateStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  DateTime tmpDate;
                  tmpDate = dateFormat.parse(snapshot.data);

                  return DateTimeField(
                    format: dateFormat,
                    initialValue: tmpDate,
                    resetIcon: null,
                    onChanged: (date) {
                      String strDate = dateFormat.format(date);
                      _paymentBloc.fdateEvent.add(strDate);
                      _paymentBloc.loadPaymentList();
                    },
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100),
                      );

                      if (date != null) {
                        return date;
                      } else {
                        return currentValue;
                      }
                    },
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget tdate() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text('To Date'),
          ),
          Expanded(
            flex: 3,
            child: StreamBuilder(
              stream: _paymentBloc.fdateStream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  DateTime tmpDate;
                  tmpDate = dateFormat.parse(snapshot.data);

                  return DateTimeField(
                    format: dateFormat,
                    initialValue: tmpDate,
                    resetIcon: null,
                    onChanged: (date) {
                      String strDate = dateFormat.format(date);
                      _paymentBloc.fdateEvent.add(strDate);
                      _paymentBloc.loadPaymentList();
                    },
                    onShowPicker: (context, currentValue) async {
                      final date = await showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100),
                      );

                      if (date != null) {
                        return date;
                      } else {
                        return currentValue;
                      }
                    },
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  openPdf(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  headlist() {
    return StreamBuilder(
      stream: _paymentBloc.listpayStream,
      builder: (context, snapshot) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              CircleAvatar(
                child: Text(
                    snapshot.hasData ? snapshot.data.length.toString() : '0'),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    'List',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  listPay() {
    return StreamBuilder(
      stream: _paymentBloc.listpayStream,
      builder: (context, snapshot) {
        List<Widget> list = [];

        if (snapshot.hasData) {
          List<PaymentData> listPay = snapshot.data;

          list.add(
            Divider(),
          );
          list.add(
            Flexible(
              child: SmartRefresher(
                enablePullDown: true,
                enablePullUp: false,
                controller: _refreshController,
                onRefresh: () {
                  _paymentBloc.loadPaymentList();
                  _refreshController.refreshCompleted();
                },
                child: ListView.separated(
                  itemCount: listPay.length,
                  separatorBuilder: (context, index) => Divider(),
                  itemBuilder: (context, index) {
                    PaymentH paymentH = listPay[index].paymentH;
                    List<PaymentD> paymentD = listPay[index].paymentD;
                    // List<PaymentD> paymentA = listPay[index].paymentA;
                    List<PaymentP> paymentP = listPay[index].paymentP;

                    List<Widget> lp = [];

                    String payawb = "";
                    double totalAmount = 0.0;
                    paymentD.forEach((element) {
                      payawb += (payawb == '' ? '' : ', ') + element.hawbNo;
                      totalAmount += ((element.itemAdvance ?? 0) +
                          (element.itemFreight ?? 0) +
                          (element.itemService ?? 0) +
                          (element.itemServiceVat ?? 0));
                    });
                    paymentP.forEach(
                      (element) {
                        List<PaymentPD> paymentPd = element.paymentPd;

                        double totalPay = 0;
                        paymentPd.forEach((element) {
                          totalPay += ((element.itemAdvance ?? 0) +
                              (element.itemFreight ?? 0) +
                              (element.itemService ?? 0) +
                              (element.itemServiceVat ?? 0));
                        });

                        lp.add(
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  datetimeFormat
                                      .format(element.paymentP.createDate),
                                  // style: TextStyle(fontSize: 12.0),
                                ),
                                Text(element.paymentP.payMethod),
                                RaisedButton(
                                  padding: EdgeInsets.all(0.0),
                                  child: Column(
                                    children: <Widget>[
                                      Text(element.paymentP.payStatus),
                                      Text(numberFormat
                                          .format(totalPay)
                                          .toString()),
                                    ],
                                  ),
                                  color: element.paymentP.payStatus == 'DONE'
                                      ? Colors.green
                                      : Colors.orange,
                                  onPressed: () {},
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                    return Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  10.0, 0.0, 10.0, 0.0),
                              child: Text((index + 1).toString() + ')'),
                            ),
                            Divider(),
                            Expanded(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.of(context).pushNamed(
                                          RouteData.RouteChargeDetail,
                                          arguments: ChargeDetailArguments(
                                            paymentCode: paymentH.paymentCode,
                                          ),
                                        );
                                      },
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'AWB : ' + payawb,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          Text('Amount : ' +
                                              numberFormat
                                                  .format(totalAmount)
                                                  .toString()),
                                          // Text(
                                          //   'Payment : ' + paymentH.paymentCode,
                                          //   overflow: TextOverflow.ellipsis,
                                          //   maxLines: 1,
                                          // ),
                                          Text(
                                            'Name : ' + paymentH.paymentName,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                          ),
                                          Text(
                                            'Address : ' + paymentH.paymentAddress,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  (paymentH.receiptCode != null &&
                                          paymentH.receiptCode != '' &&
                                          paymentH.paymentStatus == 'PAID')
                                      ? IconButton(
                                          icon: Icon(Icons.receipt),
                                          onPressed: () async {
                                            String url =
                                                await _paymentBloc.receiptUrl(
                                                    paymentH.receiptCode);
                                            openPdf(url);
                                          },
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(50.0, 0.0, 10.0, 0.0),
                          child: Column(
                            children: lp,
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
            ),
          );

          list.add(
            Divider(),
          );
        } else {
          list.add(
            Flexible(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }

        return Flexible(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: Column(children: list),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var form = Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'รายการรับชำระเงิน',
        ),
      ),
      body: Column(
        children: <Widget>[
          fdate(),
          tdate(),
          headlist(),
          listPay(),
        ],
      ),
    );

    var modal = new Stack(
      children: [
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.black),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        form,
        StreamBuilder(
          stream: _paymentBloc.loadStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data) {
                return modal;
              }
              return Container();
            }
            return Container();
          },
        )
      ],
    );
  }
}
