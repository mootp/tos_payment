import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:tos_payment/models/auth.model.dart';
import 'package:tos_payment/services/auth.service.dart';
import 'package:tos_payment/utils/config.util.dart';

class AuthInterceptors extends InterceptorsWrapper {
  final _auth = AuthService();
  final _dio = Dio();
  final _tokenDio = Dio();
  // final _auth = Auth();
  final storage = FlutterSecureStorage();

  @override
  Future onError(DioError err) async {
    if (err.response?.statusCode == 401) {
      RequestOptions options = err.response.request;
      _dio.lock();
      final String url = '${Config.apiUrl}/RefreshToken';
      final String token = await _auth.gettoken;
      final String refreshtoken = await _auth.getrefreshtoken;
      final RefreshToken data =
          RefreshToken(token: token, refreshToken: refreshtoken);

      return _tokenDio.post(url, data: data).then((d) async {
        final RefreshToken u = RefreshToken.fromJson(d.data);
        await storage.write(key: 'token', value: u.token);
        await storage.write(key: 'refreshtoken', value: u.refreshToken);

        if (options.data is FormData) {
          FormData formData = FormData();
          formData.fields.addAll(options.data.fields);
          for (MapEntry mapFile in options.data.files) {
            var file = await DefaultCacheManager().getFilePath();
            String pathfile =
                file.replaceAll("libCachedImageData", mapFile.value.filename);
            formData.files.add(MapEntry(
                mapFile.key,
                MultipartFile.fromFileSync(pathfile,
                    filename: mapFile.value.filename)));
          }
          options.data = formData;
          options.headers = {
            HttpHeaders.authorizationHeader: 'Bearer ${u.token.toString()}',
          };
        } else {
          options.headers = {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${u.token.toString()}',
          };
        }
      }).whenComplete(() {
        _dio.unlock();
      }).then((e) {
        return _dio.request(options.path, options: options);
      });
    }
    // if (err.response?.statusCode == 400) {
    //   _auth.signOut();
    // }
    return err;
  }
}
