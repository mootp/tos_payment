import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:tos_payment/models/auth.model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as secure;
import 'package:tos_payment/utils/config.util.dart';

abstract class BaseAuth {
  Future<String> get getcode;
  Future<String> get gettoken;
  Future<String> get getrefreshtoken;
  Future<bool> refreshToken();
  Future<bool> logout();
  Future<bool> login(String username, String password);
}

class AuthService implements BaseAuth {
  final _dio = Dio();
  final storage = new secure.FlutterSecureStorage();

  Future<String> get getcode async {
    try {
      return storage.read(key: 'code');
    } catch (e) {
      return null;
    }
  }

  Future<String> get gettoken async {
    try {
      return storage.read(key: 'token');
    } catch (e) {
      return null;
    }
  }

  Future<String> get getrefreshtoken async {
    try {
      return storage.read(key: 'refreshtoken');
    } catch (e) {
      return null;
    }
  }

  Future<bool> refreshToken() async {
    try {
      final String url = '${Config.apiUrl}/RefreshToken';
      final String token = await this.gettoken;
      final String refreshtoken = await this.getrefreshtoken;

      final RefreshToken data =
          RefreshToken(token: token, refreshToken: refreshtoken);

      _dio.options.connectTimeout = 10000; //5s
      final resp = await _dio.post(url, data: json.encode(data));
      if (resp.statusCode == 200) {
        RefreshToken u = RefreshToken.fromJson(resp.data);
        await storage.write(key: 'token', value: u.token);
        await storage.write(key: 'refreshtoken', value: u.refreshToken);
        return true;
      }
    } catch (e) {}
    return false;
  }

  Future<bool> logout() async {
    try {
      Future.wait([
        storage.deleteAll(),
      ]);
      return true;
    } catch (e) {
      throw Exception(e.message.toString());
    }
  }

  Future<bool> login(String username, String password) async {
    await storage.write(key: 'code', value: username);
    final url = '${Config.apiUrl}/Login';

    final LoginData data = LoginData(
      username: username,
      password: password,
    );

    try {
      // _dio.options.connectTimeout = 10000; //5s
      final res = await _dio.post(
        url,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status < 500;
          },
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
          },
        ),
        data: jsonEncode(data),
      );
      if (res.statusCode == 200) {
        final u = AccessToken.fromJson(res.data);
        // await storage.write(key: 'id', value: u.id.toString());
        await storage.write(key: 'token', value: u.token);
        await storage.write(key: 'refreshtoken', value: u.refreshToken);
        return true;
      } else {
        await storage.delete(key: 'code');
        throw new Exception(res.data);
      }
    } catch (e) {
      await storage.delete(key: 'code');
      throw Exception(e.message.toString());
    }
  }
}
