import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:tos_payment/models/attach.model.dart';
import 'package:tos_payment/models/charge.model.dart';
import 'package:tos_payment/models/api_tos.model.dart';
import 'package:tos_payment/models/history.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/models/station.model.dart';
import 'package:tos_payment/services/Interceptor.dart';
import 'package:tos_payment/services/auth.service.dart';
import 'package:tos_payment/utils/config.util.dart';
import 'package:path/path.dart' as path;

abstract class BaseTos {
  // Future<List<CodeText>> loadstation();
  Future<List<CodeText>> loadmethod();
  Future<QrOrderData> qrorder(QrChargeData data);
  Future<String> pdfreceipt(String receiptCode);
  Future<PaymentUrl> registerPayment(RegisterPaymentData data);
  Future<PaymentUrl> createPayment(CreatePaymentData data);
  Future<List<PaymentData>> loadPaymentList(String fdate, String tdate);
  Future<PaymentData> loadPaymentDetail(String paymentCode);
  Future<PaymentUrl> payPayment(PayPaymentData data);
  Future<List<ReceiptListData>> loadReceiptList(List<String> listAwb);
  Future<List<HistoryListData>> loadHistoryList(String fdate, String tdate);
  Future<bool> awbAttach(AwbAttachData data);
  Future<bool> awbUpdate(AwbUpdateData data);
  Future<List<StationData>> loadStationAccess();
}

class TosService implements BaseTos {
  final _dio = Dio();
  final _auth = AuthService();
  final _authInterceptors = AuthInterceptors();
  final DateFormat datetimeFormat = DateFormat('yyyy-MM-dd HH:mm');

  // Future<List<CodeText>> loadstation() async {
  //   List<CodeText> _data = [];
  //   _data.add(CodeText(code: 'BKK01S', text: 'BKK01S - RM3'));
  //   _data.add(CodeText(code: 'BKK02S', text: 'BKK02S - BNG'));
  //   _data.add(CodeText(code: 'BKK03S', text: 'BKK03S - DMG'));
  //   _data.add(CodeText(code: 'TEST02S', text: 'TEST02S - TEST'));
  //   return _data;
  // }

  Future<List<CodeText>> loadmethod() async {
    List<CodeText> _data = [];
    _data.add(CodeText(code: 'CASH', text: 'CASH'));
    _data.add(CodeText(code: 'TRANSFER', text: 'TRANSFER'));
    _data.add(CodeText(code: 'QR', text: 'QR CODE'));
    return _data;
  }

  Future<QrOrderData> qrorder(QrChargeData data) async {
    final String url = '${Config.bankUiUrl}/qrorder';
    try {
      final resp = await _dio.post(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
          },
        ),
        data: jsonEncode(data),
      );

      if (resp.statusCode == 200) {
        QrOrderData _data = QrOrderData.fromJson(resp.data);
        return _data;
      } else {
        // return false;
        var jsonerr = jsonDecode(resp.data);
        String err = "[qrorder] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<String> pdfreceipt(String receiptCode) async {
    final String url = '${Config.apiUrl}/GetPdfReceipt/' + receiptCode;
    final String token = await _auth.gettoken;
    try {
      final resp = await _dio.get(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
      );

      if (resp.statusCode == 200) {
        ReceiptUrl _data = ReceiptUrl.fromJson(resp.data);
        return _data.url;
      } else {
        // return false;
        var jsonerr = jsonDecode(resp.data);
        String err = "[pdfreceipt] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<PaymentUrl> registerPayment(RegisterPaymentData data) async {
    final url = '${Config.apiUrl}/RegisterPayment';
    final String token = await _auth.gettoken;
    var formData = FormData();
    formData.fields
      ..add(MapEntry('PaymentCode', data.paymentCode ?? ''))
      ..add(MapEntry('Name', data.name ?? ''))
      ..add(MapEntry('Branch', data.branch ?? ''))
      ..add(MapEntry('TaxID', data.taxId ?? ''))
      ..add(MapEntry('Address', data.address ?? ''))
      ..add(MapEntry('TransferDate', data.transferDate ?? ''))
      ..add(MapEntry('TransferAmount',
          data.transferAmount == null ? '' : data.transferAmount.toString()))
      ..add(MapEntry('ChargeId', data.chargeId ?? ''))
      ..add(MapEntry('ChargeMethod', data.chargeMethod ?? ''))
      ..add(MapEntry('PaymentStation', data.paymentStation ?? ''));

    // for (var i = 0; i < data.hawbNos.length; i++) {
    //   formData.fields
    //     ..add(MapEntry(
    //         'HawbNos[' + i.toString() + '].HawbNo', data.hawbNos[0].hawbNo))
    //     ..add(MapEntry('HawbNos[' + i.toString() + '].Freight',
    //         data.hawbNos[0].freight.toString()));
    // }

    await Future.wait(data.images.map((img) async {
      String filename = path.basename(img);
      formData.files.add(MapEntry(
        'Images',
        await MultipartFile.fromFile(img, filename: filename),
      ));
    }));

    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.post(
        url,
        options: Options(
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
        data: formData,
      );
      if (res.statusCode == 200) {
        PaymentUrl _data = new PaymentUrl.fromJson(res.data);
        return _data;
      } else {
        // return false;
        var jsonerr = jsonDecode(res.data);
        String err = "[registerPayment] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<PaymentUrl> createPayment(CreatePaymentData data) async {
    final url = '${Config.apiUrl}/CreatePayment';
    final String token = await _auth.gettoken;

    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.post(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
        data: jsonEncode(data),
      );
      if (res.statusCode == 200) {
        PaymentUrl _data = new PaymentUrl.fromJson(res.data);
        return _data;
      } else {
        // return false;
        var jsonerr = jsonDecode(res.data);
        String err = "[createPayment] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<List<PaymentData>> loadPaymentList(String fdate, String tdate) async {
    final url = '${Config.apiUrl}/PaymentList/' + fdate + '/' + tdate + '';
    final String token = await _auth.gettoken;
    List<PaymentData> _list = [];
    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.get(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
      );
      if (res.statusCode == 200) {
        var list = res.data.map((e) => new PaymentData.fromJson(e)).toList();
        list.forEach((element) {
          _list.add(element);
        });

        return _list;
      } else {
        // return false;
        var jsonerr = jsonDecode(res.data);
        String err = "[loadPaymentList] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<PaymentData> loadPaymentDetail(String paymentCode) async {
    final url = '${Config.apiUrl}/PaymentDetail/' + paymentCode + '';
    final String token = await _auth.gettoken;
    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.get(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
      );
      if (res.statusCode == 200) {
        PaymentData payment = new PaymentData.fromJson(res.data);
        return payment;
      } else {
        // return false;
        var jsonerr = jsonDecode(res.data);
        String err = "[loadPaymentDetail] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<PaymentUrl> payPayment(PayPaymentData data) async {
    final url = '${Config.apiUrl}/PayPayment';
    final String token = await _auth.gettoken;
    var formData = FormData();
    formData.fields
      ..add(MapEntry('PaymentCode', data.paymentCode ?? ''))
      ..add(MapEntry('ReceiptCode', data.receiptCode ?? ''))
      ..add(MapEntry('PayId', data.payId ?? ''))
      ..add(MapEntry('PayMethod', data.payMethod ?? ''));

    if (data.payDate != null) {
      formData.fields
          .add(MapEntry('PayDate', datetimeFormat.format(data.payDate)));
    }

    if (data.payAmount != null) {
      formData.fields.add(MapEntry('PayAmount', data.payAmount.toString()));
    }

    for (var i = 0; i < data.hawbNos.length; i++) {
      formData.fields
        ..add(
          MapEntry(
            'HawbNos[' + i.toString() + '].HawbNo',
            data.hawbNos[i].hawbNo,
          ),
        )
        ..add(
          MapEntry(
            'HawbNos[' + i.toString() + '].ItemAdvance',
            data.hawbNos[i].itemAdvance.toString(),
          ),
        )
        ..add(
          MapEntry(
            'HawbNos[' + i.toString() + '].ItemFreight',
            data.hawbNos[i].itemFreight.toString(),
          ),
        )
        ..add(
          MapEntry(
            'HawbNos[' + i.toString() + '].ItemService',
            data.hawbNos[i].itemService.toString(),
          ),
        )
        ..add(
          MapEntry(
            'HawbNos[' + i.toString() + '].ItemServiceVat',
            data.hawbNos[i].itemServiceVat.toString(),
          ),
        );
    }

    await Future.wait(data.payAttachs.map((img) async {
      String filename = path.basename(img);
      formData.files.add(MapEntry(
        'PayAttachs',
        await MultipartFile.fromFile(img, filename: filename),
      ));
    }));

    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.post(
        url,
        options: Options(
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
        data: formData,
      );
      if (res.statusCode == 200) {
        PaymentUrl _data = new PaymentUrl.fromJson(res.data);
        return _data;
      } else {
        // return false;
        var jsonerr = jsonDecode(res.data);
        String err = "[payPayment] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<List<ReceiptListData>> loadReceiptList(List<String> listAwb) async {
    final String url = '${Config.apiUrl}/ReceiptList';
    final String token = await _auth.gettoken;
    List<ReceiptListData> _list = [];
    try {
      final resp = await _dio.post(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
        data: jsonEncode(listAwb),
      );

      if (resp.statusCode == 200) {
        var list =
            resp.data.map((e) => new ReceiptListData.fromJson(e)).toList();
        list.forEach((element) {
          _list.add(element);
        });

        return _list;
      } else {
        // return false;
        var jsonerr = jsonDecode(resp.data);
        String err = "[loadReceiptList] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<bool> awbAttach(AwbAttachData data) async {
    final url = '${Config.apiUrl}/AwbAttach';
    final String token = await _auth.gettoken;
    var formData = FormData();

    for (var i = 0; i < data.hawbNos.length; i++) {
      formData.fields
        ..add(
          MapEntry(
            'HawbNos[' + i.toString() + '].HawbNo',
            data.hawbNos[i].hawbNo,
          ),
        );

      await Future.wait(data.hawbNos[i].awbAttachs.map((img) async {
        String filename = path.basename(img);
        formData.files.add(MapEntry(
          'HawbNos[' + i.toString() + '].AwbAttachs',
          await MultipartFile.fromFile(img, filename: filename),
        ));
      }));
    }

    await Future.wait(data.cardAttachs.map((img) async {
      String filename = path.basename(img);
      formData.files.add(MapEntry(
        'CardAttachs',
        await MultipartFile.fromFile(img, filename: filename),
      ));
    }));

    await Future.wait(data.photoAttachs.map((img) async {
      String filename = path.basename(img);
      formData.files.add(MapEntry(
        'PhotoAttachs',
        await MultipartFile.fromFile(img, filename: filename),
      ));
    }));

    await Future.wait(data.otherAttachs.map((img) async {
      String filename = path.basename(img);
      formData.files.add(MapEntry(
        'OtherAttachs',
        await MultipartFile.fromFile(img, filename: filename),
      ));
    }));

    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.post(
        url,
        options: Options(
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
        data: formData,
      );
      if (res.statusCode == 200) {
        return true;
      } else {
        // return false;
        var jsonerr = jsonDecode(res.data);
        String err = "[awbAttach] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<List<HistoryListData>> loadHistoryList(
      String fdate, String tdate) async {
    final url =
        '${Config.apiUrl}/ScanVerifyHistoryList/' + fdate + '/' + tdate + '';
    final String token = await _auth.gettoken;
    List<HistoryListData> _list = [];
    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.get(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
      );
      if (res.statusCode == 200) {
        var list =
            res.data.map((e) => new HistoryListData.fromJson(e)).toList();
        list.forEach((element) {
          _list.add(element);
        });

        return _list;
      } else {
        throw Exception(res.data);
      }
    } catch (e) {
      throw Exception(e.message.toString());
    }
  }

  Future<bool> awbUpdate(AwbUpdateData data) async {
    final url = '${Config.apiUrl}/AwbUpdate';
    final String token = await _auth.gettoken;
    try {
      _dio.interceptors
          .add(InterceptorsWrapper(onError: _authInterceptors.onError));
      final res = await _dio.post(
        url,
        options: Options(
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
        data: jsonEncode(data),
      );
      if (res.statusCode == 200) {
        return true;
      } else {
        // return false;
        var jsonerr = jsonDecode(res.data);
        String err = "[loadHistoryList] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }

  Future<List<StationData>> loadStationAccess() async {
    final String url = '${Config.apiUrl}/StationAccess';
    final String token = await _auth.gettoken;
    List<StationData> _list = [];
    try {
      final resp = await _dio.get(
        url,
        options: Options(
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
            HttpHeaders.authorizationHeader: 'Bearer ${token.toString()}',
          },
        ),
      );

      if (resp.statusCode == 200) {
        var list =
            resp.data.map((e) => new StationData.fromJson(e)).toList();
        list.forEach((element) {
          _list.add(element);
        });
        return _list;
      } else {
        // return false;
        var jsonerr = jsonDecode(resp.data);
        String err = "[loadStationAccess] error. please try again later. ";
        if (jsonerr['message'] != null) {
          err = jsonerr['message'];
        }
        throw Exception(err);
      }
    } catch (e) {
      throw e;
    }
  }
}
