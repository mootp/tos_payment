import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/models/enum.model.dart';

class AccountArguments {}

class HomeArguments {}

class LoginArguments {}

class PaymentArguments {
  ProcessType type;
  String taxId;
  String branch;
  String name;
  String address;
  String station;
  String method;
  List<AwbData> listAwb;
  PaymentArguments({
    this.type,
    this.taxId = '',
    this.branch = '',
    this.name = '',
    this.address = '',
    this.station = '',
    this.method = '',
    this.listAwb,
  });
}

class PaymentSummaryArguments {
  String paymentCode;
  String receiptCode;

  String taxId;
  String branch;
  String name;
  String address;
  String station;
  String method;
  String process;
  List<AwbData> listAwb;

  PaymentSummaryArguments({
    this.paymentCode,
    this.receiptCode,
    this.taxId = '',
    this.branch = '',
    this.name = '',
    this.address = '',
    this.station = '',
    this.method = '',
    this.process,
    this.listAwb,
  });
}

class RootArguments {
  String paymentCode;
  RootArguments({
    this.paymentCode,
  });
}

class ScanArguments {}

class SplashArguments {}

class ChargeArguments {
  String paymentCode;
  String receiptCode;
  String taxid;
  String branch;
  String name;
  String address;
  String station;
  String method;
  List<AwbData> listAwb;

  ChargeArguments({
    this.paymentCode,
    this.receiptCode,
    this.taxid,
    this.branch,
    this.name,
    this.address,
    this.station,
    this.method,
    this.listAwb,
  });
}

class ChargeQrArguments {
  double amount;
  String currency;
  String description;
  List<AwbData> listAwb;
  ChargeQrArguments({
    this.amount,
    this.currency,
    this.description,
    this.listAwb,
  });
}

class PaymentListArguments {}

class PaymentChartArguments {}

class ChargeDetailArguments {
  String paymentCode;
  double totalCharge;
  ChargeDetailArguments({
    this.paymentCode,
    this.totalCharge,
  });
}

class ChargeSuccessArguments {
  PaymentUrl payment;
  ChargeSuccessArguments({
    this.payment,
  });
}

class AttachArguments {
  List<AwbData> listAwb;

  AttachArguments({
    this.listAwb,
  });
}

class AttachSuccessArguments {}

class ScanHistoryArguments {}
