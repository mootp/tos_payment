import 'dart:convert';
class StationData {
    StationData({
        this.stationCode,
        this.stationName,
        this.stationDefault,
    });

    String stationCode;
    String stationName;
    String stationDefault;

    factory StationData.fromRawJson(String str) => StationData.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory StationData.fromJson(Map<String, dynamic> json) => StationData(
        stationCode: json["stationCode"] == null ? null : json["stationCode"],
        stationName: json["stationName"] == null ? null : json["stationName"],
        stationDefault: json["stationDefault"] == null ? null : json["stationDefault"],
    );

    Map<String, dynamic> toJson() => {
        "stationCode": stationCode == null ? null : stationCode,
        "stationName": stationName == null ? null : stationName,
        "stationDefault": stationDefault == null ? null : stationDefault,
    };
}
