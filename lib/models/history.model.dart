import 'dart:convert';

class HistoryListData {
  HistoryListData({
    this.verifyId,
    this.verifyCode,
    this.cardAttachs,
    this.photoAttachs,
    this.otherAttachs,
    this.hawbNos,
    this.createDate,
    this.createBy,
    this.updateDate,
    this.updateBy,
  });

  int verifyId;
  String verifyCode;
  List<String> cardAttachs;
  List<String> photoAttachs;
  List<String> otherAttachs;
  List<HawbNo> hawbNos;
  DateTime createDate;
  String createBy;
  DateTime updateDate;
  String updateBy;

  factory HistoryListData.fromRawJson(String str) =>
      HistoryListData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory HistoryListData.fromJson(Map<String, dynamic> json) =>
      HistoryListData(
        verifyId: json["verifyId"] == null ? null : json["verifyId"],
        verifyCode: json["verifyCode"] == null ? null : json["verifyCode"],
        cardAttachs: json["cardAttachs"] == null
            ? null
            : List<String>.from(json["cardAttachs"].map((x) => x)),
        photoAttachs: json["photoAttachs"] == null
            ? null
            : List<String>.from(json["photoAttachs"].map((x) => x)),
        otherAttachs: json["otherAttachs"] == null
            ? null
            : List<String>.from(json["otherAttachs"].map((x) => x)),
        hawbNos: json["hawbNos"] == null
            ? null
            : List<HawbNo>.from(json["hawbNos"].map((x) => HawbNo.fromJson(x))),
        createDate: json["createDate"] == null
            ? null
            : DateTime.parse(json["createDate"]),
        createBy: json["createBy"] == null ? null : json["createBy"],
        updateDate: json["updateDate"] == null
            ? null
            : DateTime.parse(json["updateDate"]),
        updateBy: json["updateBy"] == null ? null : json["updateBy"],
      );

  Map<String, dynamic> toJson() => {
        "verifyId": verifyId == null ? null : verifyId,
        "verifyCode": verifyCode == null ? null : verifyCode,
        "cardAttachs": cardAttachs == null
            ? null
            : List<dynamic>.from(cardAttachs.map((x) => x)),
        "photoAttachs": photoAttachs == null
            ? null
            : List<dynamic>.from(photoAttachs.map((x) => x)),
        "otherAttachs": otherAttachs == null
            ? null
            : List<dynamic>.from(otherAttachs.map((x) => x)),
        "hawbNos": hawbNos == null
            ? null
            : List<dynamic>.from(hawbNos.map((x) => x.toJson())),
        "createDate": createDate == null ? null : createDate.toIso8601String(),
        "createBy": createBy == null ? null : createBy,
        "updateDate": updateDate == null ? null : updateDate.toIso8601String(),
        "updateBy": updateBy == null ? null : updateBy,
      };
}

class HawbNo {
  HawbNo({
    this.awbId,
    this.hawbNo,
    this.awbAttachs,
    this.logs,
  });

  int awbId;
  String hawbNo;
  List<String> awbAttachs;
  List<Log> logs;

  factory HawbNo.fromRawJson(String str) => HawbNo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory HawbNo.fromJson(Map<String, dynamic> json) => HawbNo(
        awbId: json["awbId"] == null ? null : json["awbId"],
        hawbNo: json["hawbNo"] == null ? null : json["hawbNo"],
        awbAttachs: json["awbAttachs"] == null
            ? null
            : List<String>.from(json["awbAttachs"].map((x) => x)),
        logs: json["logs"] == null
            ? null
            : List<Log>.from(json["logs"].map((x) => Log.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "awbId": awbId == null ? null : awbId,
        "hawbNo": hawbNo == null ? null : hawbNo,
        "awbAttachs": awbAttachs == null
            ? null
            : List<dynamic>.from(awbAttachs.map((x) => x)),
        "logs": logs == null
            ? null
            : List<dynamic>.from(logs.map((x) => x.toJson())),
      };
}

class Log {
  Log({
    this.logId,
    this.awbFrom,
    this.awbTo,
  });

  int logId;
  String awbFrom;
  String awbTo;

  factory Log.fromRawJson(String str) => Log.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Log.fromJson(Map<String, dynamic> json) => Log(
        logId: json["logId"] == null ? null : json["logId"],
        awbFrom: json["awbFrom"] == null ? null : json["awbFrom"],
        awbTo: json["awbTo"] == null ? null : json["awbTo"],
      );

  Map<String, dynamic> toJson() => {
        "logId": logId == null ? null : logId,
        "awbFrom": awbFrom == null ? null : awbFrom,
        "awbTo": awbTo == null ? null : awbTo,
      };
}
