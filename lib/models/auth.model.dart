import 'dart:convert';

class AccessToken {
  int id;
  String token;
  String refreshToken;

  AccessToken({
    this.id,
    this.token,
    this.refreshToken,
  });

  factory AccessToken.fromRawJson(String str) =>
      AccessToken.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AccessToken.fromJson(Map<String, dynamic> json) => AccessToken(
        id: json["id"] == null ? null : json["id"],
        token: json["token"] == null ? null : json["token"],
        refreshToken:
            json["refreshToken"] == null ? null : json["refreshToken"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "token": token == null ? null : token,
        "refreshToken": refreshToken == null ? null : refreshToken,
      };
}

class RefreshToken {
  String token;
  String refreshToken;

  RefreshToken({
    this.token,
    this.refreshToken,
  });

  factory RefreshToken.fromRawJson(String str) =>
      RefreshToken.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RefreshToken.fromJson(Map<String, dynamic> json) => RefreshToken(
        token: json["token"] == null ? null : json["token"],
        refreshToken:
            json["refreshToken"] == null ? null : json["refreshToken"],
      );

  Map<String, dynamic> toJson() => {
        "token": token == null ? null : token,
        "refreshToken": refreshToken == null ? null : refreshToken,
      };
}

class LoginData {
  String username;
  String password;

  LoginData({
    this.username,
    this.password,
  });

  factory LoginData.fromRawJson(String str) =>
      LoginData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LoginData.fromJson(Map<String, dynamic> json) => LoginData(
        username: json["username"] == null ? null : json["username"],
        password: json["password"] == null ? null : json["password"],
      );

  Map<String, dynamic> toJson() => {
        "username": username == null ? null : username,
        "password": password == null ? null : password,
      };
}
