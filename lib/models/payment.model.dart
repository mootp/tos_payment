import 'dart:convert';

import 'dart:io';

class AwbVerifyData {
  String hawbNo;
  double itemAdvance;
  double itemFreight;
  double itemService;
  double itemServiceVat;
  List<File> awbAttach;
  AwbVerifyData({
    this.hawbNo = '',
    this.itemAdvance = 0.0,
    this.itemFreight = 0.0,
    this.itemService = 0.0,
    this.itemServiceVat = 0.0,
    this.awbAttach,
  });

  factory AwbVerifyData.fromRawJson(String str) => AwbVerifyData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AwbVerifyData.fromJson(Map<String, dynamic> json) => AwbVerifyData(
        hawbNo: json["HawbNo"] == null ? null : json["HawbNo"],
        itemAdvance:
            json["ItemAdvance"] == null ? null : json["ItemAdvance"].toDouble(),
        itemFreight:
            json["ItemFreight"] == null ? null : json["ItemFreight"].toDouble(),
        itemService:
            json["ItemService"] == null ? null : json["ItemService"].toDouble(),
        itemServiceVat: json["ItemServiceVat"] == null
            ? null
            : json["ItemServiceVat"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "HawbNo": hawbNo == null ? null : hawbNo,
        "ItemAdvance": itemAdvance == null ? null : itemAdvance,
        "ItemFreight": itemFreight == null ? null : itemFreight,
        "ItemService": itemService == null ? null : itemService,
        "ItemServiceVat": itemServiceVat == null ? null : itemServiceVat,
      };
}

class AwbData {
  String hawbNo;
  double itemAdvance;
  double itemFreight;
  double itemService;
  double itemServiceVat;
  AwbData({
    this.hawbNo = '',
    this.itemAdvance = 0.0,
    this.itemFreight = 0.0,
    this.itemService = 0.0,
    this.itemServiceVat = 0.0,
  });

  factory AwbData.fromRawJson(String str) => AwbData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AwbData.fromJson(Map<String, dynamic> json) => AwbData(
        hawbNo: json["HawbNo"] == null ? null : json["HawbNo"],
        itemAdvance:
            json["ItemAdvance"] == null ? null : json["ItemAdvance"].toDouble(),
        itemFreight:
            json["ItemFreight"] == null ? null : json["ItemFreight"].toDouble(),
        itemService:
            json["ItemService"] == null ? null : json["ItemService"].toDouble(),
        itemServiceVat: json["ItemServiceVat"] == null
            ? null
            : json["ItemServiceVat"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "HawbNo": hawbNo == null ? null : hawbNo,
        "ItemAdvance": itemAdvance == null ? null : itemAdvance,
        "ItemFreight": itemFreight == null ? null : itemFreight,
        "ItemService": itemService == null ? null : itemService,
        "ItemServiceVat": itemServiceVat == null ? null : itemServiceVat,
      };
}

class RegisterPaymentData {
  String paymentCode;
  String name;
  String branch;
  String taxId;
  String address;
  String transferDate;
  double transferAmount;
  String chargeId;
  String chargeMethod;
  String paymentStation;
  List<String> images;
  List<String> urlImages;
  List<RegisterPaymentHawb> hawbNos;

  RegisterPaymentData({
    this.paymentCode,
    this.name,
    this.branch,
    this.taxId,
    this.address,
    this.transferDate,
    this.transferAmount,
    this.chargeId,
    this.chargeMethod,
    this.paymentStation,
    this.images,
    this.urlImages,
    this.hawbNos,
  });

  factory RegisterPaymentData.fromRawJson(String str) =>
      RegisterPaymentData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RegisterPaymentData.fromJson(Map<String, dynamic> json) =>
      RegisterPaymentData(
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        name: json["name"] == null ? null : json["name"],
        branch: json["branch"] == null ? null : json["branch"],
        taxId: json["taxID"] == null ? null : json["taxID"],
        address: json["address"] == null ? null : json["address"],
        transferDate:
            json["transferDate"] == null ? null : json["transferDate"],
        transferAmount:
            json["transferAmount"] == null ? null : json["transferAmount"],
        chargeId: json["chargeId"] == null ? null : json["chargeId"],
        chargeMethod:
            json["chargeMethod"] == null ? null : json["chargeMethod"],
        paymentStation:
            json["paymentStation"] == null ? null : json["paymentStation"],
        images: json["images"] == null
            ? null
            : List<String>.from(json["images"].map((x) => x)),
        urlImages: json["urlImages"] == null
            ? null
            : List<String>.from(json["urlImages"].map((x) => x)),
        hawbNos: json["hawbNos"] == null
            ? null
            : List<RegisterPaymentHawb>.from(
                json["hawbNos"].map((x) => RegisterPaymentHawb.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "paymentCode": paymentCode == null ? null : paymentCode,
        "name": name == null ? null : name,
        "branch": branch == null ? null : branch,
        "taxID": taxId == null ? null : taxId,
        "address": address == null ? null : address,
        "transferDate": transferDate == null ? null : transferDate,
        "transferAmount": transferAmount == null ? null : transferAmount,
        "chargeId": chargeId == null ? null : chargeId,
        "chargeMethod": chargeMethod == null ? null : chargeMethod,
        "paymentStation": paymentStation == null ? null : paymentStation,
        "images":
            images == null ? null : List<dynamic>.from(images.map((x) => x)),
        "urlImages": urlImages == null
            ? null
            : List<dynamic>.from(urlImages.map((x) => x)),
        "hawbNos": hawbNos == null
            ? null
            : List<dynamic>.from(hawbNos.map((x) => x.toJson())),
      };
}

class RegisterPaymentHawb {
  String hawbNo;
  double itemAdvance;
  double itemFreight;
  double itemService;
  double itemServiceVat;

  RegisterPaymentHawb({
    this.hawbNo,
    this.itemAdvance,
    this.itemFreight,
    this.itemService,
    this.itemServiceVat,
  });

  factory RegisterPaymentHawb.fromRawJson(String str) =>
      RegisterPaymentHawb.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RegisterPaymentHawb.fromJson(Map<String, dynamic> json) =>
      RegisterPaymentHawb(
        hawbNo: json["hawbNo"] == null ? null : json["hawbNo"],
        itemAdvance:
            json["itemAdvance"] == null ? null : json["itemAdvance"].toDouble(),
        itemFreight:
            json["itemFreight"] == null ? null : json["itemFreight"].toDouble(),
        itemService:
            json["itemService"] == null ? null : json["itemService"].toDouble(),
        itemServiceVat: json["itemServiceVat"] == null
            ? null
            : json["itemServiceVat"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "hawbNo": hawbNo == null ? null : hawbNo,
        "itemAdvance": itemAdvance == null ? null : itemAdvance,
        "itemFreight": itemFreight == null ? null : itemFreight,
        "itemService": itemService == null ? null : itemService,
        "itemServiceVat": itemServiceVat == null ? null : itemServiceVat,
      };
}

class PaymentUrl {
  String paymentCode;
  String paymentStatus;
  String token;
  String url;
  String receiptCode;

  PaymentUrl({
    this.paymentCode,
    this.paymentStatus,
    this.token,
    this.url,
    this.receiptCode,
  });

  factory PaymentUrl.fromRawJson(String str) =>
      PaymentUrl.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentUrl.fromJson(Map<String, dynamic> json) => PaymentUrl(
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        paymentStatus:
            json["paymentStatus"] == null ? null : json["paymentStatus"],
        token: json["token"] == null ? null : json["token"],
        url: json["url"] == null ? null : json["url"],
        receiptCode: json["receiptCode"] == null ? null : json["receiptCode"],
      );

  Map<String, dynamic> toJson() => {
        "paymentCode": paymentCode == null ? null : paymentCode,
        "paymentStatus": paymentStatus == null ? null : paymentStatus,
        "token": token == null ? null : token,
        "url": url == null ? null : url,
        "receiptCode": receiptCode == null ? null : receiptCode,
      };
}

class ReceiptUrl {
  String hawbNo;
  String type;
  String url;

  ReceiptUrl({
    this.hawbNo,
    this.type,
    this.url,
  });

  factory ReceiptUrl.fromRawJson(String str) =>
      ReceiptUrl.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ReceiptUrl.fromJson(Map<String, dynamic> json) => ReceiptUrl(
        hawbNo: json["hawbNo"] == null ? null : json["hawbNo"],
        type: json["type"] == null ? null : json["type"],
        url: json["url"] == null ? null : json["url"],
      );

  Map<String, dynamic> toJson() => {
        "hawbNo": hawbNo == null ? null : hawbNo,
        "type": type == null ? null : type,
        "url": url == null ? null : url,
      };
}

class CreatePaymentData {
  CreatePaymentData({
    this.receiptCode,
    this.paymentCode,
    this.name,
    this.branch,
    this.taxId,
    this.address,
    this.station,
    this.process,
    this.hawbNos,
  });

  String receiptCode;
  String paymentCode;
  String name;
  String branch;
  String taxId;
  String address;
  String station;
  String process;
  List<AwbData> hawbNos;

  factory CreatePaymentData.fromRawJson(String str) =>
      CreatePaymentData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CreatePaymentData.fromJson(Map<String, dynamic> json) =>
      CreatePaymentData(
        receiptCode: json["receiptCode"] == null ? null : json["receiptCode"],
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        name: json["name"] == null ? null : json["name"],
        branch: json["branch"] == null ? null : json["branch"],
        taxId: json["taxID"] == null ? null : json["taxID"],
        address: json["address"] == null ? null : json["address"],
        station: json["station"] == null ? null : json["station"],
        process: json["process"] == null ? null : json["process"],
        hawbNos: json["hawbNos"] == null
            ? null
            : List<AwbData>.from(
                json["hawbNos"].map((x) => AwbData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "receiptCode": receiptCode == null ? null : receiptCode,
        "paymentCode": paymentCode == null ? null : paymentCode,
        "name": name == null ? null : name,
        "branch": branch == null ? null : branch,
        "taxID": taxId == null ? null : taxId,
        "address": address == null ? null : address,
        "station": station == null ? null : station,
        "process": process == null ? null : process,
        "hawbNos": hawbNos == null
            ? null
            : List<dynamic>.from(hawbNos.map((x) => x.toJson())),
      };
}

class PaymentData {
  PaymentData({
    this.paymentH,
    this.paymentD,
    this.paymentA,
    this.paymentP,
  });

  PaymentH paymentH;
  List<PaymentD> paymentD;
  List<PaymentD> paymentA;
  List<PaymentP> paymentP;

  factory PaymentData.fromRawJson(String str) =>
      PaymentData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentData.fromJson(Map<String, dynamic> json) => PaymentData(
        paymentH: json["paymentH"] == null
            ? null
            : PaymentH.fromJson(json["paymentH"]),
        paymentD: json["paymentD"] == null
            ? null
            : List<PaymentD>.from(
                json["paymentD"].map((x) => PaymentD.fromJson(x))),
        paymentA: json["paymentA"] == null
            ? null
            : List<PaymentD>.from(
                json["paymentA"].map((x) => PaymentD.fromJson(x))),
        paymentP: json["paymentP"] == null
            ? null
            : List<PaymentP>.from(
                json["paymentP"].map((x) => PaymentP.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "paymentH": paymentH == null ? null : paymentH.toJson(),
        "paymentD": paymentD == null
            ? null
            : List<PaymentD>.from(paymentD.map((x) => x.toJson())),
        "paymentA": paymentA == null
            ? null
            : List<PaymentD>.from(paymentA.map((x) => x.toJson())),
        "paymentP": paymentP == null
            ? null
            : List<PaymentP>.from(paymentP.map((x) => x.toJson())),
      };
}

class PaymentD {
  PaymentD({
    this.id,
    this.paymentCode,
    this.bookingCode,
    this.hawbNo,
    this.itemAdvance,
    this.itemFreight,
    this.itemService,
    this.itemServiceVat,
    this.payCode,
  });

  int id;
  String paymentCode;
  String bookingCode;
  String hawbNo;
  double itemAdvance;
  double itemFreight;
  double itemService;
  double itemServiceVat;
  String payCode;

  factory PaymentD.fromRawJson(String str) =>
      PaymentD.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentD.fromJson(Map<String, dynamic> json) => PaymentD(
        id: json["id"] == null ? null : json["id"],
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        bookingCode: json["bookingCode"] == null ? null : json["bookingCode"],
        hawbNo: json["hawbNo"] == null ? null : json["hawbNo"],
        itemAdvance:
            json["itemAdvance"] == null ? null : json["itemAdvance"].toDouble(),
        itemFreight:
            json["itemFreight"] == null ? null : json["itemFreight"].toDouble(),
        itemService:
            json["itemService"] == null ? null : json["itemService"].toDouble(),
        itemServiceVat: json["itemServiceVat"] == null
            ? null
            : json["itemServiceVat"].toDouble(),
        payCode: json["payCode"] == null ? null : json["payCode"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "paymentCode": paymentCode == null ? null : paymentCode,
        "bookingCode": bookingCode == null ? null : bookingCode,
        "hawbNo": hawbNo == null ? null : hawbNo,
        "itemAdvance": itemAdvance == null ? null : itemAdvance,
        "itemFreight": itemFreight == null ? null : itemFreight,
        "itemService": itemService == null ? null : itemService,
        "itemServiceVat": itemServiceVat == null ? null : itemServiceVat,
        "payCode": payCode == null ? null : payCode,
      };
}

class PaymentH {
  PaymentH({
    this.paymentCode,
    this.paymentDate,
    this.paymentStatus,
    this.paymentMethod,
    this.paymentStation,
    this.paymentName,
    this.paymentBranch,
    this.paymentTaxid,
    this.paymentAddress,
    this.paymentAddressSend,
    this.paymentEmailSend,
    this.paymentProcess,
    this.transferDate,
    this.transferAmount,
    this.transferSlip,
    this.cfPaidDate,
    this.cfPaidBy,
    this.paymentRemark,
    this.caDate,
    this.caBy,
    this.createDate,
    this.createBy,
    this.receiptCode,
    this.updateDate,
    this.updateBy,
  });

  String paymentCode;
  DateTime paymentDate;
  String paymentStatus;
  String paymentMethod;
  String paymentStation;
  String paymentName;
  String paymentBranch;
  String paymentTaxid;
  String paymentAddress;
  String paymentAddressSend;
  String paymentEmailSend;
  String paymentProcess;
  DateTime transferDate;
  double transferAmount;
  String transferSlip;
  DateTime cfPaidDate;
  String cfPaidBy;
  String paymentRemark;
  DateTime caDate;
  String caBy;
  DateTime createDate;
  String createBy;
  String receiptCode;
  DateTime updateDate;
  String updateBy;

  factory PaymentH.fromRawJson(String str) =>
      PaymentH.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentH.fromJson(Map<String, dynamic> json) => PaymentH(
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        paymentDate: json["paymentDate"] == null
            ? null
            : DateTime.parse(json["paymentDate"]),
        paymentStatus:
            json["paymentStatus"] == null ? null : json["paymentStatus"],
        paymentMethod:
            json["paymentMethod"] == null ? null : json["paymentMethod"],
        paymentStation:
            json["paymentStation"] == null ? null : json["paymentStation"],
        paymentName: json["paymentName"] == null ? null : json["paymentName"],
        paymentBranch:
            json["paymentBranch"] == null ? null : json["paymentBranch"],
        paymentTaxid:
            json["paymentTaxid"] == null ? null : json["paymentTaxid"],
        paymentAddress:
            json["paymentAddress"] == null ? null : json["paymentAddress"],
        paymentAddressSend: json["paymentAddressSend"] == null
            ? null
            : json["paymentAddressSend"],
        paymentEmailSend:
            json["paymentEmailSend"] == null ? null : json["paymentEmailSend"],
        paymentProcess:
            json["paymentProcess"] == null ? null : json["paymentProcess"],
        transferDate: json["transferDate"] == null
            ? null
            : DateTime.parse(json["transferDate"]),
        transferAmount: json["transferAmount"] == null
            ? null
            : json["transferAmount"].toDouble(),
        transferSlip:
            json["transferSlip"] == null ? null : json["transferSlip"],
        cfPaidDate: json["cfPaidDate"] == null
            ? null
            : DateTime.parse(json["cfPaidDate"]),
        cfPaidBy: json["cfPaidBy"] == null ? null : json["cfPaidBy"],
        paymentRemark:
            json["paymentRemark"] == null ? null : json["paymentRemark"],
        caDate: json["caDate"] == null ? null : DateTime.parse(json["caDate"]),
        caBy: json["caBy"] == null ? null : json["caBy"],
        createDate: json["createDate"] == null
            ? null
            : DateTime.parse(json["createDate"]),
        createBy: json["createBy"] == null ? null : json["createBy"],
        receiptCode: json["receiptCode"] == null ? null : json["receiptCode"],
        updateDate: json["updateDate"] == null
            ? null
            : DateTime.parse(json["updateDate"]),
        updateBy: json["updateBy"] == null ? null : json["updateBy"],
      );

  Map<String, dynamic> toJson() => {
        "paymentCode": paymentCode == null ? null : paymentCode,
        "paymentDate":
            paymentDate == null ? null : paymentDate.toIso8601String(),
        "paymentStatus": paymentStatus == null ? null : paymentStatus,
        "paymentMethod": paymentMethod == null ? null : paymentMethod,
        "paymentStation": paymentStation == null ? null : paymentStation,
        "paymentName": paymentName == null ? null : paymentName,
        "paymentBranch": paymentBranch == null ? null : paymentBranch,
        "paymentTaxid": paymentTaxid == null ? null : paymentTaxid,
        "paymentAddress": paymentAddress == null ? null : paymentAddress,
        "paymentAddressSend":
            paymentAddressSend == null ? null : paymentAddressSend,
        "paymentEmailSend": paymentEmailSend == null ? null : paymentEmailSend,
        "paymentProcess": paymentProcess == null ? null : paymentProcess,
        "transferDate":
            transferDate == null ? null : transferDate.toIso8601String(),
        "transferAmount": transferAmount == null ? null : transferAmount,
        "transferSlip": transferSlip == null ? null : transferSlip,
        "cfPaidDate": cfPaidDate == null ? null : cfPaidDate.toIso8601String(),
        "cfPaidBy": cfPaidBy == null ? null : cfPaidBy,
        "paymentRemark": paymentRemark == null ? null : paymentRemark,
        "caDate": caDate == null ? null : caDate.toIso8601String(),
        "caBy": caBy == null ? null : caBy,
        "createDate": createDate == null ? null : createDate.toIso8601String(),
        "createBy": createBy == null ? null : createBy,
        "receiptCode": receiptCode == null ? null : receiptCode,
        "updateDate": updateDate == null ? null : updateDate.toIso8601String(),
        "updateBy": updateBy == null ? null : updateBy,
      };
}

class PaymentP {
  PaymentP({
    this.paymentP,
    this.paymentPd,
    this.paymentPa,
  });

  PaymentPP paymentP;
  List<PaymentPD> paymentPd;
  List<PaymentPa> paymentPa;

  factory PaymentP.fromRawJson(String str) =>
      PaymentP.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentP.fromJson(Map<String, dynamic> json) => PaymentP(
        paymentP: json["paymentP"] == null
            ? null
            : PaymentPP.fromJson(json["paymentP"]),
        paymentPd: json["paymentPD"] == null
            ? null
            : List<PaymentPD>.from(
                json["paymentPD"].map((x) => PaymentPD.fromJson(x))),
        paymentPa: json["paymentPA"] == null
            ? null
            : List<PaymentPa>.from(
                json["paymentPA"].map((x) => PaymentPa.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "paymentP": paymentP == null ? null : paymentP.toJson(),
        "paymentPD": paymentPd == null
            ? null
            : List<PaymentPD>.from(paymentPd.map((x) => x.toJson())),
        "paymentPA": paymentPa == null
            ? null
            : List<PaymentPa>.from(paymentPa.map((x) => x.toJson())),
      };
}

class PaymentPP {
  PaymentPP({
    this.id,
    this.paymentCode,
    this.payCode,
    this.payStatus,
    this.payDate,
    this.payMethod,
    this.payId,
    this.payAmount,
    this.createDate,
    this.createBy,
    this.updateDate,
    this.updateBy,
  });

  int id;
  String paymentCode;
  String payCode;
  String payStatus;
  DateTime payDate;
  String payMethod;
  String payId;
  double payAmount;
  DateTime createDate;
  String createBy;
  DateTime updateDate;
  String updateBy;

  factory PaymentPP.fromRawJson(String str) =>
      PaymentPP.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentPP.fromJson(Map<String, dynamic> json) => PaymentPP(
        id: json["id"] == null ? null : json["id"],
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        payCode: json["payCode"] == null ? null : json["payCode"],
        payStatus: json["payStatus"] == null ? null : json["payStatus"],
        payDate:
            json["payDate"] == null ? null : DateTime.parse(json["payDate"]),
        payMethod: json["payMethod"] == null ? null : json["payMethod"],
        payId: json["payId"] == null ? null : json["payId"],
        payAmount:
            json["payAmount"] == null ? null : json["payAmount"].toDouble(),
        createDate: json["createDate"] == null
            ? null
            : DateTime.parse(json["createDate"]),
        createBy: json["createBy"] == null ? null : json["createBy"],
        updateDate: json["updateDate"] == null
            ? null
            : DateTime.parse(json["updateDate"]),
        updateBy: json["updateBy"] == null ? null : json["updateBy"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "paymentCode": paymentCode == null ? null : paymentCode,
        "payCode": payCode == null ? null : payCode,
        "payStatus": payStatus == null ? null : payStatus,
        "payDate": payDate == null ? null : payDate.toIso8601String(),
        "payMethod": payMethod == null ? null : payMethod,
        "payId": payId == null ? null : payId,
        "payAmount": payAmount == null ? null : payAmount,
        "createDate": createDate == null ? null : createDate.toIso8601String(),
        "createBy": createBy == null ? null : createBy,
        "updateDate": updateDate == null ? null : updateDate.toIso8601String(),
        "updateBy": updateBy == null ? null : updateBy,
      };
}

class PaymentPD {
  PaymentPD({
    this.id,
    this.paymentCode,
    this.bookingCode,
    this.hawbNo,
    this.itemAdvance,
    this.itemFreight,
    this.itemService,
    this.itemServiceVat,
    this.payCode,
  });

  int id;
  String paymentCode;
  String bookingCode;
  String hawbNo;
  double itemAdvance;
  double itemFreight;
  double itemService;
  double itemServiceVat;
  String payCode;

  factory PaymentPD.fromRawJson(String str) =>
      PaymentPD.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentPD.fromJson(Map<String, dynamic> json) => PaymentPD(
        id: json["id"] == null ? null : json["id"],
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        bookingCode: json["bookingCode"] == null ? null : json["bookingCode"],
        hawbNo: json["hawbNo"] == null ? null : json["hawbNo"],
        itemAdvance:
            json["itemAdvance"] == null ? null : json["itemAdvance"].toDouble(),
        itemFreight:
            json["itemFreight"] == null ? null : json["itemFreight"].toDouble(),
        itemService:
            json["itemService"] == null ? null : json["itemService"].toDouble(),
        itemServiceVat: json["itemServiceVat"] == null
            ? null
            : json["itemServiceVat"].toDouble(),
        payCode: json["payCode"] == null ? null : json["payCode"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "paymentCode": paymentCode == null ? null : paymentCode,
        "bookingCode": bookingCode == null ? null : bookingCode,
        "hawbNo": hawbNo == null ? null : hawbNo,
        "itemAdvance": itemAdvance == null ? null : itemAdvance,
        "itemFreight": itemFreight == null ? null : itemFreight,
        "itemService": itemService == null ? null : itemService,
        "itemServiceVat": itemServiceVat == null ? null : itemServiceVat,
        "payCode": payCode == null ? null : payCode,
      };
}

class PaymentPa {
  PaymentPa({
    this.id,
    this.paymentCode,
    this.payCode,
    this.payAttach,
    this.createDate,
    this.createBy,
    this.updateDate,
    this.updateBy,
  });

  int id;
  String paymentCode;
  String payCode;
  String payAttach;
  DateTime createDate;
  String createBy;
  DateTime updateDate;
  String updateBy;

  factory PaymentPa.fromRawJson(String str) =>
      PaymentPa.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PaymentPa.fromJson(Map<String, dynamic> json) => PaymentPa(
        id: json["id"] == null ? null : json["id"],
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        payCode: json["payCode"] == null ? null : json["payCode"],
        payAttach: json["payAttach"] == null ? null : json["payAttach"],
        createDate: json["createDate"] == null
            ? null
            : DateTime.parse(json["createDate"]),
        createBy: json["createBy"] == null ? null : json["createBy"],
        updateDate: json["updateDate"] == null
            ? null
            : DateTime.parse(json["updateDate"]),
        updateBy: json["updateBy"] == null ? null : json["updateBy"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "paymentCode": paymentCode == null ? null : paymentCode,
        "payCode": payCode == null ? null : payCode,
        "payAttach": payAttach == null ? null : payAttach,
        "createDate": createDate == null ? null : createDate.toIso8601String(),
        "createBy": createBy == null ? null : createBy,
        "updateDate": updateDate == null ? null : updateDate.toIso8601String(),
        "updateBy": updateBy == null ? null : updateBy,
      };
}

class PayPaymentData {
  String paymentCode;
  String receiptCode;
  String payId;
  String payMethod;
  DateTime payDate;
  double payAmount;
  List<String> payAttachs;
  List<AwbData> hawbNos;

  PayPaymentData({
    this.paymentCode,
    this.receiptCode,
    this.payId,
    this.payMethod,
    this.payDate,
    this.payAmount,
    this.payAttachs,
    this.hawbNos,
  });

  factory PayPaymentData.fromRawJson(String str) =>
      PayPaymentData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PayPaymentData.fromJson(Map<String, dynamic> json) => PayPaymentData(
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        receiptCode: json["receiptCode"] == null ? null : json["receiptCode"],
        payId: json["payId"] == null ? null : json["payId"],
        payMethod: json["payMethod"] == null ? null : json["payMethod"],
        payDate:
            json["payDate"] == null ? null : DateTime.parse(json["payDate"]),
        payAmount:
            json["payAmount"] == null ? null : json["payAmount"].toDouble(),
        payAttachs: json["payAttachs"] == null
            ? null
            : List<String>.from(json["payAttachs"].map((x) => x)),
        hawbNos: json["hawbNos"] == null
            ? null
            : List<AwbData>.from(
                json["hawbNos"].map((x) => AwbData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "paymentCode": paymentCode == null ? null : paymentCode,
        "receiptCode": receiptCode == null ? null : receiptCode,
        "payId": payId == null ? null : payId,
        "payMethod": payMethod == null ? null : payMethod,
        "payDate": payDate == null ? null : payDate.toIso8601String(),
        "payAmount": payAmount == null ? null : payAmount,
        "payAttachs": payAttachs == null
            ? null
            : List<String>.from(payAttachs.map((x) => x)),
        "hawbNos": hawbNos == null
            ? null
            : List<AwbData>.from(hawbNos.map((x) => x.toJson())),
      };
}
