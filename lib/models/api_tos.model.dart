import 'dart:convert';

class QrChargeData {
  QrChargeData({
    this.referenceOrder,
    this.referenceAwb,
    this.referenceCode,
    this.referenceBA,
    this.amount,
    this.currency,
    this.description,
    this.customerId,
    this.sof,
  });

  String referenceOrder;
  String referenceAwb;
  String referenceCode;
  String referenceBA;
  double amount;
  String currency;
  String description;
  dynamic customerId;
  String sof;

  factory QrChargeData.fromRawJson(String str) =>
      QrChargeData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory QrChargeData.fromJson(Map<String, dynamic> json) => QrChargeData(
        referenceOrder:
            json["referenceOrder"] == null ? null : json["referenceOrder"],
        referenceAwb:
            json["referenceAwb"] == null ? null : json["referenceAwb"],
        referenceCode:
            json["referenceCode"] == null ? null : json["referenceCode"],
        referenceBA: json["referenceBA"] == null ? null : json["referenceBA"],
        amount: json["amount"] == null ? null : json["amount"].toDouble(),
        currency: json["currency"] == null ? null : json["currency"],
        description: json["description"] == null ? null : json["description"],
        customerId: json["customerId"],
        sof: json["sof"] == null ? null : json["sof"],
      );

  Map<String, dynamic> toJson() => {
        "referenceOrder": referenceOrder == null ? null : referenceOrder,
        "referenceAwb": referenceAwb == null ? null : referenceAwb,
        "referenceCode": referenceCode == null ? null : referenceCode,
        "referenceBA": referenceBA == null ? null : referenceBA,
        
        "amount": amount == null ? null : amount,
        "currency": currency == null ? null : currency,
        "description": description == null ? null : description,
        "customerId": customerId,
        "sof": sof == null ? null : sof,
      };
}

class QrOrderData {
  QrOrderData({
    this.id,
    this.object,
    this.created,
    this.livemode,
    this.amount,
    this.currency,
    this.customer,
    this.description,
    this.metadata,
    this.status,
    this.referenceOrder,
    this.sourceType,
    this.additionalData,
    this.failureCode,
    this.failureMessage,
    this.expireTimeSeconds,
  });

  String id;
  String object;
  String created;
  bool livemode;
  double amount;
  String currency;
  dynamic customer;
  String description;
  dynamic metadata;
  String status;
  String referenceOrder;
  String sourceType;
  AdditionalData additionalData;
  dynamic failureCode;
  dynamic failureMessage;
  int expireTimeSeconds;

  factory QrOrderData.fromRawJson(String str) =>
      QrOrderData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory QrOrderData.fromJson(Map<String, dynamic> json) => QrOrderData(
        id: json["id"] == null ? null : json["id"],
        object: json["object"] == null ? null : json["object"],
        created: json["created"] == null ? null : json["created"],
        livemode: json["livemode"] == null ? null : json["livemode"],
        amount: json["amount"] == null ? null : json["amount"].toDouble(),
        currency: json["currency"] == null ? null : json["currency"],
        customer: json["customer"],
        description: json["description"] == null ? null : json["description"],
        metadata: json["metadata"],
        status: json["status"] == null ? null : json["status"],
        referenceOrder:
            json["reference_order"] == null ? null : json["reference_order"],
        sourceType: json["source_type"] == null ? null : json["source_type"],
        additionalData: json["additional_data"] == null
            ? null
            : AdditionalData.fromJson(json["additional_data"]),
        failureCode: json["failure_code"],
        failureMessage: json["failure_message"],
        expireTimeSeconds: json["expire_time_seconds"] == null
            ? null
            : json["expire_time_seconds"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "object": object == null ? null : object,
        "created": created == null ? null : created,
        "livemode": livemode == null ? null : livemode,
        "amount": amount == null ? null : amount,
        "currency": currency == null ? null : currency,
        "customer": customer,
        "description": description == null ? null : description,
        "metadata": metadata,
        "status": status == null ? null : status,
        "reference_order": referenceOrder == null ? null : referenceOrder,
        "source_type": sourceType == null ? null : sourceType,
        "additional_data":
            additionalData == null ? null : additionalData.toJson(),
        "failure_code": failureCode,
        "failure_message": failureMessage,
        "expire_time_seconds":
            expireTimeSeconds == null ? null : expireTimeSeconds,
      };
}

class AdditionalData {
  AdditionalData({
    this.term,
    this.mid,
    this.tid,
    this.smartpayId,
    this.campaignId,
  });

  dynamic term;
  dynamic mid;
  dynamic tid;
  dynamic smartpayId;
  dynamic campaignId;

  factory AdditionalData.fromRawJson(String str) =>
      AdditionalData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AdditionalData.fromJson(Map<String, dynamic> json) => AdditionalData(
        term: json["term"],
        mid: json["mid"],
        tid: json["tid"],
        smartpayId: json["smartpay_id"],
        campaignId: json["campaign_id"],
      );

  Map<String, dynamic> toJson() => {
        "term": term,
        "mid": mid,
        "tid": tid,
        "smartpay_id": smartpayId,
        "campaign_id": campaignId,
      };
}



class ReceiptListData {
    ReceiptListData({
        this.h,
        this.d,
    });

    H h;
    List<D> d;

    factory ReceiptListData.fromRawJson(String str) => ReceiptListData.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory ReceiptListData.fromJson(Map<String, dynamic> json) => ReceiptListData(
        h: json["h"] == null ? null : H.fromJson(json["h"]),
        d: json["d"] == null ? null : List<D>.from(json["d"].map((x) => D.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "h": h == null ? null : h.toJson(),
        "d": d == null ? null : List<D>.from(d.map((x) => x.toJson())),
    };
}

class D {
    D({
        this.id,
        this.receiptCode,
        this.hawbNo,
        this.recerenceNo1,
        this.bookingCode,
        this.itemAdvance,
        this.itemFreight,
        this.itemService,
        this.itemServiceVat,
    });

    int id;
    String receiptCode;
    String hawbNo;
    String recerenceNo1;
    String bookingCode;
    double itemAdvance;
    double itemFreight;
    double itemService;
    double itemServiceVat;

    factory D.fromRawJson(String str) => D.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory D.fromJson(Map<String, dynamic> json) => D(
        id: json["id"] == null ? null : json["id"],
        receiptCode: json["receiptCode"] == null ? null : json["receiptCode"],
        hawbNo: json["hawbNo"] == null ? null : json["hawbNo"],
        recerenceNo1: json["recerenceNo1"] == null ? null : json["recerenceNo1"],
        bookingCode: json["bookingCode"] == null ? null : json["bookingCode"],
        itemAdvance: json["itemAdvance"] == null ? null : json["itemAdvance"].toDouble(),
        itemFreight: json["itemFreight"] == null ? null : json["itemFreight"].toDouble(),
        itemService: json["itemService"] == null ? null : json["itemService"].toDouble(),
        itemServiceVat: json["itemServiceVat"] == null ? null : json["itemServiceVat"].toDouble(),
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "receiptCode": receiptCode == null ? null : receiptCode,
        "hawbNo": hawbNo == null ? null : hawbNo,
        "recerenceNo1": recerenceNo1 == null ? null : recerenceNo1,
        "bookingCode": bookingCode == null ? null : bookingCode,
        "itemAdvance": itemAdvance == null ? null : itemAdvance,
        "itemFreight": itemFreight == null ? null : itemFreight,
        "itemService": itemService == null ? null : itemService,
        "itemServiceVat": itemServiceVat == null ? null : itemServiceVat,
    };
}

class H {
    H({
        this.receiptCode,
        this.receiptRef,
        this.receiptType,
        this.receiptDate,
        this.receiptStatus,
        this.receiptMethod,
        this.paymentCode,
        this.chargeId,
        this.receiptStation,
        this.receiptCompany,
        this.receiptName,
        this.receiptBranch,
        this.receiptTaxId,
        this.receiptAddress,
        this.receiptAddressSend,
        this.receiptAmount,
        this.cfPaidDate,
        this.cfPaidBy,
        this.actualPaid,
        this.baDate,
        this.baBy,
        this.caDate,
        this.caBy,
        this.receiptRemark,
        this.collectDate,
        this.collectBy,
        this.statementNo,
        this.fnDate,
        this.fnBy,
        this.transferRunning,
        this.createDate,
        this.createBy,
        this.updateDate,
        this.updateBy,
    });

    String receiptCode;
    String receiptRef;
    String receiptType;
    DateTime receiptDate;
    String receiptStatus;
    String receiptMethod;
    String paymentCode;
    String chargeId;
    String receiptStation;
    String receiptCompany;
    String receiptName;
    String receiptBranch;
    String receiptTaxId;
    String receiptAddress;
    String receiptAddressSend;
    double receiptAmount;
    DateTime cfPaidDate;
    String cfPaidBy;
    double actualPaid;
    DateTime baDate;
    String baBy;
    DateTime caDate;
    String caBy;
    String receiptRemark;
    DateTime collectDate;
    String collectBy;
    String statementNo;
    DateTime fnDate;
    String fnBy;
    String transferRunning;
    DateTime createDate;
    String createBy;
    DateTime updateDate;
    String updateBy;

    factory H.fromRawJson(String str) => H.fromJson(json.decode(str));

    String toRawJson() => json.encode(toJson());

    factory H.fromJson(Map<String, dynamic> json) => H(
        receiptCode: json["receiptCode"] == null ? null : json["receiptCode"],
        receiptRef: json["receiptRef"] == null ? null : json["receiptRef"],
        receiptType: json["receiptType"] == null ? null : json["receiptType"],
        receiptDate: json["receiptDate"] == null ? null : DateTime.parse(json["receiptDate"]),
        receiptStatus: json["receiptStatus"] == null ? null : json["receiptStatus"],
        receiptMethod: json["receiptMethod"] == null ? null : json["receiptMethod"],
        paymentCode: json["paymentCode"] == null ? null : json["paymentCode"],
        chargeId: json["chargeId"] == null ? null : json["chargeId"],
        receiptStation: json["receiptStation"] == null ? null : json["receiptStation"],
        receiptCompany: json["receiptCompany"] == null ? null : json["receiptCompany"],
        receiptName: json["receiptName"] == null ? null : json["receiptName"],
        receiptBranch: json["receiptBranch"] == null ? null : json["receiptBranch"],
        receiptTaxId: json["receiptTaxId"] == null ? null : json["receiptTaxId"],
        receiptAddress: json["receiptAddress"] == null ? null : json["receiptAddress"],
        receiptAddressSend: json["receiptAddressSend"] == null ? null : json["receiptAddressSend"],
        receiptAmount: json["receiptAmount"] == null ? null : json["receiptAmount"].toDouble(),
        cfPaidDate: json["cfPaidDate"] == null ? null : DateTime.parse(json["cfPaidDate"]),
        cfPaidBy: json["cfPaidBy"] == null ? null : json["cfPaidBy"],
        actualPaid: json["actualPaid"] == null ? null : json["actualPaid"].toDouble(),
        baDate: json["baDate"] == null ? null : DateTime.parse(json["baDate"]),
        baBy: json["baBy"] == null ? null : json["baBy"],
        caDate: json["caDate"] == null ? null : DateTime.parse(json["caDate"]),
        caBy: json["caBy"] == null ? null : json["caBy"],
        receiptRemark: json["receiptRemark"] == null ? null : json["receiptRemark"],
        collectDate: json["collectDate"] == null ? null : DateTime.parse(json["collectDate"]),
        collectBy: json["collectBy"] == null ? null : json["collectBy"],
        statementNo: json["statementNo"] == null ? null : json["statementNo"],
        fnDate: json["fnDate"] == null ? null : DateTime.parse(json["fnDate"]),
        fnBy: json["fnBy"] == null ? null : json["fnBy"],
        transferRunning: json["transferRunning"] == null ? null : json["transferRunning"],
        createDate: json["createDate"] == null ? null : DateTime.parse(json["createDate"]),
        createBy: json["createBy"] == null ? null : json["createBy"],
        updateDate: json["updateDate"] == null ? null : DateTime.parse(json["updateDate"]),
        updateBy: json["updateBy"] == null ? null : json["updateBy"],
    );

    Map<String, dynamic> toJson() => {
        "receiptCode": receiptCode == null ? null : receiptCode,
        "receiptRef": receiptRef == null ? null : receiptRef,
        "receiptType": receiptType == null ? null : receiptType,
        "receiptDate": receiptDate == null ? null : receiptDate.toIso8601String(),
        "receiptStatus": receiptStatus == null ? null : receiptStatus,
        "receiptMethod": receiptMethod == null ? null : receiptMethod,
        "paymentCode": paymentCode == null ? null : paymentCode,
        "chargeId": chargeId == null ? null : chargeId,
        "receiptStation": receiptStation == null ? null : receiptStation,
        "receiptCompany": receiptCompany == null ? null : receiptCompany,
        "receiptName": receiptName == null ? null : receiptName,
        "receiptBranch": receiptBranch == null ? null : receiptBranch,
        "receiptTaxId": receiptTaxId == null ? null : receiptTaxId,
        "receiptAddress": receiptAddress == null ? null : receiptAddress,
        "receiptAddressSend": receiptAddressSend == null ? null : receiptAddressSend,
        "receiptAmount": receiptAmount == null ? null : receiptAmount,
        "cfPaidDate": cfPaidDate == null ? null : cfPaidDate.toIso8601String(),
        "cfPaidBy": cfPaidBy == null ? null : cfPaidBy,
        "actualPaid": actualPaid == null ? null : actualPaid,
        "baDate": baDate == null ? null : baDate.toIso8601String(),
        "baBy": baBy == null ? null : baBy,
        "caDate": caDate == null ? null : caDate.toIso8601String(),
        "caBy": caBy == null ? null : caBy,
        "receiptRemark": receiptRemark == null ? null : receiptRemark,
        "collectDate": collectDate == null ? null : collectDate.toIso8601String(),
        "collectBy": collectBy == null ? null : collectBy,
        "statementNo": statementNo == null ? null : statementNo,
        "fnDate": fnDate == null ? null : fnDate.toIso8601String(),
        "fnBy": fnBy == null ? null : fnBy,
        "transferRunning": transferRunning == null ? null : transferRunning,
        "createDate": createDate == null ? null : createDate.toIso8601String(),
        "createBy": createBy == null ? null : createBy,
        "updateDate": updateDate == null ? null : updateDate.toIso8601String(),
        "updateBy": updateBy == null ? null : updateBy,
    };
}