import 'dart:convert';

class AwbAttachData {
  List<String> cardAttachs;
  List<String> photoAttachs;
  List<String> otherAttachs;
  List<Awb> hawbNos;

  AwbAttachData({
    this.cardAttachs,
    this.photoAttachs,
    this.otherAttachs,
    this.hawbNos,
  });

  factory AwbAttachData.fromRawJson(String str) =>
      AwbAttachData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AwbAttachData.fromJson(Map<String, dynamic> json) => AwbAttachData(
        cardAttachs: json["cardAttachs"] == null
            ? null
            : List<String>.from(json["cardAttachs"].map((x) => x)),
        photoAttachs: json["photoAttachs"] == null
            ? null
            : List<String>.from(json["photoAttachs"].map((x) => x)),
        otherAttachs: json["otherAttachs"] == null
            ? null
            : List<String>.from(json["otherAttachs"].map((x) => x)),
        hawbNos: json["hawbNos"] == null
            ? null
            : List<Awb>.from(json["hawbNos"].map((x) => Awb.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "cardAttachs": cardAttachs == null
            ? null
            : List<String>.from(cardAttachs.map((x) => x)),
        "photoAttachs": photoAttachs == null
            ? null
            : List<String>.from(photoAttachs.map((x) => x)),
        "otherAttachs": otherAttachs == null
            ? null
            : List<String>.from(otherAttachs.map((x) => x)),
        "hawbNos": hawbNos == null
            ? null
            : List<Awb>.from(hawbNos.map((x) => x.toJson())),
      };
}

class Awb {
  String hawbNo;
  List<String> awbAttachs;
  Awb({
    this.hawbNo = '',
    this.awbAttachs,
  });

  factory Awb.fromRawJson(String str) => Awb.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Awb.fromJson(Map<String, dynamic> json) => Awb(
        hawbNo: json["HawbNo"] == null ? null : json["HawbNo"],
        awbAttachs: json["awbAttachs"] == null
            ? null
            : List<String>.from(json["awbAttachs"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "HawbNo": hawbNo == null ? null : hawbNo,
        "awbAttachs": awbAttachs == null
            ? null
            : List<String>.from(awbAttachs.map((x) => x)),
      };
}

class AwbUpdateData {
  AwbUpdateData({
    this.verifyId,
    this.verifyCode,
    this.hawbNos,
  });

  int verifyId;
  String verifyCode;
  List<AwbHawbNo> hawbNos;

  factory AwbUpdateData.fromRawJson(String str) =>
      AwbUpdateData.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AwbUpdateData.fromJson(Map<String, dynamic> json) => AwbUpdateData(
        verifyId: json["verifyId"] == null ? null : json["verifyId"],
        verifyCode: json["verifyCode"] == null ? null : json["verifyCode"],
        hawbNos: json["hawbNos"] == null
            ? null
            : List<AwbHawbNo>.from(json["hawbNos"].map((x) => AwbHawbNo.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "verifyId": verifyId == null ? null : verifyId,
        "verifyCode": verifyCode == null ? null : verifyCode,
        "hawbNos": hawbNos == null
            ? null
            : List<dynamic>.from(hawbNos.map((x) => x.toJson())),
      };
}

class AwbHawbNo {
  AwbHawbNo({
    this.awbId,
    this.hawbNo,
    this.oldHawbNo,
  });

  int awbId;
  String hawbNo;
  String oldHawbNo;

  factory AwbHawbNo.fromRawJson(String str) => AwbHawbNo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AwbHawbNo.fromJson(Map<String, dynamic> json) => AwbHawbNo(
        awbId: json["awbId"] == null ? null : json["awbId"],
        hawbNo: json["hawbNo"] == null ? null : json["hawbNo"],
        oldHawbNo: json["oldHawbNo"] == null ? null : json["oldHawbNo"],
      );

  Map<String, dynamic> toJson() => {
        "awbId": awbId == null ? null : awbId,
        "hawbNo": hawbNo == null ? null : hawbNo,
        "oldHawbNo": oldHawbNo == null ? null : oldHawbNo,
      };
}
