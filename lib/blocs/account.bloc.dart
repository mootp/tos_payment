import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/services/auth.service.dart';
class AccountBloc extends BlocBase with Validator {
  BaseAuth _authservice = new AuthService();
  AccountBloc() {
    loadEvent.add(false);
  }

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  @override
  void dispose() {
    _load?.close();
    super.dispose();
  }

  logout() async {
    bool chk = false;
    loadEvent.add(true);

    try {
      chk = await _authservice.logout();
      if (!chk) {
        Fluttertoast.showToast(msg: 'Error logout.');
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.message.toString());
    }

    loadEvent.add(false);

    return chk;
  }
}
