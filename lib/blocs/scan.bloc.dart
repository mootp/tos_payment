import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/models/enum.model.dart';
import 'package:tos_payment/models/payment.model.dart';

class ScanBloc extends BlocBase with Validator {
  ScanBloc() {
    loadEvent.add(false);
    keyEvent.add(GlobalKey(debugLabel: 'QR'));
    flashEvent.add(FlashStatus.flashOn);
    cameraEvent.add(CameraType.backCamera);
    listawbEvent.add([]);
    manualawbEvent.add('');
  }

  @override
  void dispose() {
    _load?.close();
    _controller?.close();
    _key?.close();
    _scan?.close();
    _flash?.close();
    _camera?.close();
    _listawb?.close();
    _manualawb?.close();
    super.dispose();
  }

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  final BehaviorSubject<QRViewController> _controller =
      new BehaviorSubject<QRViewController>();
  Stream<QRViewController> get controllerStream => _controller.stream;
  Sink<QRViewController> get controllerEvent => _controller.sink;

  final BehaviorSubject<GlobalKey> _key = new BehaviorSubject<GlobalKey>();
  Stream<GlobalKey> get keyStream => _key.stream;
  Sink<GlobalKey> get keyEvent => _key.sink;

  final BehaviorSubject<String> _scan = new BehaviorSubject<String>();
  Stream<String> get scanStream => _scan.stream;
  Sink<String> get scanEvent => _scan.sink;

  final BehaviorSubject<FlashStatus> _flash =
      new BehaviorSubject<FlashStatus>();
  Stream<FlashStatus> get flashStream => _flash.stream;
  Sink<FlashStatus> get flashEvent => _flash.sink;

  final BehaviorSubject<CameraType> _camera = new BehaviorSubject<CameraType>();
  Stream<CameraType> get cameraStream => _camera.stream;
  Sink<CameraType> get cameraEvent => _camera.sink;

  final BehaviorSubject<List<AwbData>> _listawb =
      new BehaviorSubject<List<AwbData>>();
  Stream<List<AwbData>> get listawbStream => _listawb.stream;
  Stream<List<AwbData>> get listawbValidate =>
      _listawb.stream.transform(validateListAwbNoTotal);
  Sink<List<AwbData>> get listawbEvent => _listawb.sink;

  final BehaviorSubject<String> _manualawb = new BehaviorSubject<String>();
  Stream<String> get manualawbStream => _manualawb.stream;
  Sink<String> get manualawbEvent => _manualawb.sink;

  Stream<bool> get awbValid => Rx.combineLatest([listawbValidate], (a) => true);

  onCameraCreated(QRViewController controller) {
    controllerEvent.add(controller);
    controller.scannedDataStream.listen((scanData) {
      scanCallback(scanData);
    });
  }

  toggleFlash() {
    QRViewController controller = _controller.value;
    FlashStatus status = _flash.value;
    if (controller != null) {
      controller.toggleFlash();
      if (status == FlashStatus.flashOn) {
        flashEvent.add(FlashStatus.flashOff);
      } else {
        flashEvent.add(FlashStatus.flashOn);
      }
    }
  }

  flipCamera() {
    QRViewController controller = _controller.value;
    CameraType status = _camera.value;

    if (controller != null) {
      controller.flipCamera();
      if (status == CameraType.backCamera) {
        cameraEvent.add(CameraType.frontCamera);
      } else {
        cameraEvent.add(CameraType.backCamera);
      }
    }
  }

  pauseCamera() {
    QRViewController controller = _controller.value;
    controller.pauseCamera();
  }

  resumeCamera() {
    QRViewController controller = _controller.value;
    controller.resumeCamera();
  }

  scanCallback(String scanData) {
    List<AwbData> list = _listawb.value;
    var check = list.where((d) => d.hawbNo == scanData);
    if (check.length == 0) {
      list.add(
        AwbData(
          hawbNo: scanData,
          itemAdvance: 0.0,
          itemFreight: 0.0,
          itemService: 0.0,
          itemServiceVat: 0.0,
        ),
      );
      listawbEvent.add(list);
    }
  }

  awbadd() {
    String awb = _manualawb.value.toUpperCase().trim();
    if (awb == '') {
      manualawbEvent.add('');
    } else {
      List<AwbData> tmplist = _listawb.value;
      var check = tmplist.where((d) => d.hawbNo == awb);
      if (check.length == 0) {
        tmplist.add(
          AwbData(
            hawbNo: awb,
            itemAdvance: 0.0,
            itemFreight: 0.0,
            itemService: 0.0,
            itemServiceVat: 0.0,
          ),
        );
        listawbEvent.add(tmplist);
      }
      manualawbEvent.add('');
    }
  }

  awbdel(String awb) {
    List<AwbData> tmplist = _listawb.value;
    tmplist.removeWhere((item) => item.hawbNo == awb);
    listawbEvent.add(tmplist);
  }
}
