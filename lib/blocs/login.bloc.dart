import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/services/auth.service.dart';

class LoginBloc extends BlocBase with Validator {
  BaseAuth _authservice = new AuthService();
  LoginBloc() {
    loadEvent.add(false);
  }

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  // final BehaviorSubject<List<Widget>> _listPage =
  //     new BehaviorSubject<List<Widget>>();
  // Stream<List<Widget>> get listPageStream => _listPage.stream;
  // Sink<List<Widget>> get listPageEvent => _listPage.sink;

  @override
  void dispose() {
    _load?.close();
    super.dispose();
  }

  login(String user, String pass) async {
    bool chk = false;
    loadEvent.add(true);

    try {
      chk = await _authservice.login(user, pass);
      if (!chk) {
        Fluttertoast.showToast(msg: 'Username or Password is wrong');
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.message.toString());
    }

    loadEvent.add(false);

    return chk;
  }
}
