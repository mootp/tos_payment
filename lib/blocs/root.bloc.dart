import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/pages/account.page.dart';
import 'package:tos_payment/pages/home.page.dart';

class RootBloc extends BlocBase with Validator {
  RootBloc() {
    // set init data

    pageindexEvent.add(0);
    listPageEvent.add([
      HomePage(),
      AccountPage(),
    ]);
  }

  final BehaviorSubject<int> _pageindex = new BehaviorSubject<int>();
  Stream<int> get pageindexStream => _pageindex.stream;
  Sink<int> get pageindexEvent => _pageindex.sink;

  final BehaviorSubject<List<Widget>> _listPage =
      new BehaviorSubject<List<Widget>>();
  Stream<List<Widget>> get listPageStream => _listPage.stream;
  Sink<List<Widget>> get listPageEvent => _listPage.sink;

  @override
  void dispose() {
    _pageindex?.close();
    _listPage?.close();

    super.dispose();
  }
}
