import 'dart:io';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/models/api_tos.model.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/enum.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/services/auth.service.dart';
import 'package:tos_payment/services/tos.service.dart';
import 'package:tos_payment/utils/config.util.dart';

class ChargeBloc extends BlocBase with Validator {
  BaseTos _tosservice = new TosService();
  BaseAuth _authservice = new AuthService();
  ChargeBloc() {
    loadEvent.add(false);
    listImagesEvent.add(List<File>());

    transferDateTextEditingEvent.add(TextEditingController());
    transferDateTextEditingEvent.add(TextEditingController());
    transferDateEvent.add("");
    transferAmountEvent.add(null);
    // orderIdEvent.add('');
    // chargeIdEvent.add('');
    urlEvent.add('');

    priceEvent.add(0.0);
    listawbEvent.add([]);
  }

  @override
  void dispose() {
    _listImages?.close();
    _transferDateTextEditing?.close();
    _transferDate?.close();
    _transferAmountTextEditing?.close();
    _transferAmount?.close();
    _orderId?.close();
    _chargeId?.close();
    _url?.close();
    _load?.close();
    _paymentCode?.close();
    _price?.close();
    _listawb?.close();
    _payment?.close();
    super.dispose();
  }

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  final BehaviorSubject<List<File>> _listImages =
      new BehaviorSubject<List<File>>();
  Stream<List<File>> get listImagesStream => _listImages.stream;
  Stream<List<File>> get listImagesvalidate =>
      _listImages.stream.transform(validateListFile);
  Sink<List<File>> get listImagesEvent => _listImages.sink;

  final BehaviorSubject<TextEditingController> _transferDateTextEditing =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get transferDateTextEditingStream =>
      _transferDateTextEditing.stream;
  Sink<TextEditingController> get transferDateTextEditingEvent =>
      _transferDateTextEditing.sink;

  final BehaviorSubject<String> _transferDate = new BehaviorSubject<String>();
  Stream<String> get transferDateStream =>
      _transferDate.stream.transform(validateEmpty);
  Sink<String> get transferDateEvent => _transferDate.sink;

  final BehaviorSubject<TextEditingController> _transferAmountTextEditing =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get transferAmountEditingStream =>
      _transferAmountTextEditing.stream;
  Sink<TextEditingController> get transferAmountEditingEvent =>
      _transferAmountTextEditing.sink;

  final BehaviorSubject<double> _transferAmount = new BehaviorSubject<double>();
  Stream<double> get transferAmountStream =>
      _transferAmount.stream.transform(validateDoubleEmpty);
  Sink<double> get transferAmountEvent => _transferAmount.sink;

  final BehaviorSubject<String> _orderId = new BehaviorSubject<String>();
  Stream<String> get orderIdStream => _orderId.stream;
  Sink<String> get orderIdEvent => _orderId.sink;

  final BehaviorSubject<String> _chargeId = new BehaviorSubject<String>();
  Stream<String> get chargeIdStream => _chargeId.stream;
  Sink<String> get chargeIdEvent => _chargeId.sink;

  final BehaviorSubject<String> _url = new BehaviorSubject<String>();
  Stream<String> get urlStream => _url.stream;
  Sink<String> get urlEvent => _url.sink;

  final BehaviorSubject<String> _paymentCode = new BehaviorSubject<String>();
  Stream<String> get paymentCodeStream => _paymentCode.stream;
  Sink<String> get paymentCodeEvent => _paymentCode.sink;

  final BehaviorSubject<double> _price = new BehaviorSubject<double>();
  Stream<double> get priceStream => _price.stream;
  Sink<double> get priceEvent => _price.sink;

  final BehaviorSubject<List<AwbData>> _listawb =
      new BehaviorSubject<List<AwbData>>();
  Stream<List<AwbData>> get listawbStream => _listawb.stream;
  Stream<List<AwbData>> get listawbValidate =>
      _listawb.stream.transform(validateListAwb);
  Sink<List<AwbData>> get listawbEvent => _listawb.sink;

  final BehaviorSubject<PaymentData> _payment =
      new BehaviorSubject<PaymentData>();
  Stream<PaymentData> get paymentStream => _payment.stream;
  Sink<PaymentData> get paymentEvent => _payment.sink;

  Stream<bool> get transferValid =>
      Rx.combineLatest([listImagesvalidate], (a) => true);

  addImage(File pic) {
    List<File> resultList = _listImages.value;
    resultList.add(pic);
    listImagesEvent.add(resultList);
  }

  removeImage(String delpath) {
    List<File> listImage = _listImages.value;
    for (var i = 0; i < listImage.length; i++) {
      String path = listImage[i].path;
      if (path == delpath) {
        listImage.removeAt(i);
      }
    }
    listImagesEvent.add(listImage);
  }

  showTransferDate(String time) {
    String txt = time;
    transferDateEvent.add(txt);
    _transferDateTextEditing.value.text = txt;
  }

  qrurl(ChargeArguments arguments) async {
    try {
      List<AwbData> listAwb = arguments.listAwb;
      String fawb = '';
      String awb = '';
      double total = 0.0;
      listAwb.forEach((item) {
        double totalRow = item.itemAdvance +
            item.itemFreight +
            item.itemService +
            item.itemServiceVat;
        total += totalRow;

        if (fawb == '') {
          fawb = item.hawbNo;
        }
        awb += (awb == '' ? '' : ',') + item.hawbNo;
      });

      String ba = await _authservice.getcode;

      QrOrderData order = await _tosservice.qrorder(
        QrChargeData(
          referenceOrder: fawb,
          referenceAwb: awb,
          referenceCode: arguments.station,
          referenceBA: ba,
          amount: total,
          currency: 'THB',
          description: arguments.name,
        ),
      );
      String bankUiUrl = Config.bankUiUrl;
      String url =
          bankUiUrl + '/qrkbank/' + order.id + '/' + order.amount.toString();
      urlEvent.add(url);
    } catch (e) {
      Fluttertoast.showToast(msg: e.message.toString());
    }
  }

  receiptUrl(String receiptCode) async {
    try {
      String url = await _tosservice.pdfreceipt(receiptCode);
      return url;
    } catch (e) {
      Fluttertoast.showToast(msg: e.message.toString());
    }
  }

  registerPayment(ChargeArguments arguments, String method) async {
    loadEvent.add(true);
    try {
      List<File> listImage = _listImages.value;
      List<String> listimg = [];
      List<RegisterPaymentHawb> listAwb = [];

      await Future.wait(listImage.map((img) async {
        String path = img.path;
        listimg.add(path);
      }));

      arguments.listAwb.forEach((item) {
        listAwb.add(RegisterPaymentHawb(
          itemAdvance: item.itemAdvance,
          itemFreight: item.itemFreight,
          itemService: item.itemService,
          itemServiceVat: item.itemServiceVat,
          hawbNo: item.hawbNo,
        ));
      });

      RegisterPaymentData data = new RegisterPaymentData(
        taxId: arguments.taxid,
        branch: arguments.branch,
        name: arguments.name,
        address: arguments.address,
        chargeId: _chargeId.value,
        hawbNos: listAwb,
        images: listimg,
        transferDate: _transferDate.value,
        transferAmount: _transferAmount.value,
        chargeMethod: method,
        paymentStation: arguments.station,
      );
      PaymentUrl res = await _tosservice.registerPayment(data);
      loadEvent.add(false);
      return res;
    } catch (e) {
      loadEvent.add(false);
      Fluttertoast.showToast(msg: e.message.toString());
    }
  }

  loadPaymentDetail() async {
    loadEvent.add(true);
    String paymentCode = _paymentCode.value;
    PaymentData data = await _tosservice.loadPaymentDetail(paymentCode);

    paymentEvent.add(data);

    List<AwbData> list = [];
    data.paymentD.forEach((element) {
      list.add(
        AwbData(
          hawbNo: element.hawbNo,
          itemAdvance: element.itemAdvance ?? 0,
          itemFreight: element.itemFreight ?? 0,
          itemService: element.itemService ?? 0,
          itemServiceVat: element.itemServiceVat ?? 0,
        ),
      );
    });
    listawbEvent.add(list);
    loadEvent.add(false);
  }

  editawbprice(String awb, PriceType type) {
    List<AwbData> list = _listawb.value;
    double price = _price.value;

    list.where((d) => d.hawbNo == awb).forEach((item) {
      if (type == PriceType.advance) {
        item.itemAdvance = price;
      } else if (type == PriceType.freight) {
        item.itemFreight = price;
      } else if (type == PriceType.service) {
        item.itemService = price;
      } else if (type == PriceType.vat) {
        item.itemServiceVat = price;
      }

      // item.itemAdvance: item.itemAdvance,
      //     item.itemFreight: 0.0,
      //     item.itemService: 0.0,
      //     item.itemServiceVat: 0.0,
    });

    priceEvent.add(0.0);
    listawbEvent.add(list);
  }

  payPayment(ChargeArguments arguments, String method) async {
    loadEvent.add(true);
    try {
      List<File> listImage = _listImages.value;
      List<String> payAttachs = [];
      List<AwbData> hawbNos = [];

      await Future.wait(listImage.map((img) async {
        String path = img.path;
        payAttachs.add(path);
      }));

      arguments.listAwb.forEach((item) {
        hawbNos.add(AwbData(
          hawbNo: item.hawbNo,
          itemAdvance: item.itemAdvance,
          itemFreight: item.itemFreight,
          itemService: item.itemService,
          itemServiceVat: item.itemServiceVat,
        ));
      });

      PayPaymentData data = new PayPaymentData(
        paymentCode: arguments.paymentCode,
        receiptCode: arguments.receiptCode,
        payId: _chargeId.value,
        payMethod: method,
        payDate: (_transferDate.value == null || _transferDate.value == '')
            ? null
            : DateTime.parse(_transferDate.value),
        payAmount: _transferAmount.value,
        payAttachs: payAttachs,
        hawbNos: hawbNos,
      );
      PaymentUrl res = await _tosservice.payPayment(data);
      loadEvent.add(false);
      return res;
    } catch (e) {
      loadEvent.add(false);
      Fluttertoast.showToast(msg: e.message.toString());
    }
  }
}
