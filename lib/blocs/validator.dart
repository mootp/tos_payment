import 'dart:async';
import 'dart:io';
import 'package:tos_payment/models/payment.model.dart';

class Validator {
  final validateEmpty =
      StreamTransformer<String, String>.fromHandlers(handleData: (value, sink) {
    if ((value ?? '').toString().isEmpty) {
      sink.addError("กรุณากรอกข้อมูล");
    } else {
      sink.add(value);
    }
  });

  final validateDoubleEmpty =
      StreamTransformer<double, double>.fromHandlers(handleData: (value, sink) {
    if ((value ?? '').toString().isEmpty) {
      sink.addError("กรุณากรอกช้อมูล");
    } else {
      sink.add(value);
    }
  });

  final validateListAwb =
      StreamTransformer<List<AwbData>, List<AwbData>>.fromHandlers(
          handleData: (value, sink) {
    if (value.length <= 0) {
      sink.addError("ไม่มีช้อมูลการเก็บเงิน");
    } else {
      double total = 0.0;
      value.forEach((item) {
        double totalRow = item.itemAdvance +
            item.itemFreight +
            item.itemService +
            item.itemServiceVat;
        total += totalRow;
      });

      if (total > 0) {
        sink.add(value);
      } else {
        sink.addError("กรุณาระบุจำนวนเงิน");
      }
    }
  });

  final validateListAwbNoTotal =
      StreamTransformer<List<AwbData>, List<AwbData>>.fromHandlers(
          handleData: (value, sink) {
    if (value.length <= 0) {
      sink.addError("ไม่มีช้อมูล AWB");
    } else {
      sink.add(value);
    }
  });


  final validateListFile =
      StreamTransformer<List<File>, List<File>>.fromHandlers(
          handleData: (value, sink) {
    if (value.length <= 0) {
      sink.addError("กรุณาแนบสลิปการโอนเงิน");
    } else {
      sink.add(value);
    }
  });

  final validateAwbNoAttach =
      StreamTransformer<List<AwbVerifyData>, List<AwbVerifyData>>.fromHandlers(
          handleData: (value, sink) {
    if (value.length <= 0) {
      sink.addError("ไม่มีช้อมูล AWB");
    } else {
      sink.add(value);
    }
  });
}
