import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';

class HomeBloc extends BlocBase with Validator {
  HomeBloc() {
    loadEvent.add(false);
  }

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  @override
  void dispose() {
    _load?.close();
    super.dispose();
  }
}
