import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/services/auth.service.dart';
import 'package:tos_payment/utils/routes.util.dart';

class SplashBloc extends BlocBase with Validator {
  BaseAuth _authservice = new AuthService();
  SplashBloc() {
    loadEvent.add(false);
    pageEvent.add('');
  }

  final BehaviorSubject<String> _page = new BehaviorSubject<String>();
  Stream<String> get pageStream => _page.stream;
  Sink<String> get pageEvent => _page.sink;

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  // final BehaviorSubject<List<Widget>> _listPage =
  //     new BehaviorSubject<List<Widget>>();
  // Stream<List<Widget>> get listPageStream => _listPage.stream;
  // Sink<List<Widget>> get listPageEvent => _listPage.sink;

  @override
  void dispose() {
    _load?.close();
    _page?.close();
    super.dispose();
  }

  checkUser() async {
    try {
      String token = await _authservice.gettoken;
      if (token != null) {
        pageEvent.add(RouteData.RouteRoot);
      } else {
        pageEvent.add(RouteData.RouteLogin);
      }
    } catch (e) {
      pageEvent.add(RouteData.RouteLogin);
    }
  }
}
