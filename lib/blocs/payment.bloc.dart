import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/models/api_tos.model.dart';
import 'package:tos_payment/models/arguments.model.dart';
import 'package:tos_payment/models/charge.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/models/station.model.dart';
import 'package:tos_payment/services/tos.service.dart';
import 'package:tos_payment/models/enum.model.dart';

class PaymentBloc extends BlocBase with Validator {
  BaseTos _tosservice = new TosService();
  PaymentBloc() {
    loadEvent.add(false);
    taxidTextEditingControllerEvent.add(TextEditingController());
    taxidEvent.add('');
    branchTextEditingControllerEvent.add(TextEditingController());
    branchEvent.add('');
    nameTextEditingControllerEvent.add(TextEditingController());
    nameEvent.add('');
    addressTextEditingControllerEvent.add(TextEditingController());
    addressEvent.add('');

    stationTextEditingControllerEvent.add(TextEditingController());
    stationEvent.add('');
    methodTextEditingControllerEvent.add(TextEditingController());
    methodEvent.add('');
    liststationEvent.add([]);
    listmethodEvent.add([]);

    listawbEvent.add([]);
    priceEvent.add(0.0);

    listreceiptEvent.add([]);
  }

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  final BehaviorSubject<TextEditingController> _taxidTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get taxidTextEditingControllerStream =>
      _taxidTextEditingController.stream;
  Sink<TextEditingController> get taxidTextEditingControllerEvent =>
      _taxidTextEditingController.sink;

  final BehaviorSubject<String> _taxid = new BehaviorSubject<String>();
  Stream<String> get taxidStream => _taxid.stream;
  Sink<String> get taxidEvent => _taxid.sink;

  final BehaviorSubject<TextEditingController> _branchTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get branchTextEditingControllerStream =>
      _branchTextEditingController.stream;
  Sink<TextEditingController> get branchTextEditingControllerEvent =>
      _branchTextEditingController.sink;

  final BehaviorSubject<String> _branch = new BehaviorSubject<String>();
  Stream<String> get branchStream => _branch.stream;
  Sink<String> get branchEvent => _branch.sink;

  final BehaviorSubject<TextEditingController> _nameTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get nameTextEditingControllerStream =>
      _nameTextEditingController.stream;
  Sink<TextEditingController> get nameTextEditingControllerEvent =>
      _nameTextEditingController.sink;

  final BehaviorSubject<String> _name = new BehaviorSubject<String>();
  Stream<String> get nameStream => _name.stream.transform(validateEmpty);
  Sink<String> get nameEvent => _name.sink;

  final BehaviorSubject<TextEditingController> _addressTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get addressTextEditingControllerStream =>
      _addressTextEditingController.stream;
  Sink<TextEditingController> get addressTextEditingControllerEvent =>
      _addressTextEditingController.sink;

  final BehaviorSubject<String> _address = new BehaviorSubject<String>();
  Stream<String> get addressStream => _address.stream.transform(validateEmpty);
  Sink<String> get addressEvent => _address.sink;

  final BehaviorSubject<List<AwbData>> _listawb =
      new BehaviorSubject<List<AwbData>>();
  Stream<List<AwbData>> get listawbStream => _listawb.stream;
  Stream<List<AwbData>> get listawbValidate =>
      _listawb.stream.transform(validateListAwb);
  Sink<List<AwbData>> get listawbEvent => _listawb.sink;

  final BehaviorSubject<double> _price = new BehaviorSubject<double>();
  Stream<double> get priceStream => _price.stream;
  Sink<double> get priceEvent => _price.sink;

  final BehaviorSubject<TextEditingController> _stationTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get stationTextEditingControllerStream =>
      _stationTextEditingController.stream;
  Sink<TextEditingController> get stationTextEditingControllerEvent =>
      _stationTextEditingController.sink;

  final BehaviorSubject<String> _station = new BehaviorSubject<String>();
  Stream<String> get stationStream => _station.stream.transform(validateEmpty);
  Sink<String> get stationEvent => _station.sink;

  final BehaviorSubject<TextEditingController> _methodTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get methodTextEditingControllerStream =>
      _methodTextEditingController.stream;
  Sink<TextEditingController> get methodTextEditingControllerEvent =>
      _methodTextEditingController.sink;

  final BehaviorSubject<String> _method = new BehaviorSubject<String>();
  Stream<String> get methodStream => _method.stream;
  Sink<String> get methodEvent => _method.sink;

  // final BehaviorSubject<List<CodeText>> _liststation =
  //     new BehaviorSubject<List<CodeText>>();
  // Stream<List<CodeText>> get liststationStream => _liststation.stream;
  // Sink<List<CodeText>> get liststationEvent => _liststation.sink;

  final BehaviorSubject<List<StationData>> _liststation =
      new BehaviorSubject<List<StationData>>();
  Stream<List<StationData>> get liststationStream => _liststation.stream;
  Sink<List<StationData>> get liststationEvent => _liststation.sink;

  final BehaviorSubject<List<CodeText>> _listmethod =
      new BehaviorSubject<List<CodeText>>();
  Stream<List<CodeText>> get listmethodStream => _listmethod.stream;
  Sink<List<CodeText>> get listmethodEvent => _listmethod.sink;

  Stream<bool> get detailValid => Rx.combineLatest4(nameStream, addressStream,
      stationStream, listawbValidate, (a, b, c, d) => true);

  final BehaviorSubject<List<PaymentData>> _listpay =
      new BehaviorSubject<List<PaymentData>>();
  Stream<List<PaymentData>> get listpayStream => _listpay.stream;
  Sink<List<PaymentData>> get listpayEvent => _listpay.sink;

  final BehaviorSubject<TextEditingController> _fdateTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get fdateTextEditingControllerStream =>
      _fdateTextEditingController.stream;
  Sink<TextEditingController> get fdateTextEditingControllerEvent =>
      _fdateTextEditingController.sink;

  final BehaviorSubject<TextEditingController> _tdateTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get tdateTextEditingControllerStream =>
      _tdateTextEditingController.stream;
  Sink<TextEditingController> get tdatesTextEditingControllerEvent =>
      _tdateTextEditingController.sink;

  final BehaviorSubject<String> _fdate = new BehaviorSubject<String>();
  Stream<String> get fdateStream => _fdate.stream;
  Sink<String> get fdateEvent => _fdate.sink;

  final BehaviorSubject<String> _tdate = new BehaviorSubject<String>();
  Stream<String> get tdateStream => _tdate.stream;
  Sink<String> get tdateEvent => _tdate.sink;

  final BehaviorSubject<List<ReceiptListData>> _listreceipt =
      new BehaviorSubject<List<ReceiptListData>>();
  Stream<List<ReceiptListData>> get listreceiptStream => _listreceipt.stream;
  Sink<List<ReceiptListData>> get listreceiptEvent => _listreceipt.sink;

  final BehaviorSubject<String> _paymentCode = new BehaviorSubject<String>();
  Stream<String> get paymentCodeStream => _paymentCode.stream;
  Sink<String> get paymentCodeEvent => _paymentCode.sink;

  final BehaviorSubject<String> _receiptCode = new BehaviorSubject<String>();
  Stream<String> get receiptCodeStream => _receiptCode.stream;
  Sink<String> get receiptCodeEvent => _receiptCode.sink;

  @override
  void dispose() {
    _load?.close();
    _taxidTextEditingController?.close();
    _taxid?.close();

    _branchTextEditingController?.close();
    _branch?.close();

    _nameTextEditingController?.close();
    _name?.close();

    _addressTextEditingController?.close();
    _address?.close();

    _listawb?.close();
    _price?.close();

    _stationTextEditingController?.close();
    _station?.close();
    _methodTextEditingController?.close();
    _method?.close();

    _liststation?.close();
    _listmethod?.close();
    _listpay?.close();
    _fdateTextEditingController?.close();
    _tdateTextEditingController?.close();
    _fdate?.close();
    _tdate?.close();

    _listreceipt?.close();
    _paymentCode?.close();
    _receiptCode?.close();
    super.dispose();
  }

  // awbprice(String awb) {
  //   List<AwbData> list = _listawb.value;
  //   double price = list.firstWhere((d) => d.awb == awb).price;
  //   return price;
  // }

  editawbprice(String awb, PriceType type) {
    List<AwbData> list = _listawb.value;
    double price = _price.value;

    list.where((d) => d.hawbNo == awb).forEach((item) {
      if (type == PriceType.advance) {
        item.itemAdvance = price;
      } else if (type == PriceType.freight) {
        item.itemFreight = price;
      } else if (type == PriceType.service) {
        item.itemService = price;
      } else if (type == PriceType.vat) {
        item.itemServiceVat = price;
      }

      // item.itemAdvance: item.itemAdvance,
      //     item.itemFreight: 0.0,
      //     item.itemService: 0.0,
      //     item.itemServiceVat: 0.0,
    });

    priceEvent.add(0.0);
    listawbEvent.add(list);
  }

  loadstation() async {
    await _tosservice.loadStationAccess().then((result) {
      liststationEvent.add(result);
      result.forEach((s) {
        if (s.stationDefault.toUpperCase() == 'Y') {
          selectstation(s.stationCode);
        }
      });
    });
  }

  checkStationCode(String station) {
    return _station.value == station;
  }

  selectstation(String station) {
    stationEvent.add(station);
    stationTextEditingControllerEvent.add(TextEditingController(text: station));
  }

  loadmethod() async {
    await _tosservice.loadmethod().then((result) {
      listmethodEvent.add(result);
    });
  }

  checkMethod(String method) {
    return _method.value == method;
  }

  selectmethod(String method) {
    methodEvent.add(method);
    methodTextEditingControllerEvent.add(TextEditingController(text: method));
  }

  Future<PaymentSummaryArguments> paymentSummaryArguments() async {
    PaymentSummaryArguments arguments = new PaymentSummaryArguments(
      paymentCode: null,
      receiptCode: null,
      taxId: _taxid.value,
      branch: _branch.value,
      name: _name.value,
      address: _address.value,
      station: _station.value,
      method: _method.value,
      process: 'PICKUP',
      listAwb: _listawb.value,
    );
    return arguments;
  }

  Future<PaymentUrl> createPayment(BuildContext context, String process) async {
    loadEvent.add(true);
    String paymentCode = _paymentCode.value;
    String receiptCode = _receiptCode.value;
    CreatePaymentData data = new CreatePaymentData(
      paymentCode: paymentCode,
      receiptCode: receiptCode,
      taxId: _taxid.value,
      branch: _branch.value,
      name: _name.value,
      address: _address.value,
      station: _station.value,
      process: process,
      hawbNos: _listawb.value,
    );
    PaymentUrl payment = await _tosservice.createPayment(data);

    loadEvent.add(false);
    return payment;
  }

  loadPaymentList() async {
    loadEvent.add(true);
    try {
      List<PaymentData> list =
          await _tosservice.loadPaymentList(_fdate.value, _tdate.value);
      listpayEvent.add(list);
    } catch (e) {
      // Fluttertoast.showToast(msg: e.message.toString());
    }
    loadEvent.add(false);
  }

  receiptUrl(String receiptCode) async {
    try {
      String url = await _tosservice.pdfreceipt(receiptCode);
      return url;
    } catch (e) {
      Fluttertoast.showToast(msg: e.message.toString());
    }
  }

  loadReceiptList() async {
    loadEvent.add(true);
    try {
      List<AwbData> listawb = _listawb.value;
      List<String> list = [];

      listawb.forEach((element) {
        list.add(element.hawbNo);
      });

      List<ReceiptListData> data = await _tosservice.loadReceiptList(list);
      listreceiptEvent.add(data);
      loadEvent.add(false);
    } catch (e) {
      loadEvent.add(false);
      Fluttertoast.showToast(msg: e.message.toString());
    }
  }
}
