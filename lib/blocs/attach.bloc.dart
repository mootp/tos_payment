import 'dart:io';

import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';
import 'package:tos_payment/blocs/validator.dart';
import 'package:tos_payment/models/attach.model.dart';
import 'package:tos_payment/models/history.model.dart';
import 'package:tos_payment/models/payment.model.dart';
import 'package:tos_payment/services/tos.service.dart';

class AttachBloc extends BlocBase with Validator {
  BaseTos _tosservice = new TosService();
  AttachBloc() {
    loadEvent.add(false);

    _listImagesCard.add(List<File>());
    _listImagesPhoto.add(List<File>());
    _listImagesOther.add(List<File>());

    listawbEvent.add([]);
    listhistoryEvent.add([]);
    awbTextEditingControllerEvent.add(TextEditingController());
    awbEvent.add('');
    scaffoldKeyEvent.add(GlobalKey<ScaffoldState>());
  }

  @override
  void dispose() {
    _load?.close();
    _listImagesCard?.close();
    _listImagesPhoto?.close();
    _listImagesOther?.close();
    _listawb?.close();
    _fdate?.close();
    _tdate?.close();
    _listhistory?.close();
    _awbTextEditingController?.close();
    _awb?.close();
    _scaffoldKey?.close();
    super.dispose();
  }

  final BehaviorSubject<bool> _load = new BehaviorSubject<bool>();
  Stream<bool> get loadStream => _load.stream;
  Sink<bool> get loadEvent => _load.sink;

  final BehaviorSubject<TextEditingController> _awbTextEditingController =
      new BehaviorSubject<TextEditingController>();
  Stream<TextEditingController> get awbTextEditingControllerStream =>
      _awbTextEditingController.stream;
  Sink<TextEditingController> get awbTextEditingControllerEvent =>
      _awbTextEditingController.sink;

  final BehaviorSubject<String> _awb = new BehaviorSubject<String>();
  Stream<String> get awbStream => _awb.stream.transform(validateEmpty);
  // Stream<String> get awbValidate => _awb.stream.transform(validateEmpty);
  Sink<String> get awbEvent => _awb.sink;
  Stream<bool> get awbValid => Rx.combineLatest([awbStream], (a) => true);

  final BehaviorSubject<List<File>> _listImagesCard =
      new BehaviorSubject<List<File>>();
  Stream<List<File>> get listImagesCardStream => _listImagesCard.stream;
  Stream<List<File>> get listImagesCardvalidate =>
      _listImagesCard.stream.transform(validateListFile);
  Sink<List<File>> get listImagesCardEvent => _listImagesCard.sink;

  final BehaviorSubject<List<File>> _listImagesPhoto =
      new BehaviorSubject<List<File>>();
  Stream<List<File>> get listImagesPhotoStream => _listImagesPhoto.stream;
  Stream<List<File>> get listImagesPhotovalidate =>
      _listImagesPhoto.stream.transform(validateListFile);
  Sink<List<File>> get listImagesPhotoEvent => _listImagesPhoto.sink;

  final BehaviorSubject<List<File>> _listImagesOther =
      new BehaviorSubject<List<File>>();
  Stream<List<File>> get listImagesOtherStream => _listImagesOther.stream;
  Stream<List<File>> get listImagesOthervalidate =>
      _listImagesOther.stream.transform(validateListFile);
  Sink<List<File>> get listImagesOtherEvent => _listImagesOther.sink;

  final BehaviorSubject<List<AwbVerifyData>> _listawb =
      new BehaviorSubject<List<AwbVerifyData>>();
  Stream<List<AwbVerifyData>> get listawbStream => _listawb.stream;
  Stream<List<AwbVerifyData>> get listawbValidate =>
      _listawb.stream.transform(validateAwbNoAttach);
  Sink<List<AwbVerifyData>> get listawbEvent => _listawb.sink;

  Stream<bool> get attachValid => Rx.combineLatest3(listawbValidate,
      listImagesCardvalidate, listImagesPhotovalidate, (a, b, c) => true);

  final BehaviorSubject<String> _fdate = new BehaviorSubject<String>();
  Stream<String> get fdateStream => _fdate.stream;
  Sink<String> get fdateEvent => _fdate.sink;

  final BehaviorSubject<String> _tdate = new BehaviorSubject<String>();
  Stream<String> get tdateStream => _tdate.stream;
  Sink<String> get tdateEvent => _tdate.sink;

  final BehaviorSubject<List<HistoryListData>> _listhistory =
      new BehaviorSubject<List<HistoryListData>>();
  Stream<List<HistoryListData>> get listhistoryStream => _listhistory.stream;
  Sink<List<HistoryListData>> get listhistoryEvent => _listhistory.sink;

  final BehaviorSubject<GlobalKey<ScaffoldState>> _scaffoldKey =
      new BehaviorSubject<GlobalKey<ScaffoldState>>();
  Stream<GlobalKey<ScaffoldState>> get scaffoldKeyStream => _scaffoldKey.stream;
  Sink<GlobalKey<ScaffoldState>> get scaffoldKeyEvent => _scaffoldKey.sink;

  void showInSnackBar(String value) {
    _scaffoldKey.value.currentState.showSnackBar(
      SnackBar(
        content: Text(value),
      ),
    );
  }

  addAwbList(List<AwbData> list) {
    List<AwbVerifyData> listAwb = [];

    list.forEach((e) {
      AwbVerifyData awb = AwbVerifyData();
      awb.hawbNo = e.hawbNo;
      awb.itemAdvance = e.itemAdvance;
      awb.itemFreight = e.itemFreight;
      awb.itemService = e.itemService;
      awb.itemServiceVat = e.itemServiceVat;
      awb.awbAttach = [];
      listAwb.add(awb);
    });
    listawbEvent.add(listAwb);
  }

  addImageCard(File pic) {
    List<File> resultList = _listImagesCard.value;
    resultList.add(pic);
    listImagesCardEvent.add(resultList);
  }

  addImagePhoto(File pic) {
    List<File> resultList = _listImagesPhoto.value;
    resultList.add(pic);
    listImagesPhotoEvent.add(resultList);
  }

  addImageOther(File pic) {
    List<File> resultList = _listImagesOther.value;
    resultList.add(pic);
    listImagesOtherEvent.add(resultList);
  }

  addImageAwb(File pic, String awb) {
    List<AwbVerifyData> resultList = _listawb.value;
    resultList.forEach((e) {
      if (e.hawbNo == awb) {
        e.awbAttach.add(pic);
      }
    });

    listawbEvent.add(resultList);
  }

  removeImageCard(String delpath) {
    List<File> listImage = _listImagesCard.value;
    for (var i = 0; i < listImage.length; i++) {
      String path = listImage[i].path;
      if (path == delpath) {
        listImage.removeAt(i);
      }
    }
    listImagesCardEvent.add(listImage);
  }

  removeImagePhoto(String delpath) {
    List<File> listImage = _listImagesPhoto.value;
    for (var i = 0; i < listImage.length; i++) {
      String path = listImage[i].path;
      if (path == delpath) {
        listImage.removeAt(i);
      }
    }
    listImagesPhotoEvent.add(listImage);
  }

  removeImageOther(String delpath) {
    List<File> listImage = _listImagesOther.value;
    for (var i = 0; i < listImage.length; i++) {
      String path = listImage[i].path;
      if (path == delpath) {
        listImage.removeAt(i);
      }
    }
    listImagesOtherEvent.add(listImage);
  }

  removeImageAwb(String delpath, String awb) {
    List<AwbVerifyData> listImage = _listawb.value;
    listImage.forEach((e) {
      if (e.hawbNo == awb) {
        List<File> listfile = e.awbAttach;
        for (var i = 0; i < listfile.length; i++) {
          String path = listfile[i].path;
          if (path == delpath) {
            listfile.removeAt(i);
          }
        }
      }
    });
    listawbEvent.add(listImage);
  }

  attachAwb() async {
    loadEvent.add(true);
    try {
      List<File> listImageCard = _listImagesCard.value;
      List<File> listImagePhoto = _listImagesPhoto.value;
      List<File> listImageOther = _listImagesOther.value;
      List<AwbVerifyData> listAwb = _listawb.value;

      List<String> listimgCard = [];
      List<String> listimgPhoto = [];
      List<String> listimgOther = [];
      List<Awb> hawbNos = [];

      await Future.wait(listImageCard.map((img) async {
        String path = img.path;
        listimgCard.add(path);
      }));

      await Future.wait(listImagePhoto.map((img) async {
        String path = img.path;
        listimgPhoto.add(path);
      }));
      await Future.wait(listImageOther.map((img) async {
        String path = img.path;
        listimgOther.add(path);
      }));

      listAwb.forEach((item) async {
        List<String> awbimg = [];
        await Future.wait(item.awbAttach.map((img) async {
          String path = img.path;
          awbimg.add(path);
        }));
        hawbNos.add(Awb(
          hawbNo: item.hawbNo,
          awbAttachs: awbimg,
        ));
      });

      AwbAttachData data = new AwbAttachData(
        hawbNos: hawbNos,
        cardAttachs: listimgCard,
        photoAttachs: listimgPhoto,
        otherAttachs: listimgOther,
      );
      bool res = await _tosservice.awbAttach(data);
      loadEvent.add(false);
      return res;
    } catch (e) {
      loadEvent.add(false);
      showInSnackBar(e.message);
    }
  }

  loadScanHistoryList() async {
    loadEvent.add(true);
    try {
      List<HistoryListData> list =
          await _tosservice.loadHistoryList(_fdate.value, _tdate.value);
      listhistoryEvent.add(list);
    } catch (e) {
      loadEvent.add(false);
      showInSnackBar(e.message);
    }
    loadEvent.add(false);
  }

  editAwb(int verifyId, String verifyCode, int awbId, String hawbNo) async {
    loadEvent.add(true);
    try {
      String newHawbNo = _awb.value.toUpperCase();
      AwbUpdateData data = new AwbUpdateData();
      data.verifyId = verifyId;
      data.verifyCode = verifyCode;
      data.hawbNos = [
        AwbHawbNo(
          awbId: awbId,
          hawbNo: newHawbNo,
          oldHawbNo: hawbNo,
        ),
      ];
      bool res = await _tosservice.awbUpdate(data);
      return res;
    } catch (e) {
      loadEvent.add(false);
      showInSnackBar(e.message);
    }
    loadEvent.add(false);
  }

  cancelEdit() {
    awbTextEditingControllerEvent.add(TextEditingController());
    awbEvent.add('');
  }
}
